import BlockA from "./blocks/BlockA";
import { ThemeProvider, createTheme } from "@mui/material";
import { Routes, Route, HashRouter } from "react-router-dom";
import BlockB from "./blocks/BlockB";
import BlockC from "./blocks/BlockC";
import BookingTable from "./pages/BookingTable";
import Details from "./pages/Details";
import LoginPage from "./pages/LoginPage";
import React from "react";

import Profile from "./pages/Profile";
import InstallmentsPage from "./installments/InstallmentsPage";
import Dashboard from "./pages/Dashboard";
import MyAccount from "./pages/MyAccount";
import SignUpPage from "./pages/SignUpPage";
import Recyclebin from "./pages/Recyclebin";
import RecordTableTest from "./components/RecordTableTest";
import ClientInstallments from "./pages/ClientInstallments";
import Terms from "./pages/Terms";
import BonusPage from "./pages/BonusPage";


function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
    },
  });
  return (
    <>
      <ThemeProvider theme={theme}>
        <HashRouter>
          <Routes>
            <Route index element={<LoginPage />} /> 
            <Route path="home" element={<Dashboard />} />
            <Route path="/blockb" element={<BlockB />} />
            <Route path="/blockc" element={<BlockC />} />
            <Route path="/signup" element={<SignUpPage />} />
            <Route path="/bonuspage" element={<BonusPage />} />
            <Route path="/terms" element={<Terms />} />
            <Route path="/myaccount" element={<MyAccount />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/recycle" element={<Recyclebin />} />
            <Route path="/records" element={<RecordTableTest />} />
            <Route path="/details" element={<Details />} />
            <Route path="/installments" element={<ClientInstallments />} />
            <Route path="/dash" element={<BlockA />} />
          </Routes>
        </HashRouter>
      </ThemeProvider>
    </>
  );
}

export default App;
