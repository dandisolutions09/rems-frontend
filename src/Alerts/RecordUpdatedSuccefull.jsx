import React from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

const RecordUpdatedSuccefull = ({ open, onClose, autoHideDuration }) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert
        onClose={onClose}
        severity="success"
        sx={{ width: ["60%", "100%"] }}
      >
        Record Updated Successfully!
      </Alert>
    </Snackbar>
  );
};

export default RecordUpdatedSuccefull;
