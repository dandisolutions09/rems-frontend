import React from 'react'
import "./Loader2.css"
export default function Loader2() {
  return (
    <div className="fixed top-0 left-0 w-full h-full flex justify-center items-center bg-gray-200 bg-opacity-50"><div class="loader2"></div></div>
  )
}
