import React from 'react'
import "../styles.css"
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";


import BlocksNavigation from '../components/BlocksNavigation';
import AddSucces from '../Alerts/AddSucces';
import BarChartReacords from '../components/BarChartReacords';


export default function BlockA() {
    const [openAddNurseSuccess, setOpenAddNurseSuccess] = React.useState(false);
      const [backgroundColor, setBackgroundColor] = React.useState("#65350f");
  const role = [
    {
      key: "1",
      value: "Admin",
      label: "Booking",
    },
    {
      key: "2",
      value: "Intermediate",
      label: "Selling",
    },
    // {
    //   key: "3",
    //   value: "Basic",
    //   label: "Basic",
    // },
  ];
  const status = [
    {
      value: "Active",
      label: "Agent",
    },
    {
      value: "Inactive",
      label: "No Agent",
    },
  ];
  
  const [openView, setOpenView] = React.useState(false);
  const handleChange = () =>{
    console.log("change")
  }
   const handleCloseViewUser = () => {
     setOpenView(false);
   };
   const handleOPenViewUser = () => {
     setOpenView(true);
   };
   const handle_submit = () =>{
    handleCloseViewUser();
    setOpenAddNurseSuccess(true);
     const newColor = "#fff"; // Replace 'yourNewColor' with the desired color code
     setBackgroundColor(newColor);
   }
 
    
  return (
    <>
      <AddSucces
        open={openAddNurseSuccess}
        autoHideDuration={6000}
        onClose={() => setOpenAddNurseSuccess(false)}
      />
      <Modal
        open={openView}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              M3 Block A
            </Typography>

            <CloseIcon
              fontSize="40"
              onClick={handleCloseViewUser}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  InputProps={{
                    readOnly: true,
                  }}
                  value={"M3 BLOCK A"}
                  onChange={handleChange}
                />
                <FormLabel>Full Name </FormLabel>
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  // InputProps={{
                  //   readOnly: true,
                  // }}
                  value={""}
                  //   onChange={formik.handleChange}
                />
                <FormLabel>Select </FormLabel>
                <TextField
                  id="role"
                  select
                  label="Action"
                  name="role"
                  //   value={formik.values.role}
                  //   onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>

                <FormControl>
                  <FormLabel>Select </FormLabel>

                  <TextField
                    id="status"
                    select
                    name="status"
                    // value={formik.values.status}
                    // onChange={formik.handleChange}
                    // onBlur={formik.handleBlur}
                  >
                    {status.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button
                variant="contained"
                size="small"
                sx={{ backgroundColor: "#242333" }}
                onClick={handle_submit}
              >
                Save
              </Button>
              <Button
                onClick={handleCloseViewUser}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="small"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <div className="grid bg-[#242333] text-gray-100 justify-center text-center h-screen">
       {/* < BarChartReacords/> */}

        <div>
          <div>
            <BlocksNavigation />
          </div>
          <div className="justify-center text-center">
            <Typography variant="h3" component="h2">
              Block A
            </Typography>
          </div>
          <ul class="showcase">
            <li>
              <div class="seat"></div>
              <small>N/A</small>
            </li>

            <li>
              <div class="seat selected"></div>
              <small>Booked</small>
            </li>

            <li>
              <div class="seat occupied"></div>
              <small>Sold Out</small>
            </li>
          </ul>
          <div class="container">
            <div class="screen"></div>
            <div class="row">
              <div
                className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"
                onClick={handleChange}
              >
                m2
              </div>
              <div class="seat" onClick={handleOPenViewUser}>
                m3
              </div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
            </div>
            <div class="row">
              <div class="seat">G4</div>
              <div class="seat">G6</div>
              <div class="seat"></div>
              <div class="seat occupied"></div>
              <div class="seat occupied"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
            </div>

            <div class="row">
              <div class="seat">E1</div>
              <div class="seat">E2</div>
              <div class="seat"></div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat occupied"></div>
              <div class="seat occupied"></div>
            </div>

            <div class="row">
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg">
                Q2
              </div>
              <div class="seat">Q3</div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
            </div>

            <div class="row">
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat occupied"></div>
              <div class="seat occupied"></div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
              <div class="seat"></div>
              <div class="seat"></div>
            </div>

            <div class="row">
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat"></div>
              <div class="seat occupied"></div>
              <div class="seat occupied"></div>
              <div class="seat occupied"></div>
              <div className="bg-[#65350f] h-[50px] w-[45px] m-[3px] rounded-t-lg"></div>
            </div>
          </div>
          <div className="justify-center text-center">
            <p class="text">
              You have selected <span id="count">0</span> Room for a price of{" "}
              <span id="total">0</span>milion
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
};
