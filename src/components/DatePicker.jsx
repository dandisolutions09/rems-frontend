// import * as React from 'react';
// import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
// import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
// import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
// import { DatePicker } from '@mui/x-date-pickers/DatePicker';

// export default function BasicDatePicker() {
//   return (
//     <LocalizationProvider dateAdapter={AdapterDayjs}>
//       <DemoContainer components={['DatePicker']}>
//         <DatePicker label="Basic date picker" />
//       </DemoContainer>
//     </LocalizationProvider>
//   );
// }

import * as React from "react";
import { useFormik } from "formik"; // Import useFormik hook
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export default function BasicDatePicker() {
  // Initialize Formik
  const formik = useFormik({
    initialValues: {
      selectedDate: null, // Initialize selectedDate with null
    },
    onSubmit: (values) => {
      // Submit logic, you can access selected date via values.selectedDate
      console.log(values.selectedDate);
    },
  });

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DemoContainer components={["DatePicker"]}>
        <form onSubmit={formik.handleSubmit}>
          {" "}
          {/* Wrap in a form and attach Formik's handleSubmit */}
          <DatePicker
            label="Basic date picker"
            value={formik.values.selectedDate} // Pass value from Formik
            onChange={(date) => formik.setFieldValue("selectedDate", date)} // Update Formik field value
          />
          <button type="submit">Submit</button> {/* Add a submit button */}
        </form>
      </DemoContainer>
    </LocalizationProvider>
  );
}
