import React from 'react'
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

import { useNavigate } from 'react-router-dom';
import { Box } from '@mui/material';
import LightTooltip from './LightTooltip';



export default function BlocksNavigation() {
    const navigate = useNavigate();
    const handle_NavigateA = () =>{
        navigate("/")
    }
       const handle_NavigateB = () => {
         navigate("/blockb");
       };
          const handle_NavigateC = () => {
            navigate("/blockc");
          };
  return (
    <>
      <Box
        className="absolute right-4 flex flex-row sm:gap-12 gap-4 m-4"
        sx={{ display: { xs: "block", sm: "block", md: "flex" } }}
      >
        {/* <LightTooltip title="Navigate to other blocks">
          <div className=" text-xl cursor-pointer hover:scale-110 duration-200 ease-linear">
            <ArrowForwardIosIcon />
          </div>
        </LightTooltip> */}
        <LightTooltip title="Navigate to Block A">
          <li className="cursor-pointer " >
            Block A
          </li>
        </LightTooltip>

        <LightTooltip title="Navigate to Block B">
          <li className="cursor-pointer" >
            Block B
          </li>
        </LightTooltip>
        <LightTooltip title="Navigate to Block C">
          <li className="cursor-pointer" >
            Block C
          </li>
        </LightTooltip>
      </Box>
    </>
  );
}
