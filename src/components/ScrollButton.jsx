import React, { useState } from "react";

const ScrollButton = () => {
  const [isVisible, setIsVisible] = useState(false);

  const handleScroll = () => {
    const scrollTop = window.pageYOffset;

    if (scrollTop > 300) {
      // Adjust this value as needed
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  // Add event listener for scroll
  window.addEventListener("scroll", handleScroll);

  return (
    <button
      className={`scroll-button ${isVisible ? "show" : "hide"}`}
      onClick={scrollToTop}
    >
      Scroll To Top
    </button>
  );
};

export default ScrollButton;
