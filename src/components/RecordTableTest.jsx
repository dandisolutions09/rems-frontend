import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import TablePagination from "@mui/material/TablePagination";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Dropdown from "antd/es/dropdown/dropdown";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import * as Yup from "yup";

import InputAdornment from "@mui/material/InputAdornment";

import {
  Button,
  CardActions,
  CardContent,
  CardMedia,
  FormControl,
  FormLabel,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  TableFooter,
  TextField,
} from "@mui/material";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import { styled, alpha } from "@mui/material/styles";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import LightTooltip from "./LightTooltip";
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import MainNavbar from "./MainNavbar";
import NewRecord from "./NewRecord";
import { update } from "lodash";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import RecordUpdatedSuccefull from "../Alerts/RecordUpdatedSuccefull";
import Loader2 from "../loader2/Loader2";
import { InstallDesktop, Search } from "@mui/icons-material";
import InstallmentError from "../Alerts/InstallmentError";
import Spinner1 from "./Spinner1";

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      {
        date: "2020-01-05",
        customerId: "11091700",
        amount: 3,
      },
      {
        date: "2020-01-02",
        customerId: "Anonymous",
        amount: 1,
      },
    ],
  };
}

const ChangeToBooked = async (recordId) => {
  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  console.log("GLOBAL ID", recordId._id);
  console.log("_id", storedData._id);

  console.log("change to Booked function called");

  const delete_property = {
    status: "INACTIVE",
  };

  try {
    const response = await fetch(
      `https://rems-backend.onrender.com/change-to-booked/${storedData._id}/${recordId._id}`,

      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(delete_property),
      }
    );

    if (response.ok) {
      console.log("Records updated successfully");
      //setOpenRecordChangeSuccessfully(true);
      //setRefresher(true);
      //handleNotificationMenuClose();
      //setOpenDeleteRecordModal(false);

      setTimeout(() => {
        //setOpenInstallment(false);
        // setOpenMainEditForm(false);
        reloadPage();
      }, 1000);
    } else {
      console.error("Failed to update record information");
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
};

const ChangeToInterested = async (recordId) => {
  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  console.log("GLOBAL ID", recordId._id);
  console.log("_id", storedData._id);

  console.log("change to Booked function called");

  const delete_property = {
    status: "INACTIVE",
  };

  try {
    const response = await fetch(
      `https://rems-backend.onrender.com/change-to-interested/${storedData._id}/${recordId._id}`,

      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(delete_property),
      }
    );

    if (response.ok) {
      console.log("Records updated successfully");
      //setOpenRecordChangeSuccessfully(true);
      //setRefresher(true);
      //handleNotificationMenuClose();
      //setOpenDeleteRecordModal(false);

      setTimeout(() => {
        //setOpenInstallment(false);
        // setOpenMainEditForm(false);
        reloadPage();
      }, 1000);
    } else {
      console.error("Failed to update record information");
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
};

const changeToFullySold = async (recordId) => {
  const delete_property = {
    status: "INACTIVE",
  };
  console.log("change to sold function called");
  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  console.log("GLOBAL ID", recordId._id);
  console.log("_id", storedData._id);

  try {
    const response = await fetch(
      `https://rems-backend.onrender.com/change-to-fully-sold/${storedData._id}/${recordId._id}`,

      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(delete_property),
      }
    );

    if (response.ok) {
      console.log("Records updated successfully");
      //setOpenRecordChangeSuccessfully(true);
      //setRefresher(true);
      // handleNotificationMenuClose();
      //setOpenDeleteRecordModal(false);

      setTimeout(() => {
        //setOpenInstallment(false);
        // setOpenMainEditForm(false);
        reloadPage();
      }, 1000);
    } else {
      console.error("Failed to update record information");
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
};

const ChangeToSold = async (recordId) => {
  const delete_property = {
    status: "INACTIVE",
  };
  console.log("change to sold function called");
  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  console.log("GLOBAL ID", recordId._id);
  console.log("_id", storedData._id);

  try {
    const response = await fetch(
      `https://rems-backend.onrender.com/change-to-sold/${storedData._id}/${recordId._id}`,

      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(delete_property),
      }
    );

    if (response.ok) {
      console.log("Records updated successfully");
      //setOpenRecordChangeSuccessfully(true);
      // setRefresher(true);
      //handleNotificationMenuClose();
      //setOpenDeleteRecordModal(false);

      setTimeout(() => {
        //setOpenInstallment(false);
        // setOpenMainEditForm(false);
        reloadPage();
      }, 1000);
    } else {
      console.error("Failed to update record information");
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
};
//  const formattedDate = currentDate.toLocaleDateString("en-GB", options);

//  const date_entered = new Date(values.selectedDate).toLocaleDateString(
//    "en-GB",
//    options_selected

function formatDate(dateString) {
  // Create a new Date object by parsing the date string
  const date = new Date(dateString);

  // Extract day, month, and year components
  const day = date.getDate();
  const month = date.getMonth() + 1; // Months are zero-based, so we add 1
  const year = date.getFullYear();

  // Format the date as "dd/mm/yyyy"
  return `${day}/${month}/${year}`;
}

function reloadPage() {
  window.location.reload();
}

//  );

function Row(props) {
  const house_array = [];
  const { row, globalID, chosenRowData } = props;
  const [openHouseDetailsModal, setOpenHouseDetailsModal] =
    React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = useState();
  const [house_information, setHouse_Information] = React.useState([]);
  const [incoming_instalments, setInstallments] = React.useState([]);
  const [bonusPayments, setBonusPayments] = useState();

  const [searchValue, setSearchValue] = React.useState();
  const [filteredResults, setResults] = React.useState([]);
  const [agent_information, setAgentInformation] = React.useState(null);
  const [clickedAgent, setclickedAgent] = React.useState(null);
  const [openHouseEditModal, setopenHouseEditModal] = React.useState(false);
  const [openAddHouseInfor, setOpenAddHouseInfor] = React.useState(false);
  const [openAgentForm, setOpenAgentForm] = React.useState(false);
  const [openpopup, setOpenpopUp] = React.useState(false);
  const [openInstallmentModal, setopenInstallmentModal] = React.useState(false);
  const [openBonusPaymentModal, setopenBonusPaymentModal] =
    React.useState(false);

  //setopenBonusPaymentModal
  const [refresher, setRefresher] = React.useState(false);
  const [global_index, setIndex] = React.useState();
  const [global_id, setGlobalId] = React.useState();
  const [global_date, setGlobalDate] = React.useState();
  const [openMainEditForm, setOpenMainEditForm] = useState(false);
  const [openInstallment, setOpenInstallment] = React.useState(false);
  const [openBonusPayment, setOpenBonusPayment] = React.useState(false);
  const [openEditBonus, setEditBonus] = React.useState(false);
  const [openHouseAddedSuccessfully, setOpenHouseAddedSuccessfully] =
    React.useState(false);

  const [opendeleteRecordModal, setOpenDeleteRecordModal] = useState(false);
  const [opencancelRecordModal, setOpenCancelRecordModal] = useState(false);

  const [installmentError, setInstallmentError] = useState(false);

  const [openbookClientModal, setOpenBookRecordModal] = useState(false);

  //setOpenCancelRecordModal

  //setOpenRecordChangeSuccessfully
  const [openRecordChangeSuccessfully, setOpenRecordChangeSuccessfully] =
    React.useState(false);
  const validation_agentDetails = Yup.object({
    amount: Yup.string().required("Amount is required"),
  });

  const validationSchema_clientDetails = Yup.object({
    client_firstname: Yup.string().required("First Name is required"),
    client_secondname: Yup.string().required("Second Name is required"),
    client_lastname: Yup.string(),
    client_phonenumber: Yup.string().required("Phone Number is required"),
    client_nat_id: Yup.string().required("ID is required"),
    client_email: Yup.string().email().required("Email is required"),
  });

  const validationSchema_editinstallments = Yup.object({
    amount: Yup.string().required("Amount is required"),
  });

  const [loading, setLoading] = useState(false);
  const validationSchema_housedetails = Yup.object({
    location: Yup.string().required("location is required"),
    residence: Yup.string().required("Residence is required"),
    house_number: Yup.string().required("House Number is required"),
    asking_price: Yup.string().required("Asking Price is required"),
    selling_price: Yup.string().required("Selling Price is required"),
    //house_status: Yup.string().required("House Status is required"),
    house_type: Yup.string().required("House Type is required"),
  });

  const validationSchema_agentDeatails = Yup.object({
    first_name: Yup.string().required("First Name is required"),
    last_name: Yup.string().required("Last Name is required"),
    company_name: Yup.string().required("company name is required"),
    company_email: Yup.string().email().required("company email is required"),
    phone_number: Yup.string().required("Phone Number is required"),
  });

  // const SearchIconWrapper = styled("div")(({ theme }) => ({
  //   padding: theme.spacing(0, 2),
  //   height: "100%",
  //   position: "absolute",
  //   pointerEvents: "none",
  //   display: "flex",
  //   alignItems: "center",
  //   justifyContent: "center",
  // }));

  // const StyledInputBase = styled(InputBase)(({ theme }) => ({
  //   color: "inherit",
  //   "& .MuiInputBase-input": {
  //     padding: theme.spacing(1, 1, 1, 0),
  //     // vertical padding + font size from searchIcon
  //     paddingLeft: `calc(1em + ${theme.spacing(4)})`,
  //     transition: theme.transitions.create("width"),
  //     width: "100%",
  //     [theme.breakpoints.up("md")]: {
  //       width: "20ch",
  //     },
  //   },
  // }));

  // console.log("GLOBAL IDS:::", chosenRowData);

  //console.log("row---->", row);

  const handleCloseMainEditform = () => {
    //  console.log("edit form opened");
    setOpenMainEditForm(false);
  };
  const handleCloseHouseDetailsModal = () => {
    setOpenHouseDetailsModal(false);
  };

  function handleRemoveAgent() {
    setclickedAgent({});
  }

  const handleclose_installment = () => {
    setOpenInstallment(false);
  };

  const handleclose_bonus_payment = () => {
    //setOpenBonusPayment(false);
    setEditBonus(false);
  };
  function handleOpenModal() {
    console.log("opening modal");
    setOpenHouseDetailsModal(true);
  }
  const handleOPenEditBonus = () => {
    //setEditBonus(true);
    setOpenBonusPayment(true);
    //const [openBonusPayment, setOpenBonusPayment] = React.useState(false);
  };
  const handleCloseEditBonus = () => {
    //setEditBonus(false);
    setOpenBonusPayment(false);
  };

  function handleDeleteHouseDetail(index) {
    console.log("delete house detail called");

    let updatedHouseArray = [];
    updatedHouseArray = [...house_information];
    updatedHouseArray.splice(index, 1);
    setHouse_Information(updatedHouseArray);
  }

  function handleDeleteInstallmentDetail(index) {
    console.log("delete installment detail called");

    let updatedHouseArray = [];
    updatedHouseArray = [...incoming_instalments];
    updatedHouseArray.splice(index, 1);
    setInstallments(updatedHouseArray);
  }

  function handleDeleteBonusDetail(index) {
    console.log("delete bonusDetail detail called");

    let updatedHouseArray = [];
    updatedHouseArray = [...bonusPayments];
    updatedHouseArray.splice(index, 1);
    setBonusPayments(updatedHouseArray);
  }

  const updateRecord = async (updatedRecordx, mainId, chosenRowData) => {
    // console.log("updated Newly updatedRecord:", updatedRecord);

    // console.log("respective index", selectedRowData._id);

    console.log("MAIN ID", mainId);
    console.log("record", updatedRecordx);
    //console.log("record ID", chosenRowData);
    try {
      const response = await fetch(
        // `https://rems-backend.onrender.com/edit-record/${global_id}/${selectedRowData._id}`,
        `https://rems-backend.onrender.com/edit-record/${mainId}/${selectedRowData._id}`,
        //`http://localhost:8080/edit-record/${mainId}/${selectedRowData._id}`,

        // `https://rems-backend.onrender.com/edit-record/${global_id}/${global_index}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedRecordx),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        // setOpenRecordAddedSuccessfully(true);
        setRefresher(true);

        setLoading(true);
        setTimeout(() => {
          setOpenMainEditForm(false);
          reloadPage();
        }, 2000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  function reloadPage() {
    window.location.reload();
  }

  const postInstallment = async (incoming_instalments, installment) => {
    setLoading(true);
    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/post-installment/${globalID}/${selectedRowData._id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(installment),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);

        setTimeout(() => {
          setOpenInstallment(false);
          setOpenMainEditForm(false);
          reloadPage();
        }, 2000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setLoading(false);
    }
  };

  const postBonusPayment = async (incoming_instalments, installment) => {
    setLoading(true);
    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/post-bonus-payment/${globalID}/${selectedRowData._id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(installment),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);

        setTimeout(() => {
          setOpenInstallment(false);
          setOpenMainEditForm(false);
          reloadPage();
        }, 2000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setLoading(false);
    }
  };

  // const postInstallment = async (incoming_instalments, installment) => {
  //   //let installment_array = [];
  //   //installment_array.push(installmentRecord);

  //   // const updatedInstallments = {
  //   //   installments: incoming_instalments,
  //   // };

  //   console.log("new incoming installments", installment);

  //   console.log("Document ID to be updated:-->", globalID, global_index);
  //   setLoading(true)
  //   try {
  //     const response = await fetch(
  //       `https://rems-backend.onrender.com/post-installment/${globalID}/${selectedRowData._id}`,

  //       //`https://rems-backend.onrender.com/delete-record/${selectedRowData._id}`,
  //       {
  //         method: "PUT",
  //         headers: {
  //           "Content-Type": "application/json",
  //         },
  //         body: JSON.stringify(installment),
  //       }
  //     );

  //     if (response.ok) {
  //       console.log("Records updated successfully");
  //       // setOpenRecordChangeSuccessfully(true);
  //       setRefresher(true);
  //     } else {
  //       console.error("Failed to update record information");
  //     }
  //   } catch (error) {
  //     console.error("An error occurred:", error);
  //   }.finally(()=>{
  //     setLoading(false);

  //   })
  // };

  const formikAddInstallment = useFormik({
    initialValues: {
      amount: "",
      date: "",

      // client_email: "",
      // client_nat_id: "",
    },

    validationSchema: validation_agentDetails,
    onSubmit: (values) => {
      console.log("NEW RECORD", values);

      console.log("incoming installments", incoming_instalments);

      console.log("SELECTED ROW DATA:->", selectedRowData);

      let totalInstallments = 0; // Define totalInstallments outside forEach loop

      const corrected_installment = values.amount.replace(/,/g, "");

      selectedRowData.installments.forEach((element, index) => {
        //console.log("installment amount", element.amount);
        totalInstallments += parseFloat(corrected_installment); // Accumulate installment amounts
      });

      console.log("incoming installment", values);

      if (
        corrected_installment <= totalInstallments ||
        totalInstallments === 0
      ) {
        console.error("Installment is greater the remaining balance");

        const currentDate = new Date();
        const options = {
          day: "numeric",
          year: "numeric",
          month: "numeric",
          //day: "numeric",
          // hour: "numeric",
          // minute: "numeric",
          // second: "numeric",
          //timeZoneName: "short",
        };

        //const formattedDate = currentDate.toLocaleDateString("en-US", options);
        console.log("DATE", values.date);

        const date_entered = new Date(values.date).toLocaleDateString(
          "en-GB",
          options
        );

        console.log("date entered", date_entered);

        const installment_record = {
          amount: corrected_installment,
          installment_type: "quarterly",
          created_at: date_entered,
        };

        console.log("installment RECORD", installment_record);

        //setInstallments(selectedRowData.installments);

        let installment_array = [];
        installment_array = incoming_instalments;

        installment_array.push(installment_record);

        console.error("new installment array", installment_array);

        postInstallment(installment_array, installment_record);

        // setTimeout(() => {
        //   setOpenInstallment(false);
        //   setOpenMainEditForm(false);
        //   reloadPage();
        // }, 2000);
      } else if (corrected_installment >= totalInstallments) {
        console.error("installment is greater than remaining balance");
        setInstallmentError(true);
      }
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikAddBonusPayment = useFormik({
    initialValues: {
      amount: "",
      date: "",

      // client_email: "",
      // client_nat_id: "",
    },

    validationSchema: validation_agentDetails,
    onSubmit: (values) => {
      console.log("NEW RECORD", values);

      console.log("incoming bonus payments", incoming_instalments);

      console.log("SELECTED ROW DATA:->", selectedRowData);

      let totalInstallments = 0; // Define totalInstallments outside forEach loop

      const corrected_installment = values.amount.replace(/,/g, "");

      selectedRowData.bonus_payments.forEach((element, index) => {
        //console.log("installment amount", element.amount);
        totalInstallments += parseFloat(corrected_installment); // Accumulate installment amounts
      });

      console.log("incoming installment", values);

      if (
        corrected_installment <= totalInstallments ||
        totalInstallments === 0
      ) {
        //console.error("Installment is greater the remaining balance");

        const currentDate = new Date();
        const options = {
          day: "numeric",
          year: "numeric",
          month: "numeric",
          //day: "numeric",
          // hour: "numeric",
          // minute: "numeric",
          // second: "numeric",
          //timeZoneName: "short",
        };

        //const formattedDate = currentDate.toLocaleDateString("en-US", options);
        console.log("DATE", values.date);

        const date_entered = new Date(values.date).toLocaleDateString(
          "en-US",
          options
        );

        console.log("date entered", date_entered);

        const bonus_payment_record = {
          amount: corrected_installment,
          installment_type: "quarterly",
          created_at: date_entered,
        };

        console.log("installment RECORD", bonus_payment_record);

        //setInstallments(selectedRowData.installments);

        let payments_array = [];
        payments_array = incoming_instalments;

        payments_array.push(bonus_payment_record);

        console.error("new payment array", payments_array);

        postBonusPayment(payments_array, bonus_payment_record);
      } else if (corrected_installment >= totalInstallments) {
        console.error("installment is greater than remaining balance");
        setInstallmentError(true);
      }
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditHouseDetails = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
      house_type: "",
      house_idx: "",
    },

    validationSchema: validationSchema_housedetails,
    onSubmit: (values) => {
      console.log("edit values to be submitted", values);

      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      setHouse_Information((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        newArray[values.house_idx] = updatedObject; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      setopenHouseEditModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditInstallment = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
      house_type: "",
      house_idx: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("edit values to be submitted", values);

      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      setHouse_Information((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        newArray[values.house_idx] = updatedObject; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      setopenHouseEditModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikAgentDetails = useFormik({
    // {information_to_be_Edited !== undefined && (
    //   <p>{information_to_be_Edited.asking_price}</p>
    // )}
    initialValues: {
      first_name: "",
      last_name: "",
      phone_number: "",
      company_name: "",
      company_email: "",
      // agent_email: "",
      // agent_nat_id: "",
    },

    validationSchema: validationSchema_agentDeatails,
    onSubmit: (values) => {
      console.log("Agent Details", values);

      //setAgentInformation(values);
      setclickedAgent(values);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      setOpenAgentForm(false);
      setOpenpopUp(false);
      //setOpenAgentAddedSuccessfully(true);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikHouseDetails = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
    },

    validationSchema: validationSchema_housedetails,
    onSubmit: (values) => {
      console.log("values", values);

      var matchFound = false;

      // Check each object in house_array

      console.log("match found", matchFound);
      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      console.log("updated object", updatedObject);
      house_array.push(updatedObject);

      const hasDuplicate = house_information.some(
        (house) => house.house_number === values.house_number
      );

      if (!hasDuplicate) {
        setHouse_Information((prevHouseInformation) => {
          return [...prevHouseInformation, ...house_array];
        });
        setOpenHouseAddedSuccessfully(true);
        // setOpenAddHouseInfor(true);
      } else if (hasDuplicate) {
        alert("HAS DUPLICATE CANNOT ADD!!");
      }

      // console.log("house array:", house_array);

      //const isDuplicate = house_array.some(values => house_information.some(info => /* Your comparison logic here */ ));
      console.log("house information", house_information);

      console.log("house array:", house_array);

      console.log("house information array", house_information);

      //const [house_information, setHouse_Information] = React.useState([]);

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      setOpenHouseDetailsModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  function handleEditHouseDetail(house, idx) {
    console.log("edit house detail called", house);

    formikEditHouseDetails.setValues({
      location: house.location,
      residence: house.residence,
      house_number: house.house_number,
      asking_price: house.asking_price,
      selling_price: house.selling_price,
      house_status: house.house_status,
      house_type: house.house_type,
      house_idx: idx,
    });

    setopenHouseEditModal(true);
  }

  function closeHouseEditModal() {
    setopenHouseEditModal(false);
  }

  function handleOpenAgentForm(option) {
    // console.log("option selected", option);
    // if (option === "AGENT") {
    //   setOpenAgentForm(true);
    // }

    console.log("selected row data", selectedRowData);

    formikAgentDetails.setValues({
      // location: house.location,
      // residence: house.residence,
      // house_number: house.house_number,
      // asking_price: house.asking_price,
      // selling_price: house.selling_price,
      // house_status: house.house_status,
      // house_type: house.house_type,

      // house_idx: idx,

      first_name: selectedRowData.broker_information.first_name,
      last_name: selectedRowData.broker_information.last_name,
      phone_number: selectedRowData.broker_information.phone_number,
      company_name: selectedRowData.broker_information.company_name,
      company_email: selectedRowData.broker_information.company_email,
    });
    setOpenAgentForm(true);
  }

  function handlecloseAgentForm() {
    setOpenAgentForm(false);
  }

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const formikEditMain = useFormik({
    initialValues: {
      client_firstname: "",
      client_lastname: "",
      client_secondname: "",
      client_house_status: "",
      kra_pin: "",
      client_phonenumber: "",
      client_email: "",
      client_nat_id: "",
      down_payment: "",
      selectedDate: "",

      // client_email: "",
      // client_nat_id: "",
    },

    validationSchema: validationSchema_clientDetails,
    onSubmit: (values) => {
      console.log("NEW RECORD", values);

      console.log("new agent information", clickedAgent);
      console.log("new house information", house_information);
      console.log("new installment information", incoming_instalments);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      //   installments: [
      //     {
      //       amount: "",
      //       installment_type: "",
      //       created_at: "",
      //     },
      //   ],

      const options_selected = {
        // weekday: "short",
        day: "numeric",
        month: "numeric",
        year: "numeric",
        // hour: "numeric",
        // minute: "numeric",
        // second: "numeric",
        //timeZoneName: "short",
      };

      const record = {
        agent_name: "Nancy",
        _id: selectedRowData._id,
        // created_at: formattedDate,
        status: selectedRowData.status,
        created_at: selectedRowData.created_at,
        client_information: {
          first_name: values.client_firstname,
          last_name: values.client_lastname,
          second_name: values.client_secondname,
          phone_number: values.client_phonenumber,
          nat_id: values.client_nat_id,
          email: values.client_email,
          created_at: selectedRowData.created_at,
          house_status: values.client_house_status,
        },
        house_information: house_information,

        broker_information: clickedAgent,
        installments: incoming_instalments,
        //date: global_date,
        date: new Date(values.selectedDate).toLocaleDateString(
          "en-GB",
          options_selected
        ),
        bonus_payments: bonusPayments,
      };

      console.log("EDITED RECORD", record);

      updateRecord(record, globalID, chosenRowData);

      // setLoading(true);
      // setTimeout(() => {
      //   setOpenMainEditForm(false);
      //   reloadPage();
      // }, 3000);

      //postData(record);

      //}

      // setOpenMainEditForm(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditInstallmentsDetails = useFormik({
    initialValues: {
      amount: "",
      installment_idx: "",
      selectedDate: "",
    },

    validationSchema: validationSchema_editinstallments,
    onSubmit: (values) => {
      //let house_array_to_be_edited = [];
      //const updatedHouseArray = [];
      console.log("edit installment values to be submitted", values);
      console.log("selected Row data", selectedRowData);

      const corrected_amount = values.amount.replace(/,/g, "");

      const options_selected = {
        // weekday: "short",
        day: "numeric",
        month: "numeric",
        year: "numeric",
        // hour: "numeric",
        // minute: "numeric",
        // second: "numeric",
        //timeZoneName: "short",
      };
      //const formattedDate = currentDate.toLocaleDateString("en-GB", options);
      const date_entered = new Date(values.date).toLocaleDateString(
        "en-GB",
        options_selected
      );
      console.log("date", date_entered);
      // const corrected_selling_price = values.selling_price.replace(/,/g, "");
      const updatedObject = {
        installment_idx: values.installment_idx,
        amount: corrected_amount,
        created_at: new Date(values.selectedDate).toLocaleDateString(
          "en-US",
          options_selected
        ),
        //created_at:
      };

      console.log("upduate object:--->", updatedObject);

      setInstallments((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        const updatedItem = {
          ...newArray[values.installment_idx],
          amount: updatedObject.amount,
          created_at: updatedObject.created_at,
        }; // Create a new object with updated amount
        newArray[values.installment_idx] = updatedItem; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      handleCloseEditInstallment();

      //   const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditBonusPaymentDetails = useFormik({
    initialValues: {
      amount: "",
      installment_idx: "",
      selectedDate: "",
    },

    validationSchema: validationSchema_editinstallments,
    onSubmit: (values) => {
      //let house_array_to_be_edited = [];
      //const updatedHouseArray = [];
      console.log("edit installment values to be submitted", values);
      console.log("selected Row data", selectedRowData);

      const corrected_amount = values.amount.replace(/,/g, "");

      const options_selected = {
        // weekday: "short",
        day: "numeric",
        month: "numeric",
        year: "numeric",
        // hour: "numeric",
        // minute: "numeric",
        // second: "numeric",
        //timeZoneName: "short",
      };
      //const formattedDate = currentDate.toLocaleDateString("en-GB", options);
      const date_entered = new Date(values.date).toLocaleDateString(
        "en-GB",
        options_selected
      );
      console.log("date", date_entered);
      // const corrected_selling_price = values.selling_price.replace(/,/g, "");
      const updatedObject = {
        installment_idx: values.installment_idx,
        amount: corrected_amount,
        created_at: new Date(values.selectedDate).toLocaleDateString(
          "en-US",
          options_selected
        ),
        //created_at:
      };

      console.log("upduate object:--->", updatedObject);

      setInstallments((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        const updatedItem = {
          ...newArray[values.installment_idx],
          amount: updatedObject.amount,
          created_at: updatedObject.created_at,
        }; // Create a new object with updated amount
        newArray[values.installment_idx] = updatedItem; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      handleCloseEditInstallment();

      //   const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  function rowMenuFunc(r) {
    console.log("test function", r);
    setIsOpen(true);
    setSelectedRowData(r);
  }

  function deleteRecordModal() {
    setOpenDeleteRecordModal(true);
  }

  function cancelRecordModal() {
    //setOpenDeleteRecordModal(true);
    setOpenCancelRecordModal(true);
  }

  function BookClientModal() {
    //setOpenCancelRecordModal(true);
    setOpenBookRecordModal(true);
  }

  //cancelRecordModal

  function OpenAddInstallmentModal() {
    setOpenInstallment(true);
  }

  function OpenAddBonusPaymentModal() {
    //setOpenBonusPayment(true);
    setEditBonus(true);
  }

  const handleOpenMainEditform = () => {
    console.log("edit form opened");

    console.log("selected row data", selectedRowData);

    setGlobalDate(selectedRowData.date);

    const dateString = selectedRowData.date; // Example date string in "DD/MM/YYYY" format
    const parts = dateString.split("/"); // Split the string into parts
    const year = parseInt(parts[2], 10); // Parse the year part as an integer
    const month = parseInt(parts[1], 10) - 1; // Parse the month part as an integer (subtract 1 since months are zero-based)
    const day = parseInt(parts[0], 10); // Parse the day part as an integer
    const dateObject = new Date(year, month, day); // Create the Date object
    console.log("incoming date", dateObject);

    formikEditMain.setValues({
      client_firstname: selectedRowData.client_information.first_name,
      client_lastname: selectedRowData.client_information.last_name,
      client_secondname: selectedRowData.client_information.second_name,
      kra_pin: selectedRowData.client_information.kra_pin,
      client_phonenumber: selectedRowData.client_information.phone_number,
      client_email: selectedRowData.client_information.email,
      client_nat_id: selectedRowData.client_information.nat_id,
      client_house_status: selectedRowData.client_information.house_status,
      installments: selectedRowData.down_payment,
      bonus_payments: selectedRowData.bonus_payments,
      selectedDate: new Date(dateObject),
      created_at: selectedRowData.created_at,
    });

    setHouse_Information(selectedRowData.house_information);
    setclickedAgent(selectedRowData.broker_information);
    setInstallments(selectedRowData.installments);
    setBonusPayments(selectedRowData.bonus_payments);
    setOpenMainEditForm(true);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openoticafion, setOpenNotification] = React.useState(false);
  // const isNotiOpen = Boolean(openoticafion);
  const [isOPen, setIsOpen] = useState(false);
  const notifactionId = "primary-notification-menu";

  const house_status = [
    {
      key: "1",
      value: "SOLD",
      label: "Sold",
    },

    {
      key: "2",
      value: "INTERESTED",
      label: "Interested",
    },
    {
      key: "3",
      value: "CANCELLED",
      label: "Cancelled",
    },

    {
      key: "4",
      value: "SOLD",
      label: "Sold",
    },
  ];
  const house_type = [
    {
      key: "1",
      value: "studio",
      label: "Studio",
    },
    {
      key: "2",
      value: "1-BR",
      label: "1-BR",
    },
    {
      key: "3",
      value: "1-BR+",
      label: "1-BR+",
    },
    {
      key: "4",
      value: "2-BR ",
      label: "2-BR ",
    },
    {
      key: "5",
      value: "2-BR+",
      label: "2-BR+",
    },
    {
      key: "6",
      value: "2-BR++ ",
      label: "2-BR++ ",
    },

    {
      key: "7",
      value: "3-BR  ",
      label: "3-BR ",
    },

    {
      key: "8",
      value: "3-BR+  ",
      label: "3-BR+ ",
    },

    {
      key: "9",
      value: "3-BR++  ",
      label: "3-BR++ ",
    },

    {
      key: "10",
      value: "4-BR",
      label: "4-BR",
    },
  ];

  function handleEditInstallmentDetail(installment, idx) {
    console.log("Handle edit installments", installment.created_at);
    formikEditInstallmentsDetails.setValues({
      amount: installment.amount,
      installment_idx: idx,
      selectedDate: installment.created_at,
    });

    //   setopenHouseEditModal(true);
    setopenInstallmentModal(true);
  }

  function handleEditBonusDetail(installment, idx) {
    console.log("Handle edit Bonus Detail", installment.created_at);
    formikEditBonusPaymentDetails.setValues({
      amount: installment.amount,
      installment_idx: idx,
      selectedDate: installment.created_at,
    });

    //   setopenHouseEditModal(true);
    //setopenInstallmentModal(true);
    setopenBonusPaymentModal(true);
  }

  function handleEditBonusPaymentDetail(installment, idx) {
    console.log("Handle edit BonusPayments", installment.created_at);
    formikEditInstallmentsDetails.setValues({
      amount: installment.amount,
      installment_idx: idx,
      selectedDate: installment.created_at,
    });

    //   setopenHouseEditModal(true);
    setopenInstallmentModal(true);
  }
  const handleCloseEditInstallment = () => {
    setopenInstallmentModal(false);
  };

  const handleCloseBonusEditingWindow = () => {
    setopenBonusPaymentModal(false);
  };

  function handleClose_pop() {
    // handleClose_pop
    setOpenDeleteRecordModal(false);
  }

  function handleClose_popBookModal() {
    // handleClose_pop
    setOpenBookRecordModal(false);
  }

  function handleClose_popCancelModal() {
    // handleClose_pop
    setOpenCancelRecordModal(false);
  }

  const deleteRecord = async (updatedRecord) => {
    const delete_property = {
      status: "INACTIVE",
    };

    console.log("Delete");

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/deactivate/${globalID}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
        handleNotificationMenuClose();
        setOpenDeleteRecordModal(false);

        setTimeout(() => {
          //setOpenInstallment(false);
          // setOpenMainEditForm(false);
          reloadPage();
        }, 1000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const bookClient = async (updatedRecord) => {
    const delete_property = {
      status: "INACTIVE",
    };

    console.log("Delete");

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/book/${globalID}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
        handleNotificationMenuClose();
        setOpenDeleteRecordModal(false);

        setTimeout(() => {
          //setOpenInstallment(false);
          // setOpenMainEditForm(false);
          reloadPage();
        }, 1000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const cancelRecord = async (updatedRecord) => {
    const delete_property = {
      status: "INACTIVE",
    };

    console.log("Delete");

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/cancel/${globalID}/${selectedRowData._id}`,

        //`http://localhost:8080/cancel/${globalID}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
        handleNotificationMenuClose();
        setOpenDeleteRecordModal(false);

        setTimeout(() => {
          //setOpenInstallment(false);
          // setOpenMainEditForm(false);
          reloadPage();
        }, 1000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const MoveToSold = async (updatedRecord) => {
    const delete_property = {
      status: "INACTIVE",
    };

    console.log("Delete");

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/move-sold/${globalID}/${selectedRowData._id}`,

        //`http://localhost:8080/cancel/${globalID}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
        handleNotificationMenuClose();
        setOpenDeleteRecordModal(false);

        setTimeout(() => {
          //setOpenInstallment(false);
          // setOpenMainEditForm(false);
          reloadPage();
        }, 1000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  // const handleClose_booking = INTERESTED() => {
  //   setOpenBooking(false);
  // };
  function handleNotificationMenuClose() {
    setIsOpen(false);
  }

  const navigate = useNavigate();

  const handleNavigate_clientInstall = () => {
    //navigate("/installments");

    //const usr_data = { usr_name: passed_username, usr_id: id };
    // login({ username: 'exampleUser' });

    // Introduce a delay using setTimeout (e.g., 2000 milliseconds or 2 seconds)
    setTimeout(() => {
      setLoading(true);
      navigate("/installments", { state: selectedRowData });

      // Navigate to the "/home" route after the delay
    }, 1000);
  };

  const renderNotification = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={notifactionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleNotificationMenuClose}
    >
      {/* <MenuItem sx={{ fontSize: [13, 16] }} onClick={OpenAddInstallmentModal}>
        Add installment
      </MenuItem> */}

      {selectedRowData &&
        selectedRowData.client_information.house_status != "CANCELLED" &&
        selectedRowData.client_information.house_status != "FULLY-SOLD" && (
          //selectedRowData.client_information.house_status != "INTERESTED" &&

          <MenuItem
            sx={{ fontSize: [13, 16] }}
            onClick={OpenAddInstallmentModal}
          >
            Add installment
          </MenuItem>
        )}

      <MenuItem
        sx={{ fontSize: [13, 16] }}
        onClick={handleNavigate_clientInstall}
      >
        View Installments
      </MenuItem>

      {selectedRowData &&
        selectedRowData.client_information.house_status != "CANCELLED" &&
        selectedRowData.client_information.house_status != "SOLD" &&
        selectedRowData.client_information.house_status == "INTERESTED" && (
          <MenuItem sx={{ fontSize: [13, 16] }} onClick={BookClientModal}>
            Book client{" "}
          </MenuItem>
        )}

      <MenuItem sx={{ fontSize: [13, 16] }} onClick={handleOpenMainEditform}>
        Edit Record
      </MenuItem>

      {selectedRowData &&
        selectedRowData.client_information.house_status != "CANCELLED" &&
        selectedRowData.client_information.house_status != "INTERESTED" && (
          <MenuItem
            sx={{ fontSize: [13, 16] }}
            onClick={OpenAddBonusPaymentModal}
          >
            Add Bonus Payment
          </MenuItem>
        )}

      {selectedRowData &&
        selectedRowData.client_information.house_status != "CANCELLED" && (
          <MenuItem sx={{ fontSize: [13, 16] }} onClick={cancelRecordModal}>
            Cancel Record
          </MenuItem>
        )}

      <MenuItem sx={{ fontSize: [13, 16] }} onClick={deleteRecordModal}>
        Delete Record
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <React.Fragment>
        {/* {loading && <Loader2 />} */}
        <div className="flex flex-col top-[50%] right-[50%] justify-center items-center absolute">
        {loading && <Spinner1 className="z-50" />}</div>
        <RecordUpdatedSuccefull
          open={openRecordChangeSuccessfully}
          autoHideDuration={3000}
          onClose={() => setOpenRecordChangeSuccessfully(false)}
        />

        <InstallmentError
          open={installmentError}
          autoHideDuration={10000}
          onClose={() => setInstallmentError(false)}
        />

        <Modal
          open={openAgentForm}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
              <h2 className="font-bold m-4"> Agent's Details </h2>
              <CloseIcon
                fontSize="40"
                onClick={handlecloseAgentForm}
                sx={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={formikAgentDetails.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3 mt-3">
                  <TextField
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="First Name"
                    placeholder="First Name"
                    value={formikAgentDetails.values.first_name}
                    error={
                      formikAgentDetails.touched.first_name &&
                      Boolean(formikAgentDetails.errors.first_name)
                    }
                    helperText={
                      formikAgentDetails.touched.first_name &&
                      formikAgentDetails.errors.first_name
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikAgentDetails.handleChange(e);
                    }}
                    onBlur={formikAgentDetails.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikAgentDetails.values.last_name}
                    error={
                      formikAgentDetails.touched.last_name &&
                      Boolean(formikAgentDetails.errors.last_name)
                    }
                    helperText={
                      formikAgentDetails.touched.last_name &&
                      formikAgentDetails.errors.last_name
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikAgentDetails.handleChange(e);
                    }}
                    onBlur={formikAgentDetails.handleBlur}
                  />

                  {/* <TextField
                  type="text"
                  id="agent_nat_id"
                  name="client_nat_id"
                  label="ID Number"
                  placeholder="ID Number"
                  value={formikAgentDetails.values.agent_nat_id}
                  onChange={formikAgentDetails.handleChange}
                  onBlur={formikAgentDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="agent_email"
                  name="agent_email"
                  label="email"
                  placeholder="Agent's Email"
                  value={formikAgentDetails.values.agent_email}
                  onChange={formikAgentDetails.handleChange}
                  onBlur={formikAgentDetails.handleBlur}
                /> */}
                </div>

                <TextField
                  type="text"
                  id="phone_number"
                  name="phone_number"
                  label="Phone Number"
                  placeholder="Phone Number"
                  value={formikAgentDetails.values.phone_number}
                  //onChange={formikAgentDetails.handleChange}
                  error={
                    formikAgentDetails.touched.phone_number &&
                    Boolean(formikAgentDetails.errors.phone_number)
                  }
                  helperText={
                    formikAgentDetails.touched.phone_number &&
                    formikAgentDetails.errors.phone_number
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikAgentDetails.handleChange(e);
                  }}
                  onBlur={formikAgentDetails.handleBlur}
                />

                <div className="flex flex-row gap-3 mt-3">
                  <TextField
                    type="text"
                    id="company_name"
                    name="company_name"
                    label="Company Name"
                    placeholder="Company Name"
                    value={formikAgentDetails.values.company_name}
                    //onChange={formikAgentDetails.handleChange}
                    error={
                      formikAgentDetails.touched.company_name &&
                      Boolean(formikAgentDetails.errors.company_name)
                    }
                    helperText={
                      formikAgentDetails.touched.company_name &&
                      formikAgentDetails.errors.company_name
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikAgentDetails.handleChange(e);
                    }}
                    onBlur={formikAgentDetails.handleBlur}
                  />
                  <TextField
                    type="text"
                    id="company_email"
                    name="company_email"
                    label="Company Email"
                    placeholder="Company Email"
                    value={formikAgentDetails.values.company_email}
                    //onChange={formikAgentDetails.handleChange}
                    error={
                      formikAgentDetails.touched.company_email &&
                      Boolean(formikAgentDetails.errors.company_email)
                    }
                    helperText={
                      formikAgentDetails.touched.company_email &&
                      formikAgentDetails.errors.company_email
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value;
                      formikAgentDetails.handleChange(e);
                    }}
                    onBlur={formikAgentDetails.handleBlur}
                  />
                </div>

                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save Agent
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>

        <Modal
          open={opendeleteRecordModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              <h2 className=" m-4">
                <span className="font-bold m-2">Booking Confirmation</span>
                <br></br>{" "}
                <span>Are you sure you want to delete this Record?</span>
              </h2>

              {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
            </div>

            <form>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3 mt-3"></div>
                <div className="flex flex-row gap-4">
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={deleteRecord}
                  >
                    Delete
                  </Button>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleClose_pop}
                  >
                    cancel
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>

        <Modal
          open={openbookClientModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              <h2 className=" m-4">
                <span className="font-bold m-2">Delete Confirmation</span>
                <br></br>{" "}
                <span>Are you sure you want to Book this Client?</span>
              </h2>

              {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
            </div>

            <form>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3 mt-3"></div>
                <div className="flex flex-row gap-4">
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={bookClient}
                  >
                    Yes
                  </Button>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleClose_popBookModal}
                  >
                    No
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>

        <Modal
          open={opencancelRecordModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              <h2 className=" m-4">
                <span className="font-bold m-2">Cancel Confirmation</span>
                <br></br>{" "}
                <span>Are you sure you want to Cancel this Record?</span>
              </h2>

              {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
            </div>

            <form>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3 mt-3"></div>
                <div className="flex flex-row gap-4">
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={cancelRecord}
                  >
                    yes
                  </Button>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleClose_popCancelModal}
                  >
                    No
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>

        <Modal
          open={openHouseDetailsModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              <h2 className="font-bold m-4"> Add House Information </h2>
              <CloseIcon
                fontSize="40"
                onClick={handleCloseHouseDetailsModal}
                sx={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={formikHouseDetails.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3 mt-3 mb-4">
                  <TextField
                    type="text"
                    id="location"
                    name="location"
                    label="Location"
                    placeholder="Location"
                    value={formikHouseDetails.values.location}
                    error={
                      formikHouseDetails.touched.location &&
                      Boolean(formikHouseDetails.errors.location)
                    }
                    helperText={
                      formikHouseDetails.touched.location &&
                      formikHouseDetails.errors.location
                    }
                    //onChange={formikHouseDetails.handleChange}

                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikHouseDetails.handleChange(e);
                    }}
                    onBlur={formikHouseDetails.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="residence"
                    name="residence"
                    label="House Residence"
                    placeholder="house residence"
                    value={formikHouseDetails.values.residence}
                    error={
                      formikHouseDetails.touched.residence &&
                      Boolean(formikHouseDetails.errors.residence)
                    }
                    helperText={
                      formikHouseDetails.touched.residence &&
                      formikHouseDetails.errors.residence
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikHouseDetails.handleChange(e);
                    }}
                    onBlur={formikHouseDetails.handleBlur}
                  />
                </div>
              </div>

              <TextField
                type="text"
                id="house_number"
                name="house_number"
                label="house unit"
                placeholder="House Unit"
                value={formikHouseDetails.values.house_number}
                //onChange={formikHouseDetails.handleChange}
                error={
                  formikHouseDetails.touched.house_number &&
                  Boolean(formikHouseDetails.errors.house_number)
                }
                helperText={
                  formikHouseDetails.touched.house_number &&
                  formikHouseDetails.errors.house_number
                }
                onChange={(e) => {
                  e.target.value = e.target.value.toUpperCase();
                  formikHouseDetails.handleChange(e);
                }}
                onBlur={formikHouseDetails.handleBlur}
              />

              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type="text"
                  id="asking_price"
                  name="asking_price"
                  label="Asking Price"
                  placeholder="Asking Price"
                  error={
                    formikHouseDetails.touched.asking_price &&
                    Boolean(formikHouseDetails.errors.asking_price)
                  }
                  helperText={
                    formikHouseDetails.touched.asking_price &&
                    formikHouseDetails.errors.asking_price
                  }
                  value={formikHouseDetails.values.asking_price}
                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikHouseDetails.setFieldValue(
                      "asking_price",
                      formattedValue
                    );
                  }}
                  onBlur={formikHouseDetails.handleBlur}
                />
                <TextField
                  type="text"
                  id="selling_price"
                  name="selling_price"
                  label="Selling Price"
                  placeholder="Selling Price"
                  error={
                    formikHouseDetails.touched.selling_price &&
                    Boolean(formikHouseDetails.errors.selling_price)
                  }
                  helperText={
                    formikHouseDetails.touched.selling_price &&
                    formikHouseDetails.errors.selling_price
                  }
                  value={formikHouseDetails.values.selling_price}
                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikHouseDetails.setFieldValue(
                      "selling_price",
                      formattedValue // Corrected to set "selling_price" instead of "asking_price"
                    );
                  }}
                  onBlur={formikHouseDetails.handleBlur}
                />
              </div>

              <div className="flex flex-col gap-3 mt-3">
                {/* <FormControl>
                  <TextField
                    id="house_status"
                    select
                    name="house_status"
                    label="Status"
                    placeholder="Hs Price"
                    error={
                      formikHouseDetails.touched.house_status &&
                      Boolean(formikHouseDetails.errors.house_status)
                    }
                    helperText={
                      formikHouseDetails.touched.house_status &&
                      formikHouseDetails.errors.house_status
                    }
                    fullWidth
                    // value={formikHouseDetails.values.house_status}
                    onChange={formikHouseDetails.handleChange}
                    onBlur={formikHouseDetails.handleBlur}
                  >
                    {house_status.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl> */}
                <FormControl>
                  <TextField
                    id="house_type"
                    select
                    name="house_type"
                    label="House type"
                    placeholder="house_type"
                    fullWidth
                    error={
                      formikHouseDetails.touched.house_type &&
                      Boolean(formikHouseDetails.errors.house_type)
                    }
                    helperText={
                      formikHouseDetails.touched.house_type &&
                      formikHouseDetails.errors.house_type
                    }
                    // value={formikHouseDetails.values.house_status}
                    onChange={formikHouseDetails.handleChange}
                    onBlur={formikHouseDetails.handleBlur}
                  >
                    {house_type.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                    // onClick={handleOpen_AddHouseInfor}
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>
        <TableRow
          sx={{
            "& > *": { borderBottom: "unset" },
            "&:hover": { backgroundColor: "lightgrey" },
          }}
          // onMouseEnter={() => {
          //   setSelectedRowData(row);
          //   setIndex(index);
          // }}
        >
          <TableCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell
            component="th"
            scope="row"
            className="select-none"
            sx={{ fontSize: ["10px", "15px"] }}
          >
            {row.client_information.first_name}{" "}
            {row.client_information.last_name}
          </TableCell>
          <TableCell
            align="left"
            className="select-none"
            sx={{
              fontSize: ["10px", "15px"],
              color:
                row.client_information.house_status === "BOOKED"
                  ? "blue"
                  : row.client_information.house_status === "INTERESTED"
                  ? "orange"
                  : row.client_information.house_status === "SOLD"
                  ? "green"
                  : row.client_information.house_status === "CANCELLED"
                  ? "red"
                  : row.client_information.house_status === "FULLY-SOLD"
                  ? "#5f00ff"
                  : "inherit", // If status is neither BOOKED nor Interested, inherit the default color
            }}
          >
            {row.client_information.house_status}
          </TableCell>
          {/* 
          <TableCell
            align="left"
            className="select-none"
            sx={{ fontSize: ["10px", "15px"] }}
          >
            {row.client_information.house_status}
          </TableCell> */}
          <TableCell
            align="right"
            className="select-none"
            sx={{ fontSize: ["10px", "15px"] }}
          >
            {row.date}
          </TableCell>
          <TableCell
            align="right"
            className="select-none"
            sx={{ fontSize: ["10px", "15px"] }}
          >
            {row.installments
              .reduce(
                (total, installment) => total + parseFloat(installment.amount),
                0
              )
              .toLocaleString()}
          </TableCell>

          <TableCell
            align="right"
            className="select-none"
            sx={{
              fontSize: ["10px", "15px"],
              color:
                row.client_information.house_status === "BOOKED" ||
                //  row.client_information.house_status === "FULLY-SOLD" ||
                row.client_information.house_status === "SOLD"
                  ? "inherit"
                  : "red",
            }}
          >
            {row.client_information.house_status === "BOOKED" ||
            row.client_information.house_status === "SOLD" ||
            row.client_information.house_status === "FULLY-SOLD" ||
            row.client_information.house_status === "CANCELLED" ? (
              (
                row.house_information.reduce(
                  (total, house) => total + parseFloat(house.selling_price),
                  0
                ) -
                row.installments.reduce(
                  (total, installment) =>
                    total + parseFloat(installment.amount),
                  0
                )
              ).toLocaleString()
            ) : (
              <div>Unaivailable</div>
            )}
          </TableCell>

          <TableCell
            align="right"
            className="select-none"
            sx={{ fontSize: ["10px", "15px"] }}
          >
            {row.house_information
              .reduce(
                (total, house) =>
                  total + parseFloat(house.selling_price) / 1000000,
                0
              )
              .toLocaleString()}{" "}
            m
            {/* {props.totalSellingPrice !== undefined
              ? (props.totalSellingPrice / 1000000).toLocaleString(undefined, {
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                }) + "m"
              : "Loading"} */}
          </TableCell>
          <TableCell align="right">
            <IconButton aria-controls={notifactionId}>
              <MoreVertIcon onClick={() => rowMenuFunc(row)} />
            </IconButton>
          </TableCell>
          {renderNotification}
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography
                  variant="h6"
                  gutterBottom
                  component="div"
                  className="select-none"
                >
                  House Information
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead sx={{ bgcolor: "#242333" }}>
                    <TableRow>
                      <TableCell sx={{ color: "#fff" }} className="select-none">
                        Residence
                      </TableCell>
                      <TableCell sx={{ color: "#fff" }} className="select-none">
                        Location
                      </TableCell>
                      <TableCell
                        align="right"
                        sx={{ color: "#fff" }}
                        className="select-none"
                      >
                        Unit
                      </TableCell>
                      <TableCell
                        align=""
                        sx={{ color: "#fff" }}
                        className="select-none"
                      >
                        Asking Price
                      </TableCell>
                      <TableCell
                        align="center"
                        sx={{ color: "#fff" }}
                        className="select-none"
                      >
                        Selling Price
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.house_information.map((innerRow, index) => (
                      // <Dropdown
                      //   key={index}
                      //   menu={{ items }}
                      //   trigger={["contextMenu"]}
                      //   placement="bottomLeft"
                      // >
                      <TableRow
                        key={innerRow.date}
                        sx={{ "&:hover": { backgroundColor: "lightblue" } }}
                        onMouseEnter={() => {
                          setSelectedRowData(row);
                          setIndex(index);
                        }}
                      >
                        <TableCell
                          component="th"
                          scope="row"
                          className="select-none"
                        >
                          {innerRow.residence}
                        </TableCell>
                        <TableCell className="select-none">
                          {innerRow.location}
                        </TableCell>
                        <TableCell
                          sx={{ fontSize: [12, 10] }}
                          align="right"
                          className="select-none"
                        >
                          {innerRow.house_number}
                        </TableCell>

                        <TableCell
                          align=""
                          sx={{ color: "gree" }}
                          className="select-none"
                        >
                          {parseFloat(innerRow.asking_price).toLocaleString()}
                        </TableCell>
                        <TableCell align="center" className="select-none">
                          {/* {innerRow.selling_price} */}

                          {parseFloat(innerRow.selling_price).toLocaleString()}
                        </TableCell>
                      </TableRow>
                      // </Dropdown>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>

        <Modal open={openInstallment}>
          <Box sx={style_box}>
            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginBottom: 2 }}
            >
              Add New Installment{" "}
            </Typography>

            <Typography>
              Name:{" "}
              {selectedRowData &&
                selectedRowData.client_information &&
                `${selectedRowData.client_information.first_name} ${selectedRowData.client_information.last_name}`}
            </Typography>

            <Typography>
              Balance:{" "}
              {selectedRowData &&
                selectedRowData.house_information &&
                (
                  selectedRowData.house_information.reduce((total, house) => {
                    return total + parseFloat(house.selling_price);
                  }, 0) -
                  (selectedRowData &&
                    selectedRowData.installments &&
                    selectedRowData.installments.reduce((total, amount) => {
                      return total + parseFloat(amount.amount);
                    }, 0))
                ).toLocaleString()}
            </Typography>

            <Typography variant="body2" color="text.secondary">
              <div>
                <form onSubmit={formikAddInstallment.handleSubmit}>
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="amount"
                      name="amount"
                      label="Amount"
                      placeholder="Amount"
                      value={formikAddInstallment.values.amount}
                      //onChange={formikAddInstallment.handleChange}

                      onChange={(e) => {
                        const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                        const formattedValue = rawValue
                          .replace(/\D/g, "")
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                        formikAddInstallment.setFieldValue(
                          "amount",
                          formattedValue // Corrected to set "selling_price" instead of "asking_price"
                        );
                      }}
                      onBlur={formikAddInstallment.handleBlur}
                      error={
                        formikAddInstallment.touched.amount &&
                        Boolean(formikAddInstallment.errors.amount)
                      }
                      helperText={
                        formikAddInstallment.touched.amount &&
                        formikAddInstallment.errors.amount
                      }
                    />
                  </div>

                  <div className="flex flex-row gap-3 mt-3">
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DemoContainer components={["DatePicker"]}>
                        <DatePicker
                          label="Enter Date"
                          value={formikAddInstallment.values.date} // Pass value from Formik
                          onChange={(date) =>
                            formikAddInstallment.setFieldValue("date", date)
                          } // Update Formik field value
                          // maxDate={today}
                          // maxDate={new Date()}
                        />
                      </DemoContainer>
                    </LocalizationProvider>
                  </div>

                  <div className="flex flex-row gap-3 mt-3">
                    <Button
                      variant="contained"
                      sx={{ backgroundColor: "#242333" }}
                      size="small"
                      type="submit"
                    >
                      Save
                    </Button>
                    <Button
                      variant="contained"
                      sx={{ backgroundColor: "#242333" }}
                      size="small"
                      onClick={handleclose_installment}
                    >
                      close
                    </Button>
                  </div>
                </form>
              </div>
            </Typography>
          </Box>
        </Modal>

        <Modal open={openEditBonus}>
          <Box sx={style_box}>
            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginBottom: 2 }}
            >
              Add New Bonus Payment{" "}
            </Typography>

            <Typography>
              Name:{" "}
              {selectedRowData &&
                selectedRowData.client_information &&
                `${selectedRowData.client_information.first_name} ${selectedRowData.client_information.last_name}`}
            </Typography>

            <Typography>
              House Info:{" "}
              {selectedRowData &&
                selectedRowData.house_information &&
                selectedRowData.house_information.map((house, index) => (
                  <span key={index}>
                    {` ${house.house_number}, ${house.residence}`}
                    {index !== selectedRowData.house_information.length - 1 &&
                      ", "}
                  </span>
                ))}
            </Typography>

            <Typography>
              AGENT:{" "}
              {selectedRowData &&
              selectedRowData.broker_information.first_name === ""
                ? "NO"
                : "YES"}
            </Typography>

            <Typography>
              Amount Paid:{" "}
              {selectedRowData &&
                selectedRowData.bonus_payments &&
                selectedRowData.bonus_payments
                  .reduce((total, amount) => {
                    const parsedAmount = parseFloat(amount.amount);
                    if (!isNaN(parsedAmount)) {
                      return total + parsedAmount;
                    } else {
                      return total;
                    }
                  }, 0)
                  .toLocaleString()}
            </Typography>

            <Typography>
              Balance:{" "}
              {selectedRowData &&
                selectedRowData.house_information &&
                (
                  selectedRowData.house_information.reduce((total, house) => {
                    return (
                      total +
                      (selectedRowData.broker_information &&
                      selectedRowData.broker_information.first_name === ""
                        ? parseFloat(house.selling_price * 0.005)
                        : 7000)
                    );
                  }, 0) -
                  (selectedRowData.bonus_payments &&
                    selectedRowData.bonus_payments.reduce((total, amount) => {
                      const parsedAmount = parseFloat(amount.amount);
                      return isNaN(parsedAmount) ? total : total + parsedAmount;
                    }, 0))
                ).toLocaleString()}
            </Typography>

            <Typography variant="body2" color="text.secondary">
              <div>
                <form onSubmit={formikAddBonusPayment.handleSubmit}>
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="amount"
                      name="amount"
                      label="Amount"
                      placeholder="Amount"
                      value={formikAddBonusPayment.values.amount}
                      //onChange={formikAddInstallment.handleChange}

                      onChange={(e) => {
                        const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                        const formattedValue = rawValue
                          .replace(/\D/g, "")
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                        formikAddBonusPayment.setFieldValue(
                          "amount",
                          formattedValue // Corrected to set "selling_price" instead of "asking_price"
                        );
                      }}
                      onBlur={formikAddBonusPayment.handleBlur}
                      error={
                        formikAddBonusPayment.touched.amount &&
                        Boolean(formikAddBonusPayment.errors.amount)
                      }
                      helperText={
                        formikAddBonusPayment.touched.amount &&
                        formikAddBonusPayment.errors.amount
                      }
                    />
                  </div>

                  <div className="flex flex-row gap-3 mt-3">
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DemoContainer components={["DatePicker"]}>
                        <DatePicker
                          label="Enter Date"
                          value={formikAddBonusPayment.values.date} // Pass value from Formik
                          onChange={(date) =>
                            formikAddBonusPayment.setFieldValue("date", date)
                          } // Update Formik field value
                          // maxDate={today}
                          // maxDate={new Date()}
                        />
                      </DemoContainer>
                    </LocalizationProvider>
                  </div>

                  <div className="flex flex-row gap-3 mt-3">
                    <Button
                      variant="contained"
                      sx={{ backgroundColor: "#242333" }}
                      size="small"
                      type="submit"
                    >
                      Save
                    </Button>
                    <Button
                      variant="contained"
                      sx={{ backgroundColor: "#242333" }}
                      size="small"
                      onClick={handleclose_bonus_payment}
                    >
                      close
                    </Button>
                  </div>
                </form>
              </div>
            </Typography>
          </Box>
        </Modal>

        <Modal
          open={openMainEditForm}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
                Edit Record
              </Typography>
              {/* <h2 className="font-bold m-4"> Add Record </h2> */}
              <CloseIcon
                fontSize="40"
                onClick={handleCloseMainEditform}
                sx={{ cursor: "pointer" }}
              />
            </div>
            Edit Record
            <form onSubmit={formikEditMain.handleSubmit}>
              <div className="flex flex-col gap-4  ">
                <div className="flex flex-col gap-3">
                  <Typography
                    id="modal-modal-title"
                    sx={{ fontWeight: "bold" }}
                  >
                    Edit Client's Details
                  </Typography>
                  <div className="flex flex-col gap-3">
                    <div className="flex flex-row gap-3 mt-3">
                      <TextField
                        type="text"
                        id="client_firstname"
                        name="client_firstname"
                        label="First Name"
                        placeholder="First Name"
                        value={formikEditMain.values.client_firstname}
                        // onChange={formikMain.handleChange}

                        error={
                          formikEditMain.touched.client_firstname &&
                          Boolean(formikEditMain.errors.client_firstname)
                        }
                        helperText={
                          formikEditMain.touched.client_firstname &&
                          formikEditMain.errors.client_firstname
                        }
                        onChange={(e) => {
                          e.target.value = e.target.value.toUpperCase();
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />

                      <TextField
                        type="text"
                        id="client_secondname"
                        name="client_secondname"
                        label="Second Name"
                        placeholder="Second Name"
                        value={formikEditMain.values.client_secondname}
                        //onChange={formikMain.handleChange}

                        error={
                          formikEditMain.touched.client_secondname &&
                          Boolean(formikEditMain.errors.client_secondname)
                        }
                        helperText={
                          formikEditMain.touched.client_secondname &&
                          formikEditMain.errors.client_secondname
                        }
                        onChange={(e) => {
                          e.target.value = e.target.value.toUpperCase();
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />
                    </div>
                    <TextField
                      type="text"
                      id="client_lastname"
                      name="client_lastname"
                      label="Last Name"
                      placeholder="Last Name"
                      value={formikEditMain.values.client_lastname}
                      // onChange={formikMain.handleChange}
                      // error={
                      //   formikEditMain.touched.client_lastname &&
                      //   Boolean(formikEditMain.errors.client_lastname)
                      // }
                      // helperText={
                      //   formikEditMain.touched.client_lastname &&
                      //   formikEditMain.errors.client_lastname
                      // }

                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikEditMain.handleChange(e);
                      }}
                      onBlur={formikEditMain.handleBlur}
                    />

                    <div className="flex flex-row gap-3 mt-3">
                      <TextField
                        type="text"
                        id="client_phonenumber"
                        name="client_phonenumber"
                        label="Phone Number"
                        placeholder="Phone Number"
                        value={formikEditMain.values.client_phonenumber}
                        //onChange={formikMain.handleChange}

                        error={
                          formikEditMain.touched.client_phonenumber &&
                          Boolean(formikEditMain.errors.client_phonenumber)
                        }
                        helperText={
                          formikEditMain.touched.client_phonenumber &&
                          formikEditMain.errors.client_phonenumber
                        }
                        onChange={(e) => {
                          e.target.value = e.target.value;
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />
                      <TextField
                        type="text"
                        id="client_nat_id"
                        name="client_nat_id"
                        label="ID Number"
                        placeholder="ID Number"
                        value={formikEditMain.values.client_nat_id}
                        //onChange={formikMain.handleChange}

                        error={
                          formikEditMain.touched.client_nat_id &&
                          Boolean(formikEditMain.errors.client_nat_id)
                        }
                        helperText={
                          formikEditMain.touched.client_nat_id &&
                          formikEditMain.errors.client_nat_id
                        }
                        onChange={(e) => {
                          e.target.value = e.target.value.toUpperCase();
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />
                      {/* <TextField
                        type="text"
                        id="client_email"
                        name="client_email"
                        label="email"
                        placeholder="Client's Email"
                        value={formikEditMain.values.client_email}
                        onChange={formikEditMain.handleChange}
                        // onChange={(e) => {
                        //   e.target.value = e.target.value.toUpperCase();
                        //   formikMain.handleChange(e);
                        // }}
                        onBlur={formikEditMain.handleBlur}
                      /> */}
                    </div>
                    <div className="flex flex-row gap-3 mt-3">
                      <TextField
                        type="text"
                        id="client_email"
                        name="client_email"
                        label="Email"
                        placeholder="Email"
                        value={formikEditMain.values.client_email}
                        //onChange={formikMain.handleChange}

                        error={
                          formikEditMain.touched.client_email &&
                          Boolean(formikEditMain.errors.client_email)
                        }
                        helperText={
                          formikEditMain.touched.client_email &&
                          formikEditMain.errors.client_email
                        }
                        onChange={(e) => {
                          e.target.value = e.target.value;
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />
                      <TextField
                        type="text"
                        id="kra_pin"
                        name="kra_pin"
                        label="KRA PIN"
                        placeholder="ID Number"
                        value={formikEditMain.values.kra_pin}
                        //onChange={formikMain.handleChange}
                        onChange={(e) => {
                          e.target.value = e.target.value.toUpperCase();
                          formikEditMain.handleChange(e);
                        }}
                        onBlur={formikEditMain.handleBlur}
                      />
                    </div>

                    <TextField
                      type="text"
                      id="date"
                      name="date"
                      label="Booking Date"
                      placeholder="Client's Email"
                      value={(() => {
                        const selectedDate = new Date(
                          formikEditMain.values.selectedDate
                        );
                        const months = [
                          "Jan",
                          "Feb",
                          "Mar",
                          "Apr",
                          "May",
                          "Jun",
                          "Jul",
                          "Aug",
                          "Sep",
                          "Oct",
                          "Nov",
                          "Dec",
                        ];
                        return `${
                          months[selectedDate.getMonth()]
                        } ${selectedDate.getDate()}, ${selectedDate.getFullYear()}`;
                      })()}
                      // value={new Date(formikEditMain.values.selectedDate).toLocaleDateString()}
                      onChange={formikEditMain.handleChange}
                      // onChange={(e) => {
                      //   e.target.value = e.target.value.toUpperCase();
                      //   formikMain.handleChange(e);
                      // }}
                      onBlur={formikEditMain.handleBlur}
                    />

                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DemoContainer components={["DatePicker"]}>
                        <DatePicker
                          label="Edit Date"
                          //value={formikEditMain.values.selectedDate} // Pass value from Formik
                          onChange={(date) =>
                            formikEditMain.setFieldValue("selectedDate", date)
                          } // Update Formik field value
                          // maxDate={today}
                          // maxDate={new Date()}
                        />
                      </DemoContainer>
                    </LocalizationProvider>
                  </div>

                  <CardMedia height="540" />
                  <CardContent>
                    <Typography
                      id="modal-modal-title"
                      sx={{ fontWeight: "bold", marginBottom: 2 }}
                    >
                      Edit House Details
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      <div>
                        {house_information.length > 0 ? (
                          <ul>
                            {house_information.map((house_info, index) => (
                              <li
                                key={index}
                                style={{
                                  border: "1px solid #ccc",
                                  padding: "8px",
                                  marginBottom: "8px",
                                }}
                              >
                                <span style={{ marginRight: "8px" }}>
                                  &#8226;
                                </span>
                                <span className="text-[#000]">Loc:&nbsp;</span>
                                {house_info.location}, &nbsp;{" "}
                                <span className="text-[#000]">Res:&nbsp;</span>
                                {house_info.residence},&nbsp;{" "}
                                <span className="text-[#000]">Type:&nbsp;</span>
                                {house_info.house_type},&nbsp;{" "}
                                <span className="text-[#000]">Sp: &nbsp;</span>
                                Kshs{" "}
                                {parseFloat(
                                  house_info.selling_price
                                ).toLocaleString()}
                                ,&nbsp;{" "}
                                <span className="text-[#000]">HsNo:&nbsp;</span>
                                {house_info.house_number},{" "}
                                <div className="flex flex-row justify-between mt-1">
                                  <LightTooltip title="Edit house ">
                                    {/* <EditIcon
                                        sx={{
                                          color: "#fff",
                                          cursor: "pointer",
                                          fontSize: "20px",
                                        }}
                                        onClick={() =>
                                          handleEditHouseDetail(
                                            house_info,
                                            index
                                          )
                                        }
                                      /> */}

                                    <Button
                                      variant="contained"
                                      // sx={{ backgroundColor: "red" }}
                                      size="small"
                                      onClick={() =>
                                        handleEditHouseDetail(house_info, index)
                                      }
                                    >
                                      EDIT
                                    </Button>
                                  </LightTooltip>

                                  <LightTooltip title="Delete house "></LightTooltip>
                                </div>
                              </li>
                            ))}
                          </ul>
                        ) : (
                          <div>
                            <ul>
                              {/* {house_information.map((house_info, index) => ( */}
                              <li
                                // key={index}
                                style={{
                                  border: "1px solid #ccc",
                                  padding: "8px",
                                  marginBottom: "8px",
                                }}
                              >
                                <span style={{ marginRight: "8px" }}>
                                  &#8226;
                                </span>
                                No Houses Added!
                              </li>
                              {/* ))} */}
                            </ul>
                          </div>
                        )}
                      </div>
                    </Typography>

                    <CardActions>
                      {/* <div className="flex flex-row gap-6"> */}
                      <Button
                        variant="contained"
                        sx={{ backgroundColor: "#242333", marginTop: 1 }}
                        size="small"
                        onClick={handleOpenModal}
                      >
                        + Add Houses
                      </Button>
                      {/* </div> */}
                    </CardActions>
                  </CardContent>
                </div>
              </div>

              <Typography
                id="modal-modal-title"
                sx={{ fontWeight: "bold", marginTop: 2 }}
              >
                Agent's Information
              </Typography>
              <div className="flex text-center justify-center mt-4 ">
                <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
                  <Search>
                    <SearchIconWrapper>
                      <SearchIcon />
                    </SearchIconWrapper>
                    <StyledInputBase
                      //onClick={handleSearchModal}
                      //onClick={aboutToSearch}
                      placeholder="Type Client Name"
                      // placeholder={searchType || "Search Name..."}
                      inputProps={{ "aria-label": "search" }}
                      value={searchValue}
                      onChange={(e) => handleChange(e.target.value)}
                    />
                  </Search>
                  {/* 
            <Button variant="contained" onClick={handleSearchModal}>
              Search
            </Button> */}
                </Box>
              </div>

              <div
                className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]"
                // style={scroll}
              >
                {filteredResults.map((result, id) => {
                  // return <SearchResult result={result.name} key={id} />;
                  return (
                    <div
                      className="flex flex-row gap-2 hover:bg-blue-400 p-2 cursor-pointer"
                      key={id}
                      onClick={() =>
                        handleSearchResultItem(result.broker_information)
                      }
                    >
                      {result.broker_information.first_name}{" "}
                      {result.broker_information.last_name}
                    </div>
                  );
                })}
              </div>

              <CardMedia height="540" />
              <CardContent>
                {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}

                <Typography variant="body2" color="text.secondary">
                  {agent_information && (
                    <p>First Name: {agent_information.agent_firstname}</p>
                  )}
                  <div>
                    <ul>
                      {clickedAgent ? (
                        <li
                          style={{
                            border: "1px solid #ccc",
                            padding: "8px",
                            marginBottom: "8px",
                          }}
                        >
                          <span style={{ marginRight: "8px" }}>&#8226;</span>
                          <div>
                            {clickedAgent.first_name} {clickedAgent.last_name}
                            ,&nbsp;&nbsp;
                            {clickedAgent.phone_number}&nbsp;&nbsp;
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "red", marginLeft: 22 }}
                              size="small"
                              type="submit"
                              onClick={handleRemoveAgent}
                            >
                              Remove
                            </Button>
                          </div>
                        </li>
                      ) : (
                        <li>No Agents!</li>
                      )}
                    </ul>
                  </div>
                </Typography>

                <CardActions>
                  <div className="flex flex-row gap-6">
                    <Button
                      variant="contained"
                      sx={{ backgroundColor: "#242333" }}
                      size="small"
                      onClick={handleOpenAgentForm}
                    >
                      + Edit Agent
                    </Button>

                    {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
                  </div>
                </CardActions>
              </CardContent>

              <h2 className="font-bold m"> Edit Installments </h2>

              <CardMedia height="540" />
              <CardContent>
                {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}
                <Typography variant="body2" color="text.secondary">
                  <div>
                    {incoming_instalments.length > 0 ? (
                      <ul>
                        {incoming_instalments.map((installment, index) => (
                          <li
                            key={index}
                            style={{
                              border: "1px solid #ccc",
                              padding: "8px",
                              marginBottom: "8px",
                            }}
                            className="flex flex-col gap-4 justify-center"
                          >
                            <div>
                              {/* <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span> */}
                              {/* Installment {index + 1}: &nbsp;{" "} */}
                              <div className="flex flex-col">
                                &#8226;{installment.created_at}
                                &nbsp; KES &nbsp;
                                {parseFloat(
                                  installment.amount
                                ).toLocaleString()}
                              </div>
                            </div>

                            <div className="flex flex-row space-x-2">
                              <Button
                                variant="contained"
                                sx={{ backgroundColor: "#242333" }}
                                size="small"
                                onClick={() =>
                                  handleEditInstallmentDetail(
                                    installment,
                                    index
                                  )
                                }
                              >
                                Edit
                              </Button>
                              <Button
                                variant="contained"
                                sx={{ backgroundColor: "red" }}
                                size="small"
                                onClick={() =>
                                  handleDeleteInstallmentDetail(index)
                                }
                              >
                                Delete
                              </Button>
                            </div>
                          </li>
                        ))}
                      </ul>
                    ) : (
                      <div>
                        <ul>
                          {/* {house_information.map((house_info, index) => ( */}
                          <li
                            // key={index}
                            style={{
                              border: "1px solid #ccc",
                              padding: "8px",
                              marginBottom: "8px",
                            }}
                          >
                            <span style={{ marginRight: "8px" }}>&#8226;</span>
                            No Installments Added!
                          </li>
                          {/* ))} */}
                        </ul>
                      </div>
                    )}
                  </div>
                </Typography>

                <CardActions>
                  <div className="flex flex-row gap-6">
                    {/* <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleOpenAgentForm}
                  >
                    + Add New Installment
                  </Button> */}

                    {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
                  </div>
                </CardActions>
              </CardContent>
              <div className="flex m-4 ">
                <Button onClick={handleOPenEditBonus} variant="contained">
                  Edit Bonus Payment Button
                </Button>
              </div>
              {/* <form onSubmit={formikEditAgentDetails.handleSubmit}> */}

              {/* <Button type="submit">Save Agent</Button> */}
              {/* </form> */}

              <div className="flex m-4 gap-4 ">
                <Button
                  variant="contained"
                  size="large"
                  type="submit"
                  sx={{ backgroundColor: "#242333" }}
                  // onClick={handle_submit}
                >
                  Save Record
                </Button>
                <Button
                  onClick={handleCloseMainEditform}
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="large"
                >
                  Close
                </Button>
              </div>
            </form>
          </Box>
        </Modal>

        {/* *******************EDIT HOUSE INSTALLMENT************************** */}
        <Modal
          open={openInstallmentModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          onClose={handleCloseEditInstallment}
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
              <h2 className="font-bold m-4"> Edit House Installment</h2>
              <CloseIcon
                fontSize="40"
                onClick={handleCloseEditInstallment}
                sx={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={formikEditInstallmentsDetails.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <TextField
                  type="text"
                  id="amount"
                  name="amount"
                  label="Installment"
                  placeholder=""
                  value={formikEditInstallmentsDetails.values.amount}
                  // onChange={formikEditInstallmentsDetails.handleChange}

                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditInstallmentsDetails.setFieldValue(
                      "amount",
                      formattedValue
                    );
                  }}
                  onBlur={formikEditInstallmentsDetails.handleBlur}
                  error={
                    formikEditInstallmentsDetails.touched.amount &&
                    Boolean(formikEditInstallmentsDetails.errors.amount)
                  }
                  helperText={
                    formikEditInstallmentsDetails.touched.amount &&
                    formikEditInstallmentsDetails.errors.amount
                  }
                />

                <TextField
                  type="text"
                  id="selectedDate"
                  name="selectedDate"
                  label="Booking Date"
                  placeholder="Client's Email"
                  value={(() => {
                    const selectedDate = new Date(
                      formikEditInstallmentsDetails.values.selectedDate
                    );
                    const months = [
                      "Jan",
                      "Feb",
                      "Mar",
                      "Apr",
                      "May",
                      "Jun",
                      "Jul",
                      "Aug",
                      "Sep",
                      "Oct",
                      "Nov",
                      "Dec",
                    ];
                    return `${
                      months[selectedDate.getMonth()]
                    } ${selectedDate.getDate()}, ${selectedDate.getFullYear()}`;
                  })()}
                  // value={new Date(formikEditMain.values.selectedDate).toLocaleDateString()}
                  onChange={formikEditInstallmentsDetails.handleChange}
                  // onChange={(e) => {
                  //   e.target.value = e.target.value.toUpperCase();
                  //   formikMain.handleChange(e);
                  // }}
                  onBlur={formikEditInstallmentsDetails.handleBlur}
                />

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={["DatePicker"]}>
                    <DatePicker
                      label="Edit Date"
                      //value={formikEditMain.values.selectedDate} // Pass value from Formik
                      onChange={(selectedDate) =>
                        formikEditInstallmentsDetails.setFieldValue(
                          "selectedDate",
                          selectedDate
                        )
                      }
                    />
                  </DemoContainer>
                </LocalizationProvider>

                <div>
                  <Button
                    // onClick={handleClose_booking}
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </Box>
        </Modal>
        {/* *******************EDIT BONUS PAYMENT************************** */}

        <Modal
          open={openBonusPaymentModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          onClose={handleCloseEditInstallment}
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
              <h2 className="font-bold m-4"> Bonus Editing Window</h2>
              <CloseIcon
                fontSize="40"
                // onClick={handleCloseEditInstallment}
                onClick={handleCloseBonusEditingWindow}
                sx={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={formikEditBonusPaymentDetails.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <TextField
                  type="text"
                  id="amount"
                  name="amount"
                  label="Installment"
                  placeholder=""
                  value={formikEditBonusPaymentDetails.values.amount}
                  // onChange={formikEditInstallmentsDetails.handleChange}

                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditBonusPaymentDetails.setFieldValue(
                      "amount",
                      formattedValue
                    );
                  }}
                  onBlur={formikEditBonusPaymentDetails.handleBlur}
                  error={
                    formikEditBonusPaymentDetails.touched.amount &&
                    Boolean(formikEditBonusPaymentDetails.errors.amount)
                  }
                  helperText={
                    formikEditBonusPaymentDetails.touched.amount &&
                    formikEditBonusPaymentDetails.errors.amount
                  }
                />

                {/* <TextField
                  type="text"
                  id="amount"
                  name="amount"
                  label="Installment Date"
                  placeholder=""
                  value={formikEditInstallmentsDetails.values.date}
                  // onChange={formikEditInstallmentsDetails.handleChange}

                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditInstallmentsDetails.setFieldValue(
                      "date",
                      formattedValue
                    );
                  }}
                  onBlur={formikEditInstallmentsDetails.handleBlur}
                  error={
                    formikEditInstallmentsDetails.touched.created_at &&
                    Boolean(formikEditInstallmentsDetails.errors.created_at)
                  }
                  helperText={
                    formikEditInstallmentsDetails.touched.created_at &&
                    formikEditInstallmentsDetails.errors.created_at
                  }
                /> */}

                <TextField
                  type="text"
                  id="selectedDate"
                  name="selectedDate"
                  label="Booking Date"
                  placeholder="Client's Email"
                  value={(() => {
                    const selectedDate = new Date(
                      formikEditBonusPaymentDetails.values.selectedDate
                    );
                    const months = [
                      "Jan",
                      "Feb",
                      "Mar",
                      "Apr",
                      "May",
                      "Jun",
                      "Jul",
                      "Aug",
                      "Sep",
                      "Oct",
                      "Nov",
                      "Dec",
                    ];
                    return `${
                      months[selectedDate.getMonth()]
                    } ${selectedDate.getDate()}, ${selectedDate.getFullYear()}`;
                  })()}
                  // value={new Date(formikEditMain.values.selectedDate).toLocaleDateString()}
                  onChange={formikEditBonusPaymentDetails.handleChange}
                  // onChange={(e) => {
                  //   e.target.value = e.target.value.toUpperCase();
                  //   formikMain.handleChange(e);
                  // }}
                  onBlur={formikEditBonusPaymentDetails.handleBlur}
                />

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={["DatePicker"]}>
                    <DatePicker
                      label="Edit Date"
                      //value={formikEditMain.values.selectedDate} // Pass value from Formik
                      onChange={(selectedDate) =>
                        formikEditBonusPaymentDetails.setFieldValue(
                          "selectedDate",
                          selectedDate
                        )
                      }
                    />
                  </DemoContainer>
                </LocalizationProvider>

                <div>
                  <Button
                    // onClick={handleClose_booking}
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>

            {/* <form onSubmit={formikEditBonusPaymentDetails.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <TextField
                  type="text"
                  id="amount"
                  name="amount"
                  label="Installment"
                  placeholder=""
                  value={formikEditBonusPaymentDetails.values.amount}
                  // onChange={formikEditInstallmentsDetails.handleChange}

                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditBonusPaymentDetails.setFieldValue(
                      "amount",
                      formattedValue
                    );
                  }}
                  onBlur={formikEditBonusPaymentDetails.handleBlur}
                  error={
                    formikEditBonusPaymentDetails.touched.amount &&
                    Boolean(formikEditBonusPaymentDetails.errors.amount)
                  }
                  helperText={
                    formikEditBonusPaymentDetails.touched.amount &&
                    formikEditBonusPaymentDetails.errors.amount
                  }
                />

                {formikEditBonusPaymentDetails.values.selectedDate}

                <TextField
                  type="text"
                  id="selectedDate"
                  name="selectedDate"
                  label="Booking Date"
                  placeholder="Client's Date"
                  value={(() => {
                    const selectedDate = new Date(
                      formikEditBonusPaymentDetails.values.selectedDate
                    );
                    const months = [
                      "Jan",
                      "Feb",
                      "Mar",
                      "Apr",
                      "May",
                      "Jun",
                      "Jul",
                      "Aug",
                      "Sep",
                      "Oct",
                      "Nov",
                      "Dec",
                    ];
                    return `${
                      months[selectedDate.getMonth()]
                    } ${selectedDate.getDate()}, ${selectedDate.getFullYear()}`;
                  })()}
                  // value={new Date(formikEditMain.values.selectedDate).toLocaleDateString()}
                  onChange={formikEditBonusPaymentDetails.handleChange}
                  // onChange={(e) => {
                  //   e.target.value = e.target.value.toUpperCase();
                  //   formikMain.handleChange(e);
                  // }}
                  onBlur={formikEditBonusPaymentDetails.handleBlur}
                />

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={["DatePicker"]}>
                    <DatePicker
                      label="Edit Date"
                      //value={formikEditMain.values.selectedDate} // Pass value from Formik
                      onChange={(selectedDate) =>
                        formikEditBonusPaymentDetails.setFieldValue(
                          "selectedDate",
                          selectedDate
                        )
                      }
                    />
                  </DemoContainer>
                </LocalizationProvider>

                <div>
                  <Button
                    // onClick={handleClose_booking}
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form> */}
          </Box>
        </Modal>

        <Modal
          open={openHouseEditModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            <div className="flex flex-row justify-between">
              {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
              <h2 className="font-bold m-4"> Edit House Information </h2>
              <CloseIcon
                fontSize="40"
                onClick={closeHouseEditModal}
                sx={{ cursor: "pointer" }}
              />
            </div>

            <form onSubmit={formikEditHouseDetails.handleSubmit}>
              <div className="flex flex-row gap-3 mt-3 mb-3">
                <TextField
                  type="text"
                  id="location"
                  name="location"
                  label="Location"
                  placeholder="Location"
                  value={formikEditHouseDetails.values.location}
                  error={
                    formikEditHouseDetails.touched.location &&
                    Boolean(formikEditHouseDetails.errors.location)
                  }
                  helperText={
                    formikEditHouseDetails.touched.location &&
                    formikEditHouseDetails.errors.location
                  }
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="residence"
                  name="residence"
                  label="House Residence"
                  placeholder="Residence"
                  value={formikEditHouseDetails.values.residence}
                  error={
                    formikEditHouseDetails.touched.residence &&
                    Boolean(formikEditHouseDetails.errors.residence)
                  }
                  helperText={
                    formikEditHouseDetails.touched.residence &&
                    formikEditHouseDetails.errors.residence
                  }
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                />
              </div>

              <TextField
                type="text"
                id="house_number"
                name="house_number"
                label="House Number"
                placeholder="House Number"
                value={formikEditHouseDetails.values.house_number}
                error={
                  formikEditHouseDetails.touched.house_number &&
                  Boolean(formikEditHouseDetails.errors.house_number)
                }
                helperText={
                  formikEditHouseDetails.touched.house_number &&
                  formikEditHouseDetails.errors.house_number
                }
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />
              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type="text"
                  id="asking_price"
                  name="asking_price"
                  label="Asking Price"
                  placeholder="Asking Price"
                  value={formikEditHouseDetails.values.asking_price}
                  //onChange={formikEditHouseDetails.handleChange}'

                  error={
                    formikEditHouseDetails.touched.asking_price &&
                    Boolean(formikEditHouseDetails.errors.asking_price)
                  }
                  helperText={
                    formikEditHouseDetails.touched.asking_price &&
                    formikEditHouseDetails.errors.asking_price
                  }
                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditHouseDetails.setFieldValue(
                      "asking_price",
                      formattedValue
                    );
                  }}
                  onBlur={formikEditHouseDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="selling_price"
                  name="selling_price"
                  label="Selling Price"
                  placeholder="Selling Price"
                  value={formikEditHouseDetails.values.selling_price}
                  // onChange={formikEditHouseDetails.handleChange}

                  error={
                    formikEditHouseDetails.touched.selling_price &&
                    Boolean(formikEditHouseDetails.errors.selling_price)
                  }
                  helperText={
                    formikEditHouseDetails.touched.selling_price &&
                    formikEditHouseDetails.errors.selling_price
                  }
                  onChange={(e) => {
                    const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                    const formattedValue = rawValue
                      .replace(/\D/g, "")
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                    formikEditHouseDetails.setFieldValue(
                      "selling_price",
                      formattedValue // Corrected to set "selling_price" instead of "asking_price"
                    );
                  }}
                  onBlur={formikEditHouseDetails.handleBlur}
                />
              </div>

              <div className="flex flex-col gap-3 mt-3">
                <FormControl>
                  <TextField
                    id="house_type"
                    select
                    name="house_type"
                    label="House type"
                    placeholder="house_type"
                    fullWidth
                    value={formikEditHouseDetails.values.house_type}
                    error={
                      formikEditHouseDetails.touched.house_type &&
                      Boolean(formikEditHouseDetails.errors.house_type)
                    }
                    helperText={
                      formikEditHouseDetails.touched.house_type &&
                      formikEditHouseDetails.errors.house_type
                    }
                    onChange={formikEditHouseDetails.handleChange}
                    onBlur={formikEditHouseDetails.handleBlur}
                  >
                    {house_type.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
              </div>

              <div className="flex flex-row gap-3 mt-3"></div>

              <Button
                // onClick={handleClose_booking}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="small"
                type="submit"
              >
                Save
              </Button>
            </form>
          </Box>
        </Modal>
        {/***********************Edit Bonus Payment WINDOW***********************/}

        <Modal
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          open={openBonusPayment}
          onClose={handleCloseEditBonus}
        >
          <Box sx={style_box}>
            <h2 className="font-bold m"> Edit Bonus Payment Windows--- </h2>
            <CardMedia height="540" />
            <CardContent>
              {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}
              <Typography variant="body2" color="text.secondary">
                <div>
                  {bonusPayments && bonusPayments.length > 0 ? (
                    <ul>
                      {bonusPayments.map((installment, index) => (
                        <li
                          key={index}
                          style={{
                            border: "1px solid #ccc",
                            padding: "8px",
                            marginBottom: "8px",
                          }}
                          className="flex flex-col gap-4 justify-center"
                        >
                          <div>
                            {/* <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span> */}
                            {/* Installment {index + 1}: &nbsp;{" "} */}
                            <div className="flex flex-col">
                              &#8226;{installment.created_at}
                              &nbsp; KES &nbsp;
                              {parseFloat(installment.amount).toLocaleString()}
                            </div>
                          </div>

                          <div className="flex flex-row space-x-2">
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={() =>
                                //handleEditInstallmentDetail(installment, index)
                                handleEditBonusDetail(installment, index)
                              }
                            >
                              Edit
                            </Button>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "red" }}
                              size="small"
                              onClick={() => handleDeleteBonusDetail(index)}
                            >
                              Delete
                            </Button>
                          </div>
                        </li>
                      ))}
                    </ul>
                  ) : (
                    <div>
                      <ul>
                        {/* {house_information.map((house_info, index) => ( */}
                        <li
                          // key={index}
                          style={{
                            border: "1px solid #ccc",
                            padding: "8px",
                            marginBottom: "8px",
                          }}
                        >
                          <span style={{ marginRight: "8px" }}>&#8226;</span>
                          No Installments Added!
                        </li>
                        {/* ))} */}
                      </ul>
                    </div>
                  )}
                </div>
              </Typography>
            </CardContent>
          </Box>
        </Modal>
      </React.Fragment>
    </>
  );
}

const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};

export default function RecordTableTest() {
  const [data, setData] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  // const [page, setPage] = React.useState(0);
  //const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [loading, setLoading] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = useState(5);
  const [totalInstallments, setTotalInstallments] = useState("");
  const [balance, setBalance] = useState("");
  const [openMainEditForm, setOpenMainEditForm] = useState(false);
  const navigate = useNavigate();
  const [house_information, setHouse_Information] = React.useState([]);
  const [incoming_instalments, setInstallments] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState();
  const [filteredResults, setResults] = React.useState([]);
  const [agent_information, setAgentInformation] = React.useState(null);
  const [clickedAgent, setclickedAgent] = React.useState(null);
  const [openHouseEditModal, setopenHouseEditModal] = React.useState(false);
  const [openAddHouseInfor, setOpenAddHouseInfor] = React.useState(false);
  const [openAgentForm, setOpenAgentForm] = React.useState(false);
  const [openpopup, setOpenpopUp] = React.useState(false);
  const [openInstallmentModal, setopenInstallmentModal] = React.useState(false);
  const [refresher, setRefresher] = React.useState(false);
  const [global_index, setIndex] = React.useState();
  const [global_id, setGlobalId] = React.useState();
  const [opendeleteRecordModal, setOpenDeleteRecordModal] = useState(false);
  const [activeRecs, setActiveRecords] = useState();
  const [input, setInput] = useState("");
  const [searchTerm, setSearchTerm] = useState("");

  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);
  //console.log("ID", storedData);
  //setGlobalId(storedData._id);

  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    console.log("search term:->", event.target.value);
    handleGetAllRecords(event.target.value);
    // You can perform additional logic here, such as filtering/searching based on the entered search term
  };

  const handleChange = (event) => {
    setInput(event.target.value);
  };

  function containsHyphenAndNumber(str) {
    // Regular expression to match hyphen followed by a number
    var regex = /-\d/;

    // Test the string against the regular expression
    return regex.test(str);
  }

  function handleGetAllRecords(searchVal) {
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    // console.log("ID", storedData);
    setGlobalId(storedData._id);
    setLoading(true);
    console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        // activeRecords.sort(
        //   (a, b) => new Date(b.created_at) - new Date(a.created_at)
        // );

        activeRecords.sort((a, b) => {
          const dateA = new Date(a.created_at);
          const dateB = new Date(b.created_at);

          if (isNaN(dateA.getTime()) || isNaN(dateB.getTime())) {
            // console.log(
            //   "One or more objects have invalid 'created_at' date:",
            //   a,
            //   b
            // );
            return 0; // no sorting change if dates are invalid
          }

          return dateB - dateA; // descending order
        });

        console.log("after-sorting-records:->", activeRecords);

        activeRecords.forEach((item) => {
          if (
            item.client_information.house_status === "BOOKED" ||
            item.client_information.house_status === "INTERESTED" ||
            item.client_information.house_status === "SOLD" ||
            item.client_information.house_status === "FULLY-SOLD"
          ) {
            //console.log("BOOKED:->", item)
            const totalInstallmentsPaid = item.installments.reduce(
              (total, amount) => {
                return total + parseFloat(amount.amount);
              },
              0
            );

            const totalSellingPrice = item.house_information.reduce(
              (total, sp) => {
                return total + parseFloat(sp.selling_price);
              },
              0
            );

            const balance = totalSellingPrice - totalInstallmentsPaid;

        

            if (
              totalInstallmentsPaid > 100000 &&
              item.client_information.house_status != "FULLY-SOLD" &&
              item.client_information.house_status != "SOLD"
            ) {
              console.log("total instalments paid is greater than 100000");
              // ()

              ChangeToSold(item);
            } else if (
              totalInstallmentsPaid === 100000 &&
              item.client_information.house_status != "BOOKED"
            ) {
              ChangeToBooked(item);
            } else if (
              totalInstallmentsPaid === 0 &&
              item.client_information.house_status != "INTERESTED"
            ) {
              console.log("INTERESTED-->", item);
              ChangeToInterested(item);
            } else if (
              balance === 0 &&
              item.client_information.house_status != "FULLY-SOLD"
            ) {
              //console.log("ZERO BALANCE", item.client_information.house_status);
              changeToFullySold(item);
            } else if (
              (balance === 0 &&
                item.client_information.house_status !== "FULLY-SOLD") ||
              (balance !== 0 &&
                item.client_information.house_status === "FULLY-SOLD")
            ) {
              if (balance === 0) {
                //console.log("ZERO BALANCE", item.client_information.house_status);
                changeToFullySold(item);
              } else {
                ChangeToSold(item);
              }
            } else {
              //console.log("ITEM-->", item);
            }
          }
        });

        console.log("Search value", searchVal);

        console.log("house number", containsHyphenAndNumber(searchVal));

        if (containsHyphenAndNumber(searchVal)) {
          if (searchVal === undefined || searchVal.trim().length === 0) {
            setRows(activeRecords);
          } else {
            const lowerCaseSearchVal = searchVal.toLowerCase(); // Convert searchVal to lowercase

            const results = activeRecords.filter((element) => {
              // Filter the houses within house_information array
              const house_results = element.house_information.filter(
                (house) => {
                  const house_number = (house.house_number || "").toLowerCase(); // Ensure house_number is defined and convert to lowercase

                  // Check if lowercase house_number includes the lowercase search term
                  return house_number.includes(lowerCaseSearchVal);
                }
              );

              // If any house matches the search criteria, include the record
              return house_results.length > 0;
            });

            setRows(results);
            setActiveRecords(results);
          }
        } else if (searchVal === undefined || searchVal.trim().length === 0) {
          setRows(activeRecords);
          // setActiveRecords(activeRecords);
        } else {
          const results = activeRecords.filter((user) => {
            console.log("MY USER", user);
            const firstName =
              user.client_information && user.client_information.first_name
                ? user.client_information.first_name.toLowerCase()
                : "";
            const lastName =
              user.client_information && user.client_information.last_name
                ? user.client_information.last_name.toLowerCase()
                : "";
            const secondName =
              user.client_information && user.client_information.second_name
                ? user.client_information.second_name.toLowerCase()
                : "";

            const searchTerm = searchVal.toLowerCase();
            return (
              firstName.includes(searchTerm) ||
              lastName.includes(searchTerm) ||
              secondName.includes(searchTerm)
            );
          });

          console.log("filtered results:-->", results);

          setRows(results);
          setActiveRecords(results);
        }

        // Assuming activeRecords is an array of objects
        // activeRecords.sort((a, b) => {
        //   // Convert created_at strings to Date objects for comparison
        //   const dateA = new Date(a.created_at);
        //   const dateB = new Date(b.created_at);

        //   // Compare the Date objects
        //   return dateA - dateB;
        // });

        //console.log("rec logs", json);

        activeRecords.forEach((element, index) => {
          // console.log("element--", element);

          const totalInstallmentsPaid = element.installments.reduce(
            (total, amount) => {
              return total + parseFloat(amount.amount);
            },
            0
          );

          const totalSellingPrice = element.house_information.reduce(
            (total, sp) => {
              return total + parseFloat(sp.selling_price);
            },
            0
          );

          setBalance(totalSellingPrice - totalInstallmentsPaid);

          setTotalInstallments(totalInstallmentsPaid);
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // const handleChange = (value) => {
  //   setInput(value);

  //  // setInput(value);

  //   //handleGetAllRecords();

  //   //  setResults(filterNursesByWard(incoming_nursesData, value));

  //   // if (searchType === "Search Name" || default_search_name) {
  //   //   fetchData(value);
  //   // } else if (searchType === "Search Ward") {
  //   //   console.log("Search By Wards");
  //   //   console.log("incoming nues dara", incoming_nursesData);
  //   //   console.log("filtered", filterNursesByWard(incoming_nursesData, value));

  //   //   setResults(filterNursesByWard(incoming_nursesData, value));
  //   // }
  // };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  function deleteRecordModal() {
    setOpenDeleteRecordModal(true);
  }

  const handleOpenMainEditform = () => {
    console.log("edit form opened");

    console.log("selected row data", selectedRowData);

    // setGlobalDate(selectedRowData.date);

    //  formikEditMain.setValues({
    //    client_firstname: selectedRowData.client_information.first_name,
    //    client_lastname: selectedRowData.client_information.last_name,
    //    client_phonenumber: selectedRowData.client_information.phone_number,
    //    client_email: selectedRowData.client_information.email,
    //    client_nat_id: selectedRowData.client_information.nat_id,
    //    installments: selectedRowData.down_payment,
    //    created_at: selectedRowData.created_at,
    //  });

    //  setHouse_Information(selectedRowData.house_information);
    //  setclickedAgent(selectedRowData.broker_information);
    //  setInstallments(selectedRowData.installments);
    //  setOpenMainEditForm(true);
  };
  const items = [
    {
      key: "1",
      label: "Edit Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => handleOpenMainEditform(),
    },
    {
      key: "2",
      label: "Add Installment",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => OpenAddInstallmentModal(),
    },

    {
      key: "3",
      label: "Delete Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      // onClick: () => deleteRecord(),
      onClick: () => deleteRecordModal(),
    },
  ];
  const [isVisible, setIsVisible] = useState(false);

  // Function to scroll to the top of the page
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  const scrollToBottom = () => {
    const documentHeight = document.documentElement.scrollHeight;
    const windowHeight = window.innerHeight;
    const scrollPosition = documentHeight - windowHeight;

    window.scrollTo({
      top: scrollPosition,
      behavior: "smooth",
    });
  };

  // Function to handle the scroll event
  const handleScroll = () => {
    if (window.scrollY > 300) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  React.useEffect(() => {
    handleGetAllRecords();
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [refresher]); // The empty dependency array ensures the effect runs only once when the component mounts

  // const [scrollPosition, setScrollPosition] = useState(0);

  // const handleScrollUp = () => {
  //   window.scrollTo({
  //     top: scrollPosition - 300, // Scroll up by 100 pixels
  //     behavior: "smooth", // Optional, smooth scrolling animation
  //   });
  //   setScrollPosition(scrollPosition - 300);
  // };

  // const handleScrollDown = () => {
  //   window.scrollTo({
  //     top: scrollPosition + 600, // Scroll down by 100 pixels
  //     behavior: "smooth", // Optional, smooth scrolling animation
  //   });
  //   setScrollPosition(scrollPosition + 600);
  // };

  return (
    <>
      <MainNavbar proprecord={activeRecs} />
      {/* {loading && <Loader2 />} */}
      <div className="flex flex-col top-[50%] right-[50%] justify-center items-center absolute">
      {loading && <Spinner1 className="z-50" />}</div>
      <NewRecord />
      <div className=" fixed aria-busy: right-0  flex flex-col bottom-2 bg-transparent">
        <IconButton onClick={scrollToTop}>
          <KeyboardArrowUpIcon />
        </IconButton>
        <IconButton onClick={scrollToBottom}>
          <KeyboardArrowDownIcon />
        </IconButton>
      </div>
      <div className="flex  justify-center mt-[-20px] m-3">
        <TextField
          size="small"
          sx={{ width: [220, 400] }}
          variant="outlined"
          label="Search Name or House Number"
          value={searchTerm}
          onChange={handleSearchChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton>
                  <SearchIcon sx={{ fontSize: [18, 22] }} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </div>

      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead sx={{ bgcolor: "gray.200" }}>
            <TableRow>
              <TableCell />
              <TableCell
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                Client
              </TableCell>

              <TableCell
                align="left"
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                Status
              </TableCell>
              <TableCell
                align="right"
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                Date
              </TableCell>
              <TableCell
                align="right"
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                Deposits
              </TableCell>
              <TableCell
                align="right"
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                Balance
              </TableCell>
              <TableCell
                align="right"
                sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                className="select-none"
              >
                House Value
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <Dropdown
                  key={index}
                  menu={{ items }}
                  trigger={["contextMenu"]}
                  placement="bottomLeft"
                >
                  <Row
                    key={row._id}
                    row={row}
                    globalID={global_id}
                    chosenRowData={selectedRowData._id}
                    onMouseEnter={() => {
                      setSelectedRowData(row);
                      rowMenuFunc();
                      setIndex(index);
                      /// console.log("ROW SELECTED!!");
                    }}
                  />
                </Dropdown>
              ))}
          </TableBody>
        </Table>
        <TableFooter>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </TableFooter>
      </TableContainer>
    </>
  );
}
