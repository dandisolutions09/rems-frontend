import * as React from "react";
import { BarChart } from "@mui/x-charts/BarChart";
import { axisClasses } from "@mui/x-charts";
import { useState } from "react";
import { Box, FormControl, FormLabel, InputLabel, MenuItem } from "@mui/material";
import Select from "@mui/material/Select";

const chartSetting = {
  yAxis: [
    {
      label: "bonus",
    },
  ],
  width: 300,
  height: 300,
  sx: {
    [`.${axisClasses.left} .${axisClasses.label}`]: {
      transform: "translate(-10px, 90px)",
    },
  },
};

const valueFormatter = (value) => `KES ${value}`;

export default function BarsDataset() {
  const [expanded, setExpanded] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [revenuePercentage, setRevenuePercentage] = React.useState();
  const [rows, setRows] = React.useState([]);
  const [selectedRowData, setSelectedRowData] = useState();
  const [totalInstallments, setTotalInstallments] = React.useState("");
  const [totallSellingPrice, setTotalSellingPrice] = useState();
  const [totallBalance, setBalance] = useState();
  const [totalClients, setTotalClients] = useState(0);
  const [totalProperties, setTotalProperties] = useState();
  const [fullName, setName] = useState();
  const [bonus, setBonus] = useState();
  const [bonus_multiplier, setBonusMultiplier] = useState(0.005);
  const [email, setEmail] = useState();
  const [bonus_dataset_state, setBonusDataset] = useState();
  const [sorted_year, setSortedYear] = useState("2024");
  const [has2023, setContains2023] = useState();
  const [has2024, setContains2024] = useState();

  function handleGetAllRecords() {
    setLoading(true);
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    // console.log("ID", storedData);
    //setGlobalId(storedData._id);
    //setLoading(true);
    //console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //  console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);
        let totalInstallmentsPaid = 0;
        let totalSellingPrice = 0; // Initialize totalSellingPrice counter
        let totalProperties = 0;
        let bonus_dataset = [];

        const monthNames = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ];

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        // console.log("after-sorting-records:->", activeRecords);

        setRows(activeRecords);
        setName(json.first_name + "    " + json.last_name);

        setEmail(json.email);

        //setPhoneNumber(json.first_name + "    "+ json.last_name);

        //console.log("active records----->", activeRecords);

        setTotalClients(activeRecords.length);

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
            const totalSellingPrices = element.house_information.reduce(
              (total, house) => {
                return total + parseFloat(house.selling_price);
              },
              0
            );

            //console.error("total Bonus Recieved", totalSellingPrices * 0.03);

            // console.log("house", element)

            // console.log("month", new Date(element.date))

            const dateString = element.date; // Example date string in "DD/MM/YYYY" format
            const parts = dateString.split("/"); // Split the string into parts
            const year = parseInt(parts[2], 10); // Parse the year part as an integer
            const month = parseInt(parts[1], 10) - 1; // Parse the month part as an integer (subtract 1 since months are zero-based)
            const day = parseInt(parts[0], 10); // Parse the day part as an integer
            const dateObject = new Date(year, month, day); // Create the Date object
            //console.log("incoming date", dateObject.getMonth());

            const monthIndex = dateObject.getMonth();

            const monthName = monthNames[monthIndex];

            //console.log("Month:", monthName);

            const bonus_obj = {
              date: element.date,
              house_sp: totalSellingPrices * bonus_multiplier,
              month: monthName,
            };

            // console.warn("house object", bonus_obj);
            bonus_dataset.push(bonus_obj);

            //setBonus(totalBonus);

            //     console.log("total clients-->", activeRecords.length);

            //    setTotalClients(activeRecords.length);

            totalSellingPrice += totalSellingPrices;
            // Increment the totalSellingPrice counter

            const totalInstallments = element.installments.reduce(
              (total, installment) => {
                return total + parseFloat(installment.amount);
              },
              0
            );
            // Increment the totalInstallmentsPaid counter
            totalInstallmentsPaid += totalInstallments;

            const numHouses = element.house_information.length;
            //console.log("number of houses-->", numHouses);
            // Increment the totalProperties counter
            totalProperties += numHouses;
          } // end og activeRecords for loop
          //console.error("TOTAL SALES", totalSellingPrice * bonus_multiplier);

          // setBalance(totalSellingPrice - totalInstallmentsPaid);
          // console.log("total nst", totalInstallmentsPaid);
          //   console.log("total number of houses:-", totalProperties);
          setTotalProperties(totalProperties);

          setTotalInstallments(totalInstallmentsPaid);
          setTotalSellingPrice(totalSellingPrice);
          // console.log(
          //   "balance",
          //   parseFloat(totalSellingPrice - totalInstallmentsPaid)
          // );

          const bal = parseFloat(totalSellingPrice - totalInstallmentsPaid);
          //console.log("balance", bal / 1000000);
          setBalance(parseFloat(bal));
        }); //end of activRecords LOOP

        console.log("full array", bonus_dataset);

        function containsYear2024(dataArray) {
          return dataArray.some((obj) => {
            const year = parseInt(obj.date.split("/")[2]);
            return year === 2024;
          });
        }

        function containsYear2023(dataArray) {
          return dataArray.some((obj) => {
            const year = parseInt(obj.date.split("/")[2]);
            return year === 2023;
          });
        }

        const contains2024 = containsYear2024(bonus_dataset);
        const contains2023 = containsYear2023(bonus_dataset);
        setContains2023(containsYear2023(bonus_dataset));
        setContains2024(containsYear2024(bonus_dataset));

        console.error("has 2024", containsYear2024(bonus_dataset));

        if (contains2023) {
          setContains2023(true);
        }

        console.log("contains2024", contains2024);

        // Dictionary to store total house selling price for each month
        const monthlyTotals = {};
        console.log("sorted year", sorted_year);

        if (sorted_year != undefined) {
        }

        const filteredData = bonus_dataset.filter((entry) =>
          entry.date.includes(sorted_year)
        );
        console.error("filteredData", filteredData);

        // Step 1: Parse dates into Date objects
        const parsedData = filteredData.map((item) => {
          const [day, month, year] = item.date.split("/");
          return {
            ...item,
            date: new Date(year, month - 1, day), // month is 0-indexed in Date constructor
          };
        });

        //  //
        //         const parsedData = filteredData.map((item) => {
        //           const [day, month, year] = item.date.split("/");
        //           return {
        //             ...item,
        //             date: new Date(year, month - 1, day), // month is 0-indexed in Date constructor
        //           };
        //         });
        // Further operations with parsedData

        // Handle the case where filteredData is undefined
        console.log(
          "filteredData is undefined. Cannot proceed with mapping and parsing."
        );

        // Step 2: Group by month
        const groupedByMonth = parsedData.reduce((acc, item) => {
          const month = item.date.getMonth();
          acc[month] = acc[month] || [];
          acc[month].push(item);
          return acc;
        }, {});

        // Step 3: Calculate total house_sp for each month
        const totalsByMonth = Object.entries(groupedByMonth).map(
          ([month, items]) => ({
            month: new Date(2024, month).toLocaleString("default", {
              month: "short",
            }), // Get full month name
            total_house_sp: items.reduce(
              (total, item) => total + item.house_sp,
              0
            ),
          })
        );

        // Step 4: Sort by month
        totalsByMonth.sort((a, b) => {
          const monthA = new Date(`${a.month} 1, 2024`);
          const monthB = new Date(`${b.month} 1, 2024`);
          return monthA - monthB;
        });

        console.log(totalsByMonth);

        console.log("TOTALS BY MONTH", totalsByMonth);
        //console.log("dataet", dataset);

        const totalBonus = totalSellingPrice * bonus_multiplier;
        console.log("total selling price", totalSellingPrice);

        console.log("total bonus", totalBonus);

        // setBonus(totalsByMonth);

        setBonusDataset(totalsByMonth);
      })
      .finally(() => {
        setLoading(false);
      });
  }
  const handleChange = (event) => {
    console.log("year", event.target.value);
    setSortedYear(event.target.value);
  };

  React.useEffect(() => {
    handleGetAllRecords();
  }, [sorted_year]);
  return (
    <>
    
      <FormControl
        sx={{
          display: "flex",
          margin: 2,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <FormLabel>Select Year</FormLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={sorted_year}
         
          sx={{ width: [300, "full"] }}
       
          onChange={handleChange}
        >
          {has2023 && <MenuItem value={"2023"}>2023</MenuItem>}
          {has2024 && <MenuItem value={"2024"}>2024</MenuItem>}
        </Select>
      </FormControl>

      {/* {bonus_dataset_state && (
        <ul>
          {bonus_dataset_state.map((item, index) => (
            <li key={index}>{item.total_house_sp}</li>
          ))}
        </ul>
      )} */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
       
          width:"100%"
        }}
      >
        {bonus_dataset_state && (
          <BarChart
            width={500}
            height={350}
            dataset={bonus_dataset_state}
            xAxis={[{ scaleType: "band", dataKey: "month" }]} // Configure X-axis with month as dataKey
            series={[
              {
                dataKey: "total_house_sp",
                label: "Monthly Bonus",
                valueFormatter,
              }, // Configure series with total_house_sp as dataKey
            ]}
            // Additional chart settings
          />
        )}
      </Box>
    </>
  );
}
