import { useCountUp } from "use-count-up";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import _ from "lodash";
import { IMaskInput } from "react-imask";
import { NumericFormat } from "react-number-format";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import * as Yup from "yup";
import {
  Modal,
  TextField,
  Box,
  MenuItem,
  IconButton,
  InputAdornment,
  FormControl,
  InputLabel,
  Select,
  FormLabel,
  checkboxClasses,
  Input,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import { useFormik } from "formik";
import { Line, Circle } from "rc-progress";
import CloseIcon from "@mui/icons-material/Close";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";

import { useNavigate } from "react-router-dom";
import MainNavbar from "./MainNavbar";
//import styled from "styled-components";
import { styled, alpha } from "@mui/material/styles";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import HouseAddedSuccessfully from "../Alerts/HouseAddedSuccessfully";
import AgentAddedSuccessuflly from "../Alerts/AgentAddedSuccessuflly";
import BarChartReacords from "./BarChartReacords";
import RecordAddedSuccefull from "../Alerts/RecordAddedSuccefull";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import NohouseInform from "../Alerts/NohouseInform";
import InterestedError from "../Alerts/InterestedError";
import BookedError from "../Alerts/BookedError";
import SoldError from "../Alerts/SoldError";
import { ignore } from "antd/es/theme/useToken";

// const validationSchema = Yup.object({
//   client_firstname: Yup.string().required("First Name is required"),
//   first_name: Yup.string().required("First Name is required"),
//   client_lastname: Yup.string().required("Last Name is required"),
//   last_name: Yup.string().required("Last Name is required"),
//   client_phonenumber: Yup.string().required("Phone Number is required"),
//   phone_number: Yup.string().required("Phone Number is required"),
//   client_nat_id: Yup.string().required("ID is required"),
//   client_email: Yup.string().required("Email is required"),
//   location: Yup.string().required("location is required"),
//   residence: Yup.string().required("Residence is required"),
//   house_number: Yup.string().required("House Number is required"),
//   asking_price: Yup.string().required("Asking Price is required"),
//   selling_price: Yup.string().required("Selling Price is required"),
//   house_status: Yup.string().required("House Status is required"),
//   house_type: Yup.string().required("House Type is required"),
// });
const validationSchema_clientDetails = Yup.object({
  client_firstname: Yup.string().required("First Name is required"),
  client_secondname: Yup.string().required("Second Name is required"),
  client_lastname: Yup.string(),
  client_phonenumber: Yup.string().required("Phone Number is required"),
  client_nat_id: Yup.string().required("ID is required"),
  client_email: Yup.string().email().required("Email is required"),
  house_status: Yup.string().required("House Status is required"),
  // down_payment: Yup.string().required("Downpayment is required"),
});

const validationSchema_housedetails = Yup.object({
  location: Yup.string().required("location is required"),
  residence: Yup.string().required("Residence is required"),
  house_number: Yup.string().required("House Number is required"),
  asking_price: Yup.string().required("Asking Price is required"),
  selling_price: Yup.string().required("Selling Price is required"),
  house_type: Yup.string().required("House Type is required"),
});

const validationSchema_agentDeatails = Yup.object({
  first_name: Yup.string().required("First Name is required"),
  last_name: Yup.string().required("Last Name is required"),
  company_name: Yup.string().required("company name is required"),
  company_email: Yup.string().email().required("company email is required"),
  phone_number: Yup.string().required("Phone Number is required"),
});

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

export default function NewRecord() {
  const house_array = [];
  const [isLoading, setIsLoading] = React.useState(false);
  const [buttonLabel, setButtonLabel] = React.useState("Start");
  const [percentage, setPercentage] = React.useState();
  const [openAddHouseInfor, setOpenAddHouseInfor] = React.useState(false);
  const [user_percentage, setPercentageUser] = React.useState();
  const [numberNurses, set_numberNurses] = React.useState();
  const [numberUsers, set_numberUsers] = React.useState();
  const [openAgent, setOpenAgent] = React.useState();
  const [openBooking, setOpenBooking] = React.useState(false);
  const [residence, setResidence] = React.useState("");
  const [openpopup, setOpenpopUp] = React.useState(false);
  const [house_information, setHouse_Information] = React.useState([]);
  const [openEditModal, setOpenEditModal] = React.useState(false);
  const [information_to_be_Edited, setInforToBeEdited] = React.useState();
  const [houseIndex, setHouseIndex] = React.useState();
  const [openAgentForm, setOpenAgentForm] = React.useState(false);
  const [clickedAgent, setclickedAgent] = React.useState(null);
  const [agent_information, setAgentInformation] = React.useState(null);
  const [openHouseAddedSuccessfully, setOpenHouseAddedSuccessfully] =
    React.useState(false);
  const [openRecordAddedSuccessfully, setOpenRecordAddedSuccessfully] =
    React.useState(false);
  const [openAgentAddedSuccessfully, setOpenAgentAddedSuccessfully] =
    React.useState(false);
  const [rows, setRows] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [loading, setLoading] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = useState();
  const [totalInstallments, setTotalInstallments] = useState("");
  const [balance, setBalance] = useState("");
  const [filteredResults, setResults] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState();
  const [houseInformationAlert, setNoHouseInformationAlert] = useState(false);
  const [refresher, setRefresher] = useState(false);

  const [interestedError, setInterestedError] = useState(false);
  const [bookingError, setBookedError] = useState(false);
  const [soldError, setSoldError] = useState(false);
  const [selectedHouseStatus, setSelectedHouseStatus] = useState("");

  const [openHouseDetailsModal, setOpenHouseDetailsModal] =
    React.useState(false);
  const navigate = useNavigate();

  const [paymentMode, setPaymentMode] = useState("percentage"); // State variable to track the selected payment mode

  const { value: value1, reset: resetValue1 } = useCountUp({
    isCounting: isLoading,
    duration: 1,
    start: 0,
    end: 25,
    onComplete: () => {
      setIsLoading(false);
      setButtonLabel("Reset");
    },
  });

  const handleCloseHouseDetailsModal = () => {
    setOpenHouseDetailsModal(false);
  };
  const handleOpenAgent = () => {
    setOpenAgent(true);
  };

  const handleClose_booking = () => {
    setOpenBooking(false);
  };

  const handleCloseEditModal = () => {
    setOpenEditModal(false);
  };

  const handleOpen_booking = () => {
    setOpenBooking(true);
  };

  const handleButtonClick = () => {
    if (isLoading) {
      setIsLoading(false);
      setButtonLabel("Start");
      resetValue1();
    } else if (buttonLabel === "Reset") {
      setButtonLabel("Start");
      resetValue1();
    } else {
      setIsLoading(true);
      setButtonLabel("Reset");
    }
  };
  const handle_back = () => {
    navigate("/home");
  };
  const handleOpen_AddHouseInfor = () => {
    setOpenAddHouseInfor(true);
  };
  const handleClose_AddHouseInfor = () => {
    setOpenAddHouseInfor(false);
  };

  const postData = async (data_) => {
    let installment_array = [];
    console.log("data to be sent", data_);
    // records_array.push(data_);

    //console.log("new incoming installments", incoming_instalments);

    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/post-record/${storedData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data_),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordAddedSuccessfully(true);

        //setOpenRecordChangeSuccessfully(true);
        setRefresher(true);

        setTimeout(() => {
          setOpenBooking(false);
          reloadPage();
        }, 2000);
        setOpenRecordAddedSuccessfully(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  function handleOpenModal() {
    console.log("opening modal");
    setOpenHouseDetailsModal(true);
  }

  function handleSave() {
    console.log();
  }

  function handleRemove(index) {
    console.log("handle remove for ", index);
    let updatedHouseArray = [];
    updatedHouseArray = [...house_information];
    updatedHouseArray.splice(index, 1);
    setHouse_Information(updatedHouseArray);
  }

  function handleSearchResultItem(result) {
    console.log("result clicked", result);
    setclickedAgent(result);
    setSearchValue("");
    setResults([]);
  }

  function handleRemoveAgent() {
    setclickedAgent("");
  }

  const fetchData = (search_field_value) => {
    console.log("fetch data is called!");
    // fetch("https://nurse-backend.onrender.com/get-nurses")
    fetch("https://rems-backend.onrender.com/get-records")
      .then((response) => response.json())
      .then((json) => {
        //console.log("JSON:->", json);
        // setIncoming_nursesData((prevData) => [...prevData, json]);
        //handleGetAllNurses();
        const results = json.filter((record) => {
          console.log(
            "to be filter record",
            record.broker_information.first_name +
              record.broker_information.last_name
          );
          return (
            //   search_field_value &&
            //   record &&
            //   record.broker_information.first_name &&
            //   record.broker_information.first_name
            //     .toLowerCase()
            //     .includes(search_field_value.toLowerCase())
            // );

            search_field_value &&
            record &&
            record.broker_information.first_name &&
            record.broker_information.last_name &&
            (record.broker_information.first_name
              .toLowerCase()
              .includes(search_field_value.toLowerCase()) ||
              record.broker_information.last_name
                .toLowerCase()
                .includes(search_field_value.toLowerCase()))
          );
        });

        //setResults(results);
        console.log("filtered results", results);
        if (results.length === 0 && search_field_value) {
          console.log("No records");
          setOpenpopUp(true);
        }
        setResults(results);
      });
  };

  function handleGetAllRecords() {
    setLoading(true);
    console.log("handle get all records called!");
    fetch("https://rems-backend.onrender.com/get-records")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec records", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        setRows(json);

        json.forEach((element, index) => {
          //   console.log("element--", element);

          const totalInstallmentsPaid = element.installments.reduce(
            (total, amount) => {
              return total + parseFloat(amount.amount);
            },
            0
          );

          const totalSellingPrice = element.house_information.reduce(
            (total, sp) => {
              return total + parseFloat(sp.selling_price);
            },
            0
          );

          // console.log("total selling price", totalSellingPrice);

          //console.log("total installments paid", totalInstallmentsPaid);
          // const bal =
          setBalance(totalSellingPrice - totalInstallmentsPaid);

          setTotalInstallments(totalInstallmentsPaid);
        });

        //   const totalAmountSaved = element.savings.reduce(
        //     (total, saving => {
        //       return total + parseFloat(saving.amount_saved);
        //     },
        //     0
        //   );

        //   )

        //   console.log("incoming", incoming_nursesData);
        // console.log("filtere:->", filterNursesByWard(json, "Ward A"));
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handlePaymentModeChange = (event) => {
    setPaymentMode(event.target.value);

    formikMain.setFieldValue("down_payment", "");

    if (event.target.value == "percentage") {
      console.log(" selected percentage");
    } else if (event.target.value == "cash") {
      console.log("selected cash");
    }
    // You can perform additional actions based on the selected payment mode here
  };

  // onClick={handleOpenAgentForm}

  // function handleOpenAgent() {
  //   setOpenAgentForm(true);
  // }

  const handleChange = (agent_name_searched) => {
    console.log("event", agent_name_searched);
    setSearchValue(agent_name_searched);
    // if (agent_name_searched) {
    //filter agent's name
    // handleGetAllRecords();
    fetchData(agent_name_searched);
    //   } else {
  };

  function handleOpenAgentForm(option) {
    // console.log("option selected", option);
    // if (option === "AGENT") {
    //   setOpenAgentForm(true);
    // }
    setOpenAgentForm(true);
  }

  function handlecloseAgentForm() {
    setOpenAgentForm(false);
  }

  useEffect(() => {
    // fetchDataNurses();
    // fetchDataUsers();
    // handleGetAllRecords();
  }, [refresher]);
  const handle_booking = () => {
    navigate("/booking");
  };
  const house_status = [
    {
      key: "1",
      value: "BOOKED",
      label: "Booked",
    },

    {
      key: "2",
      value: "INTERESTED",
      label: "Interested",
    },

    {
      key: "3",
      value: "SOLD",
      label: "Sold",
    },

    {
      key: "3",
      value: "FULLY-SOLD",
      label: "Fully-Sold",
    },
  ];
  const Agent_Status = [
    {
      key: "1",
      value: "AGENT",
      label: "Agent",
      // onclick: () => handleOpenAgent(),
    },
    {
      key: "2",
      value: "NO AGENT",
      label: "No Agent",
    },
  ];

  const house_type = [
    {
      key: "1",
      value: "studio",
      label: "Studio",
    },
    {
      key: "2",
      value: "1-BR",
      label: "1-BR",
    },
    {
      key: "3",
      value: "1-BR+",
      label: "1-BR+",
    },
    {
      key: "4",
      value: "2-BR ",
      label: "2-BR ",
    },
    {
      key: "5",
      value: "2-BR+",
      label: "2-BR+",
    },
    {
      key: "6",
      value: "2-BR++ ",
      label: "2-BR++ ",
    },

    {
      key: "7",
      value: "3-BR  ",
      label: "3-BR ",
    },

    {
      key: "8",
      value: "3-BR+  ",
      label: "3-BR+ ",
    },

    {
      key: "9",
      value: "3-BR++  ",
      label: "3-BR++ ",
    },

    {
      key: "10",
      value: "4-BR",
      label: "4-BR",
    },
  ];

  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    timeZoneName: "short",
  };

  function generateAlphanumericId() {
    const alphanumericCharacters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let id = "";
    for (let i = 0; i < 6; i++) {
      id += alphanumericCharacters.charAt(
        Math.floor(Math.random() * alphanumericCharacters.length)
      );
    }
    return id;
  }

  function reloadPage() {
    window.location.reload();
  }

  const formikMain = useFormik({
    initialValues: {
      client_firstname: "",
      client_lastname: "",
      client_secondname: "",
      client_phonenumber: "",
      client_email: "",
      client_nat_id: "",
      client_kra_pin: "",
      house_status: "",
      down_payment: "",
      selectedDate: null,

      // client_email: "",
      // client_nat_id: "",
    },

    validationSchema: validationSchema_clientDetails,

    onSubmit: (values) => {
      let downPayment = 0;

      console.log("NEW RECORD", values);
      // console.log("selected date:->", values.selectedDate);
      //console.log("house status", values.house_status);

      //console.error("payment mode:->", paymentMode);
      let paymentFlagPercentage = false;
      if (paymentMode == "percentage") {
        paymentFlagPercentage = true;
      } else if (paymentMode == "cash") {
        paymentFlagPercentage = false;
      }

      const corrected_down_payment = values.down_payment.replace(/,/g, "");

      let totalSellingPrice = 0;

      house_information.forEach((house) => {
        totalSellingPrice += parseInt(house.selling_price);
      });

      const percentage = parseInt(corrected_down_payment);

      console.log("house information", house_information);
      // console.log("house status", values.house_status);

      if (paymentFlagPercentage) {
        downPayment = totalSellingPrice * (percentage / 100);
      } else if (!paymentFlagPercentage) {
        downPayment = percentage;
      }
      if (values.house_status == "INTERESTED") {
        //     if (percentage != 0) {

        // console.log("Percentage is not equal to 0");
        // console.log("PERCENTAGE NOT CORRECT!");
        // setInterestedError(true);
        //     } else if (percentage == 0) {
        //console.log("PERCENTAGE IS CORRECT");
        //const downPayment = totalSellingPrice * (percentage / 100);
        downPayment = 0;
        console.error("downpayment set to 0!");

        const corrected_prices = {
          down_payment: downPayment.toString(),
        };
        let main_updatedObject = {
          ...values, // Spread existing properties
          ...corrected_prices, // Spread updated values
        };
        console.log("corrected downpayment price:->", corrected_down_payment);
        // VALIDATION
        const currentDate = new Date();
        const options = {
          weekday: "short",
          year: "numeric",
          month: "short",
          day: "numeric",
          hour: "numeric",
          minute: "numeric",
          second: "numeric",
          //timeZoneName: "short",
        };
        const options_selected = {
          // weekday: "short",
          day: "numeric",
          month: "numeric",
          year: "numeric",
          // hour: "numeric",
          // minute: "numeric",
          // second: "numeric",
          //timeZoneName: "short",
        };
        const formattedDate = currentDate.toLocaleDateString("en-GB", options);
        const date_entered = new Date(values.selectedDate).toLocaleDateString(
          "en-GB",
          options_selected
        );
        const record = {
          _id: generateAlphanumericId(),
          // agent_name: "Nancy",
          status: "ACTIVE",
          date: date_entered,
          created_at: formattedDate,
          client_information: {
            first_name: main_updatedObject.client_firstname,
            second_name: main_updatedObject.client_secondname,
            last_name: main_updatedObject.client_lastname,
            phone_number: main_updatedObject.client_phonenumber,
            nat_id: main_updatedObject.client_nat_id,
            house_status: main_updatedObject.house_status,
            kra_pin: main_updatedObject.client_kra_pin,
            email: main_updatedObject.client_email,
            created_at: formattedDate,
          },
          house_information: house_information,
          broker_information: clickedAgent,
          installments: [
            {
              amount: main_updatedObject.down_payment,
              installment_type: "Quarterly",
              created_at: formattedDate,
            },
          ],

          bonus_payments: [
            {
              // amount: main_updatedObject.down_payment,
              // installment_type: "Quarterly",
              // created_at: formattedDate,
            },
          ],
        };
        console.log("RECORD-INTERESTED", record);
        if (record.house_information.length != 0) {
          console.log(" INFORMATION!");
          postData(record);
          setLoading(true);
          // setTimeout(() => {
          //   setOpenBooking(false);
          //   reloadPage();
          // }, 3000);
          // setOpenRecordAddedSuccessfully(true);
        } else {
          setNoHouseInformationAlert(true);
        }
        //  }
      } else if (values.house_status == "BOOKED") {
        if (percentage == 0) {
          console.log("Percentage is equal to 0");
          console.log("PERCENTAGE NOT CORRECT!");
          setInterestedError(true);
          setBookedError(true);
        } else if (percentage != 0) {
          console.log("PERCENTAGE IS CORRECT");
          //const downPayment = totalSellingPrice * (percentage / 100) + 100000;

          downPayment = 100000;

          console.error("downpayment within booked", downPayment);

          const corrected_prices = {
            down_payment: downPayment.toString(),
          };
          let main_updatedObject = {
            ...values, // Spread existing properties
            ...corrected_prices, // Spread updated values
          };
          console.log("corrected downpayment price:->", corrected_down_payment);
          // VALIDATION
          const currentDate = new Date();
          const options = {
            weekday: "short",
            year: "numeric",
            month: "short",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
            //timeZoneName: "short",
          };
          const options_selected = {
            // weekday: "short",
            day: "numeric",
            month: "numeric",
            year: "numeric",
            // hour: "numeric",
            // minute: "numeric",
            // second: "numeric",
            //timeZoneName: "short",
          };
          const formattedDate = currentDate.toLocaleDateString(
            "en-GB",
            options
          );
          const date_entered = new Date(values.selectedDate).toLocaleDateString(
            "en-GB",
            options_selected
          );
          const record = {
            _id: generateAlphanumericId(),
            // agent_name: "Nancy",
            status: "ACTIVE",
            date: date_entered,
            created_at: formattedDate,
            client_information: {
              first_name: main_updatedObject.client_firstname,
              second_name: main_updatedObject.client_secondname,
              last_name: main_updatedObject.client_lastname,
              phone_number: main_updatedObject.client_phonenumber,
              nat_id: main_updatedObject.client_nat_id,
              house_status: main_updatedObject.house_status,
              kra_pin: main_updatedObject.client_kra_pin,
              email: main_updatedObject.client_email,
              created_at: formattedDate,
            },
            house_information: house_information,
            broker_information: clickedAgent,
            installments: [
              {
                amount: main_updatedObject.down_payment,
                installment_type: "Quarterly",
                created_at: formattedDate,
              },
            ],

            bonus_payments: [
              {
                // amount: main_updatedObject.down_payment,
                // installment_type: "Quarterly",
                // created_at: formattedDate,
              },
            ],
          };
          console.log("RECORD-BOOKED", record);
          if (record.house_information.length != 0) {
            console.log(" INFORMATION!");
            postData(record);
            setLoading(true);
            // setTimeout(() => {
            //   setOpenBooking(false);
            //   reloadPage();
            // }, 3000);
            // setOpenRecordAddedSuccessfully(true);
          } else {
            setNoHouseInformationAlert(true);
          }
        }
      } else if (values.house_status == "SOLD") {
        // checkInterested = true;
        console.log("SOLD HERER", paymentFlagPercentage);

        console.error("percentage", percentage);

        if (!percentage) {
          console.error("percentage contains something", percentage);
          setSoldError(true);
        }
        console.error("percentage NAN:->>", percentage);

        if (paymentFlagPercentage) {
          downPayment = totalSellingPrice * (percentage / 100);
          ///  if (percentage < 100) {
          console.log("Percentage is less than 100");
          console.log("PERCENTAGE NOT CORRECT!");
          //setSoldError(true);
          ///setBookedError(true);
          //  } else if (percentage == 100) {
          //console.log("PERCENTAGE IS CORRECT");
          //const downPayment = totalSellingPrice * (percentage / 100);
          //console.error("downpayment within SOLD", downPayment);

          const corrected_prices = {
            down_payment: downPayment.toString(),
          };
          let main_updatedObject = {
            ...values, // Spread existing properties
            ...corrected_prices, // Spread updated values
          };
          //console.log("corrected downpayment price:->", corrected_down_payment);
          // VALIDATION
          const currentDate = new Date();
          const options = {
            weekday: "short",
            year: "numeric",
            month: "short",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
            //timeZoneName: "short",
          };
          const options_selected = {
            // weekday: "short",
            day: "numeric",
            month: "numeric",
            year: "numeric",
            // hour: "numeric",
            // minute: "numeric",
            // second: "numeric",
            //timeZoneName: "short",
          };
          const formattedDate = currentDate.toLocaleDateString(
            "en-GB",
            options
          );
          const date_entered = new Date(values.selectedDate).toLocaleDateString(
            "en-GB",
            options_selected
          );
          const record = {
            _id: generateAlphanumericId(),
            // agent_name: "Nancy",
            status: "ACTIVE",
            date: date_entered,
            created_at: formattedDate,
            client_information: {
              first_name: main_updatedObject.client_firstname,
              second_name: main_updatedObject.client_secondname,
              last_name: main_updatedObject.client_lastname,
              phone_number: main_updatedObject.client_phonenumber,
              nat_id: main_updatedObject.client_nat_id,
              house_status: main_updatedObject.house_status,
              kra_pin: main_updatedObject.client_kra_pin,
              email: main_updatedObject.client_email,
              created_at: formattedDate,
            },
            house_information: house_information,
            broker_information: clickedAgent,
            installments: [
              {
                amount: main_updatedObject.down_payment,
                installment_type: "Quarterly",
                created_at: formattedDate,
              },
            ],

            bonus_payments: [
              {
                // amount: main_updatedObject.down_payment,
                // installment_type: "Quarterly",
                // created_at: formattedDate,
              },
            ],
          };
          console.log("RECORD-SOLD", record);
          if (record.house_information.length != 0) {
            console.log(" INFORMATION!");
            postData(record);
            setLoading(true);
            // setTimeout(() => {
            //   setOpenBooking(false);
            //   reloadPage();
            // }, 3000);
            // setOpenRecordAddedSuccessfully(true);
          } else {
            setNoHouseInformationAlert(true);
          }
          //   }
        } else if (!paymentFlagPercentage) {
          downPayment = percentage;

          console.error("downpayment when its not percentage", downPayment);

          const corrected_prices = {
            down_payment: downPayment.toString(),
          };
          let main_updatedObject = {
            ...values, // Spread existing properties
            ...corrected_prices, // Spread updated values
          };
          //console.log("corrected downpayment price:->", corrected_down_payment);
          // VALIDATION
          const currentDate = new Date();

          const options_selected = {
            // weekday: "short",
            day: "numeric",
            month: "numeric",
            year: "numeric",
            // hour: "numeric",
            // minute: "numeric",
            // second: "numeric",
            //timeZoneName: "short",
          };
          const formattedDate = currentDate.toLocaleDateString(
            "en-GB",
            options
          );
          const date_entered = new Date(values.selectedDate).toLocaleDateString(
            "en-GB",
            options_selected
          );
          const record = {
            _id: generateAlphanumericId(),
            // agent_name: "Nancy",
            status: "ACTIVE",
            date: date_entered,
            created_at: formattedDate,
            client_information: {
              first_name: main_updatedObject.client_firstname,
              second_name: main_updatedObject.client_secondname,
              last_name: main_updatedObject.client_lastname,
              phone_number: main_updatedObject.client_phonenumber,
              nat_id: main_updatedObject.client_nat_id,
              house_status: main_updatedObject.house_status,
              kra_pin: main_updatedObject.client_kra_pin,
              email: main_updatedObject.client_email,
              created_at: formattedDate,
            },
            house_information: house_information,
            broker_information: clickedAgent,
            installments: [
              {
                amount: main_updatedObject.down_payment,
                installment_type: "Quarterly",
                created_at: formattedDate,
              },
            ],

            bonus_payments: [
              {
                // amount: main_updatedObject.down_payment,
                // installment_type: "Quarterly",
                // created_at: formattedDate,
              },
            ],
          };
          console.log("RECORD", record);
          if (record.house_information.length != 0) {
            console.log(" INFORMATION!");
            //postData(record);
            setLoading(true);
            // setTimeout(() => {
            //   setOpenBooking(false);
            //   reloadPage();
            // }, 3000);
            // setOpenRecordAddedSuccessfully(true);
          } else {
            setNoHouseInformationAlert(true);
          }
        }
      } else if (values.house_status == "FULLY-SOLD") {
        console.log("FULLY SOLD STATE", totalSellingPrice);

        // checkInterested = true;
        console.log("SOLD HERER", paymentFlagPercentage);

        //console.error("percentage", percentage);

        // if (!percentage) {
        //   console.error("percentage contains something", percentage);
        //   setSoldError(true);
        // }
        //console.error("percentage NAN:->>", percentage);
        downPayment = totalSellingPrice;

        //downPayment = totalSellingPrice;

        console.error("downpayment when its not percentage", downPayment);

        const corrected_prices = {
          down_payment: downPayment.toString(),
        };
        let main_updatedObject = {
          ...values, // Spread existing properties
          ...corrected_prices, // Spread updated values
        };

        const currentDate = new Date();
        //console.log("corrected downpayment price:->", corrected_down_payment);
        // VALIDATION
        const options = {
          weekday: "short",
          year: "numeric",
          month: "short",
          day: "numeric",
          hour: "numeric",
          minute: "numeric",
          second: "numeric",
          //timeZoneName: "short",
        };
        const options_selected = {
          // weekday: "short",
          day: "numeric",
          month: "numeric",
          year: "numeric",
          // hour: "numeric",
          // minute: "numeric",
          // second: "numeric",
          //timeZoneName: "short",
        };
        const formattedDate = currentDate.toLocaleDateString("en-GB", options);

        const date_entered = new Date(values.selectedDate).toLocaleDateString(
          "en-GB",
          options_selected
        );
        const record = {
          _id: generateAlphanumericId(),
          // agent_name: "Nancy",
          status: "ACTIVE",
          date: date_entered,
          created_at: formattedDate,
          client_information: {
            first_name: main_updatedObject.client_firstname,
            second_name: main_updatedObject.client_secondname,
            last_name: main_updatedObject.client_lastname,
            phone_number: main_updatedObject.client_phonenumber,
            nat_id: main_updatedObject.client_nat_id,
            house_status: main_updatedObject.house_status,
            kra_pin: main_updatedObject.client_kra_pin,
            email: main_updatedObject.client_email,
            created_at: formattedDate,
          },
          house_information: house_information,
          broker_information: clickedAgent,
          installments: [
            {
              amount: main_updatedObject.down_payment,
              installment_type: "Quarterly",
              created_at: formattedDate,
            },
          ],

          bonus_payments: [
            {
              // amount: main_updatedObject.down_payment,
              // installment_type: "Quarterly",
              // created_at: formattedDate,
            },
          ],
        };
        console.log("RECORD", record);
        if (record.house_information.length != 0) {
          console.log(" INFORMATION!");
          postData(record);
          setLoading(true);
          // setTimeout(() => {
          //   setOpenBooking(false);
          //   reloadPage();
          // }, 3000);
          // setOpenRecordAddedSuccessfully(true);
        } else {
          setNoHouseInformationAlert(true);
        }

        if (paymentFlagPercentage) {
          //downPayment = totalSellingPrice * (percentage / 100);
          ///  if (percentage < 100) {
          console.log("Percentage is less than 100");
          console.log("PERCENTAGE NOT CORRECT!");
          //setSoldError(true);
          ///setBookedError(true);
          //  } else if (percentage == 100) {
          //console.log("PERCENTAGE IS CORRECT");
          //const downPayment = totalSellingPrice * (percentage / 100);
          //console.error("downpayment within SOLD", downPayment);

          const corrected_prices = {
            down_payment: downPayment.toString(),
          };
          let main_updatedObject = {
            ...values, // Spread existing properties
            ...corrected_prices, // Spread updated values
          };
          //console.log("corrected downpayment price:->", corrected_down_payment);
          // VALIDATION
          const currentDate = new Date();
          const options = {
            weekday: "short",
            year: "numeric",
            month: "short",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
            //timeZoneName: "short",
          };
          const options_selected = {
            // weekday: "short",
            day: "numeric",
            month: "numeric",
            year: "numeric",
            // hour: "numeric",
            // minute: "numeric",
            // second: "numeric",
            //timeZoneName: "short",
          };
          const formattedDate = currentDate.toLocaleDateString(
            "en-GB",
            options
          );
          const date_entered = new Date(values.selectedDate).toLocaleDateString(
            "en-GB",
            options_selected
          );
          const record = {
            _id: generateAlphanumericId(),
            // agent_name: "Nancy",
            status: "ACTIVE",
            date: date_entered,
            created_at: formattedDate,
            client_information: {
              first_name: main_updatedObject.client_firstname,
              second_name: main_updatedObject.client_secondname,
              last_name: main_updatedObject.client_lastname,
              phone_number: main_updatedObject.client_phonenumber,
              nat_id: main_updatedObject.client_nat_id,
              house_status: main_updatedObject.house_status,
              kra_pin: main_updatedObject.client_kra_pin,
              email: main_updatedObject.client_email,
              created_at: formattedDate,
            },
            house_information: house_information,
            broker_information: clickedAgent,
            installments: [
              {
                amount: main_updatedObject.down_payment,
                installment_type: "Quarterly",
                created_at: formattedDate,
              },
            ],

            bonus_payments: [
              {
                // amount: main_updatedObject.down_payment,
                // installment_type: "Quarterly",
                // created_at: formattedDate,
              },
            ],
          };
          console.log("RECORD-FULLY-SOLD", record);
          if (record.house_information.length != 0) {
            console.log(" INFORMATION!");
            // postData(record);
            setLoading(true);
            // setTimeout(() => {
            //   setOpenBooking(false);
            //   reloadPage();
            // }, 3000);
            // setOpenRecordAddedSuccessfully(true);
          } else {
            setNoHouseInformationAlert(true);
          }
          //   }
        } else if (!paymentFlagPercentage) {
        }
      }

      // console.log("check interested is:-", checkInterested);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const today = new Date();
  today.setHours(0, 0, 0, 0);

  const formikEditHouseDetails = useFormik({
    // {information_to_be_Edited !== undefined && (
    //   <p>{information_to_be_Edited.asking_price}</p>
    // )}
    initialValues: {
      // edit_location: "",
      // edit_residence: "",
      // edit_house_number: "",
      // edit_selling_price: "",
      // edit_asking_price: "",
      // edit_house_status: "",
      // edit_house_type: "",

      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
      house_type: "",
      house_idx: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      let house_array_to_be_edited = [];
      //const updatedHouseArray = [];
      console.log("edit values to be submitted", values, houseIndex);

      // location: "",
      // residence: "",
      // house_number: "",
      // selling_price: "",
      // asking_price: "",
      // house_status: "",
      // house_type: "",

      //const updatedList = [...wards.slice(0, index), updatedArray, ...wards.slice(index + 1)];

      const house_object = {
        location: values.location,
        residence: values.residence,
        house_number: values.house_number,
        selling_price: values.selling_price,
        asking_price: values.asking_price,
        house_status: values.house_status,
        house_type: values.house_type,
      };
      updatedHouseArray.push(house_object);
      //house_array_to_be_edited.push(values);
      house_array_to_be_edited = [...house_information];
      const updatedList = [
        ...house_information.slice(0, values.house_idx),
        updatedHouseArray,
        ...house_information.slice(values.house_idx + 1),
      ];

      console.log("updated list", updatedList);

      setHouse_Information(updatedList);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
    },
  });

  const formikAgentDetails = useFormik({
    // {information_to_be_Edited !== undefined && (
    //   <p>{information_to_be_Edited.asking_price}</p>
    // )}
    initialValues: {
      first_name: "",
      last_name: "",
      phone_number: "",
      company_name: "",
      company_email: "",
      // agent_email: "",
      // agent_nat_id: "",
    },

    validationSchema: validationSchema_agentDeatails,
    onSubmit: (values) => {
      console.log("Agent Details", values);

      //setAgentInformation(values);
      setclickedAgent(values);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      setOpenAgentForm(false);
      setOpenpopUp(false);
      setOpenAgentAddedSuccessfully(true);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const handleClose_pop = () => {
    setOpenpopUp(false);
  };

  function handlecloseAgentForm() {
    setOpenAgentForm(false);
  }

  // Function to handle change in house status selection
  const handleHouseStatusChange = (event) => {
    const selectedStatus = event.target.value;
    setSelectedHouseStatus(selectedStatus); // Update state variable
    formikMain.setFieldValue("house_status", selectedStatus); // Update Formik form values
  };

  const formikHouseDetails = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      // house_status: "",
    },

    validationSchema: validationSchema_housedetails,
    onSubmit: (values) => {
      console.log("values", values);

      var matchFound = false;

      // Check each object in house_array

      console.log("match found", matchFound);
      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      console.log("updated object", updatedObject);
      house_array.push(updatedObject);

      const hasDuplicate = house_information.some(
        (house) => house.house_number === values.house_number
      );

      if (!hasDuplicate) {
        setHouse_Information((prevHouseInformation) => {
          return [...prevHouseInformation, ...house_array];
        });
        setOpenHouseAddedSuccessfully(true);
        // setOpenAddHouseInfor(true);
      } else if (hasDuplicate) {
        alert("HAS DUPLICATE CANNOT ADD!!");
      }

      // console.log("house array:", house_array);

      //const isDuplicate = house_array.some(values => house_information.some(info => /* Your comparison logic here */ ));
      console.log("house information", house_information);

      console.log("house array:", house_array);

      console.log("house information array", house_information);

      //const [house_information, setHouse_Information] = React.useState([]);

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      setOpenHouseDetailsModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  return (
    <>
      <AgentAddedSuccessuflly
        open={openAgentAddedSuccessfully}
        autoHideDuration={3000}
        onClose={() => setOpenAgentAddedSuccessfully(false)}
      />
      <NohouseInform
        open={houseInformationAlert}
        autoHideDuration={5000}
        onClose={() => setNoHouseInformationAlert(false)}
      />
      <InterestedError
        open={interestedError}
        autoHideDuration={10000}
        onClose={() => setInterestedError(false)}
      />
      <BookedError
        open={bookingError}
        autoHideDuration={10000}
        onClose={() => setBookedError(false)}
      />
      <SoldError
        open={soldError}
        autoHideDuration={10000}
        onClose={() => setSoldError(false)}
      />

      <Modal
        open={openpopup}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className=" m-4">
              <span className="font-bold m-2">Agent Not Available.</span>
              <br></br> <span>Do you wish to add Agent?</span>
            </h2>

            {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
          </div>

          <form>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3"></div>
              <div className="flex flex-row gap-4">
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={handleOpenAgentForm}
                >
                  Add Agent
                </Button>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={handleClose_pop}
                >
                  cancel
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAgentForm}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
            <h2 className="font-bold m-4"> Agent's Details </h2>
            <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form onSubmit={formikAgentDetails.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type="text"
                  id="first_name"
                  name="first_name"
                  label="First Name"
                  placeholder="First Name"
                  value={formikAgentDetails.values.first_name}
                  error={
                    formikAgentDetails.touched.first_name &&
                    Boolean(formikAgentDetails.errors.first_name)
                  }
                  helperText={
                    formikAgentDetails.touched.first_name &&
                    formikAgentDetails.errors.first_name
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikAgentDetails.handleChange(e);
                  }}
                  onBlur={formikAgentDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="last_name"
                  name="last_name"
                  label="Last Name"
                  placeholder="Last Name"
                  value={formikAgentDetails.values.last_name}
                  error={
                    formikAgentDetails.touched.last_name &&
                    Boolean(formikAgentDetails.errors.last_name)
                  }
                  helperText={
                    formikAgentDetails.touched.last_name &&
                    formikAgentDetails.errors.last_name
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikAgentDetails.handleChange(e);
                  }}
                  onBlur={formikAgentDetails.handleBlur}
                />

                {/* <TextField
                  type="text"
                  id="agent_nat_id"
                  name="client_nat_id"
                  label="ID Number"
                  placeholder="ID Number"
                  value={formikAgentDetails.values.agent_nat_id}
                  onChange={formikAgentDetails.handleChange}
                  onBlur={formikAgentDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="agent_email"
                  name="agent_email"
                  label="email"
                  placeholder="Agent's Email"
                  value={formikAgentDetails.values.agent_email}
                  onChange={formikAgentDetails.handleChange}
                  onBlur={formikAgentDetails.handleBlur}
                /> */}
              </div>

              <TextField
                type="text"
                id="phone_number"
                name="phone_number"
                label="Phone Number"
                placeholder="Phone Number"
                value={formikAgentDetails.values.phone_number}
                //onChange={formikAgentDetails.handleChange}
                error={
                  formikAgentDetails.touched.phone_number &&
                  Boolean(formikAgentDetails.errors.phone_number)
                }
                helperText={
                  formikAgentDetails.touched.phone_number &&
                  formikAgentDetails.errors.phone_number
                }
                onChange={(e) => {
                  e.target.value = e.target.value.toUpperCase();
                  formikAgentDetails.handleChange(e);
                }}
                onBlur={formikAgentDetails.handleBlur}
              />

              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type="text"
                  id="company_name"
                  name="company_name"
                  label="Company Name"
                  placeholder="Company Name"
                  value={formikAgentDetails.values.company_name}
                  //onChange={formikAgentDetails.handleChange}
                  error={
                    formikAgentDetails.touched.company_name &&
                    Boolean(formikAgentDetails.errors.company_name)
                  }
                  helperText={
                    formikAgentDetails.touched.company_name &&
                    formikAgentDetails.errors.company_name
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikAgentDetails.handleChange(e);
                  }}
                  onBlur={formikAgentDetails.handleBlur}
                />
                <TextField
                  type="text"
                  id="company_email"
                  name="company_email"
                  label="Company Email"
                  placeholder="Company Email"
                  value={formikAgentDetails.values.company_email}
                  //onChange={formikAgentDetails.handleChange}
                  error={
                    formikAgentDetails.touched.company_email &&
                    Boolean(formikAgentDetails.errors.company_email)
                  }
                  helperText={
                    formikAgentDetails.touched.company_email &&
                    formikAgentDetails.errors.company_email
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value;
                    formikAgentDetails.handleChange(e);
                  }}
                  onBlur={formikAgentDetails.handleBlur}
                />
              </div>

              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save Agent
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <HouseAddedSuccessfully
        open={openHouseAddedSuccessfully}
        autoHideDuration={3000}
        onClose={() => setOpenHouseAddedSuccessfully(false)}
      />
      <Modal open={openAddHouseInfor}>
        <Box sx={style_box}>
          <Typography sx={{ fontWeight: "bold" }}>Add House Infor ?</Typography>
          <div className="flex flex-row gap-3 mt-3">
            <Button
              variant="contained"
              size="small"
              sx={{ backgroundColor: "#242333" }}
            >
              YES
            </Button>
            <Button
              variant="contained"
              size="small"
              color="error"
              onClick={handleClose_AddHouseInfor}
            >
              CANCEL
            </Button>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openHouseDetailsModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4"> Add House Information </h2>
            <CloseIcon
              fontSize="40"
              onClick={handleCloseHouseDetailsModal}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form onSubmit={formikHouseDetails.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3 mb-4">
                <TextField
                  type="text"
                  id="location"
                  name="location"
                  label="Location"
                  placeholder="Location"
                  value={formikHouseDetails.values.location}
                  error={
                    formikHouseDetails.touched.location &&
                    Boolean(formikHouseDetails.errors.location)
                  }
                  helperText={
                    formikHouseDetails.touched.location &&
                    formikHouseDetails.errors.location
                  }
                  //onChange={formikHouseDetails.handleChange}

                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikHouseDetails.handleChange(e);
                  }}
                  onBlur={formikHouseDetails.handleBlur}
                />

                <TextField
                  type="text"
                  id="residence"
                  name="residence"
                  label="House Residence"
                  placeholder="house residence"
                  value={formikHouseDetails.values.residence}
                  error={
                    formikHouseDetails.touched.residence &&
                    Boolean(formikHouseDetails.errors.residence)
                  }
                  helperText={
                    formikHouseDetails.touched.residence &&
                    formikHouseDetails.errors.residence
                  }
                  onChange={(e) => {
                    e.target.value = e.target.value.toUpperCase();
                    formikHouseDetails.handleChange(e);
                  }}
                  onBlur={formikHouseDetails.handleBlur}
                />
              </div>
            </div>
            <TextField
              type="text"
              id="house_number"
              name="house_number"
              label="House"
              placeholder="House Number"
              value={formikHouseDetails.values.house_number}
              //onChange={formikHouseDetails.handleChange}
              error={
                formikHouseDetails.touched.house_number &&
                Boolean(formikHouseDetails.errors.house_number)
              }
              helperText={
                formikHouseDetails.touched.house_number &&
                formikHouseDetails.errors.house_number
              }
              onChange={(e) => {
                e.target.value = e.target.value.toUpperCase();
                formikHouseDetails.handleChange(e);
              }}
              onBlur={formikHouseDetails.handleBlur}
            />

            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="asking_price"
                name="asking_price"
                label="Asking Price"
                placeholder="Asking Price"
                error={
                  formikHouseDetails.touched.asking_price &&
                  Boolean(formikHouseDetails.errors.asking_price)
                }
                helperText={
                  formikHouseDetails.touched.asking_price &&
                  formikHouseDetails.errors.asking_price
                }
                value={formikHouseDetails.values.asking_price}
                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikHouseDetails.setFieldValue(
                    "asking_price",
                    formattedValue
                  );
                }}
                onBlur={formikHouseDetails.handleBlur}
              />
              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Selling Price"
                placeholder="Selling Price"
                error={
                  formikHouseDetails.touched.selling_price &&
                  Boolean(formikHouseDetails.errors.selling_price)
                }
                helperText={
                  formikHouseDetails.touched.selling_price &&
                  formikHouseDetails.errors.selling_price
                }
                value={formikHouseDetails.values.selling_price}
                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikHouseDetails.setFieldValue(
                    "selling_price",
                    formattedValue // Corrected to set "selling_price" instead of "asking_price"
                  );
                }}
                onBlur={formikHouseDetails.handleBlur}
              />
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <FormControl>
                <TextField
                  id="house_type"
                  select
                  name="house_type"
                  label="House type"
                  placeholder="house_type"
                  fullWidth
                  error={
                    formikHouseDetails.touched.house_type &&
                    Boolean(formikHouseDetails.errors.house_type)
                  }
                  helperText={
                    formikHouseDetails.touched.house_type &&
                    formikHouseDetails.errors.house_type
                  }
                  // value={formikHouseDetails.values.house_status}
                  onChange={formikHouseDetails.handleChange}
                  onBlur={formikHouseDetails.handleBlur}
                >
                  {house_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                  // onClick={handleOpen_AddHouseInfor}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openEditModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
            <h2 className="font-bold m-4"> Edit House Information </h2>
            <CloseIcon
              fontSize="40"
              onClick={handleCloseEditModal}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form onSubmit={formikEditHouseDetails.handleSubmit}>
            <TextField
              type="text"
              id="house_idx"
              name="house_idx"
              label="Index [read-only]"
              placeholder="House_index"
              value={formikEditHouseDetails.values.house_idx}
              InputProps={{
                readOnly: true,
              }}
              onChange={formikEditHouseDetails.handleChange}
              onBlur={formikEditHouseDetails.handleBlur}
            />
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="location"
                name="location"
                label="Location"
                placeholder="First Name"
                value={formikEditHouseDetails.values.location}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="residence"
                name="residence"
                label="House Residence"
                placeholder="Residence"
                value={formikEditHouseDetails.values.residence}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="house_number"
                name="house_number"
                label="house unit"
                placeholder="House Unit"
                value={formikEditHouseDetails.values.house_number}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />
            </div>
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="asking_price"
                name="asking_price"
                label="Asking Price"
                placeholder="Asking Price"
                value={formikEditHouseDetails.values.asking_price}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Selling Price"
                placeholder="Selling Price"
                value={formikEditHouseDetails.values.selling_price}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />
            </div>

            <div className="flex flex-row gap-3 mt-3">
              <FormControl>
                <TextField
                  id="house_status"
                  select
                  name="house_status"
                  label="Status"
                  placeholder="House status"
                  fullWidth
                  value={formikEditHouseDetails.values.house_status}
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                >
                  {house_status.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <div className="flex flex-row gap-3 mt-3">
              <FormControl>
                <TextField
                  id="house_type"
                  select
                  name="house_type"
                  label="House type"
                  placeholder="house_type"
                  fullWidth
                  value={formikEditHouseDetails.values.house_type}
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                >
                  {house_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <Button type="submit">Save</Button>
          </form>
        </Box>
      </Modal>
      <RecordAddedSuccefull
        open={openRecordAddedSuccessfully}
        autoHideDuration={3000}
        onClose={() => setOpenRecordAddedSuccessfully(false)}
      />
      <Modal
        open={openBooking}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", fontSize: ["14px", "18px"] }}
            >
              Add New Record
            </Typography>
            {/* <h2 className="font-bold m-4"> Add Record </h2> */}
            <CloseIcon
              fontSize="40"
              onClick={handleClose_booking}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form onSubmit={formikMain.handleSubmit}>
            <div className="flex flex-col gap-4 ">
              <div className="flex flex-col gap-3">
                <Typography
                  id="modal-modal-title"
                  sx={{ fontWeight: "bold", fontSize: ["14px", "16px"] }}
                >
                  Client's Details
                </Typography>
                <div className="flex flex-col gap-3">
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="client_firstname"
                      name="client_firstname"
                      label="First Name"
                      placeholder="First Name"
                      value={formikMain.values.client_firstname}
                      error={
                        formikMain.touched.client_firstname &&
                        Boolean(formikMain.errors.client_firstname)
                      }
                      helperText={
                        formikMain.touched.client_firstname &&
                        formikMain.errors.client_firstname
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="client_secondname"
                      name="client_secondname"
                      label="Second Name"
                      placeholder="Second Name"
                      value={formikMain.values.client_secondname}
                      error={
                        formikMain.touched.client_secondname &&
                        Boolean(formikMain.errors.client_secondname)
                      }
                      helperText={
                        formikMain.touched.client_secondname &&
                        formikMain.errors.client_secondname
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />
                  </div>

                  <TextField
                    type="text"
                    id="client_lastname"
                    name="client_lastname"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikMain.values.client_lastname}
                    //onChange={formikMain.handleChange}
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikMain.handleChange(e);
                    }}
                    error={
                      formikMain.touched.client_lastname &&
                      Boolean(formikMain.errors.client_lastname)
                    }
                    helperText={
                      formikMain.touched.client_lastname &&
                      formikMain.errors.client_lastname
                    }
                    onBlur={formikMain.handleBlur}
                  />

                  <div className="flex flex-row gap-3 ">
                    <TextField
                      type="text"
                      id="client_phonenumber"
                      name="client_phonenumber"
                      label="Phone Number"
                      placeholder="Phone Number"
                      value={formikMain.values.client_phonenumber}
                      error={
                        formikMain.touched.client_phonenumber &&
                        Boolean(formikMain.errors.client_phonenumber)
                      }
                      helperText={
                        formikMain.touched.client_phonenumber &&
                        formikMain.errors.client_phonenumber
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="client_nat_id"
                      name="client_nat_id"
                      label="ID Number"
                      placeholder="ID Number"
                      value={formikMain.values.client_nat_id}
                      error={
                        formikMain.touched.client_nat_id &&
                        Boolean(formikMain.errors.client_nat_id)
                      }
                      helperText={
                        formikMain.touched.client_nat_id &&
                        formikMain.errors.client_nat_id
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                    />
                    {/* <TextField
                      type="text"
                      id="client_lastname"
                      name="client_lastname"
                      label="Last Name"
                      placeholder="Last Name"
                      value={formikMain.values.client_lastname}
                      error={
                        formikMain.touched.client_lastname &&
                        Boolean(formikMain.errors.client_lastname)
                      }
                      helperText={
                        formikMain.touched.client_lastname &&
                        formikMain.errors.client_lastname
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}
                  </div>

                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="client_email"
                      name="client_email"
                      label="email"
                      placeholder="Client's Email"
                      value={formikMain.values.client_email}
                      onChange={formikMain.handleChange}
                      // onChange={(e) => {
                      //   e.target.value = e.target.value.toUpperCase();
                      //   formikMain.handleChange(e);
                      // }}
                      error={
                        formikMain.touched.client_email &&
                        Boolean(formikMain.errors.client_email)
                      }
                      helperText={
                        formikMain.touched.client_email &&
                        formikMain.errors.client_email
                      }
                      onBlur={formikMain.handleBlur}
                    />

                    <TextField
                      type="text"
                      id="client_kra_pin"
                      name="client_kra_pin"
                      label="KRA PIN"
                      placeholder="KRA PIN"
                      value={formikMain.values.client_kra_pin}
                      onChange={formikMain.handleChange}
                      // onChange={(e) => {
                      //   e.target.value = e.target.value.toUpperCase();
                      //   formikMain.handleChange(e);
                      // }}
                      // error={
                      //   formikMain.touched.client_email &&
                      //   Boolean(formikMain.errors.client_email)
                      // }
                      // helperText={
                      //   formikMain.touched.client_email &&
                      //   formikMain.errors.client_email
                      // }
                      onBlur={formikMain.handleBlur}
                    />
                  </div>

                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={["DatePicker"]}>
                      <DatePicker
                        label="Enter Date"
                        value={formikMain.values.selectedDate} // Pass value from Formik
                        onChange={(date) =>
                          formikMain.setFieldValue("selectedDate", date)
                        } // Update Formik field value
                        // maxDate={today}
                        // maxDate={new Date()}
                      />
                    </DemoContainer>
                  </LocalizationProvider>
                </div>

                {information_to_be_Edited !== undefined && (
                  <p>{information_to_be_Edited.asking_price}</p>
                )}

                <CardMedia height="540" />
                <CardContent>
                  <Typography
                    id="modal-modal-title"
                    sx={{ fontWeight: "bold", marginBottom: 2 }}
                  >
                    House Details
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <div>
                      {house_information.length > 0 ? (
                        <ul>
                          {house_information.map((house_info, index) => (
                            <li
                              key={index}
                              style={{
                                border: "1px solid #ccc",
                                padding: "8px",
                                marginBottom: "8px",
                                justifyContent: "space-between",
                                // width: "50px"
                              }}
                            >
                              <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span>
                              {house_info.location},&nbsp; &nbsp;
                              {house_info.residence},&nbsp; &nbsp;
                              {house_info.house_type},&nbsp;&nbsp;
                              {/* {house_info.selling_price},&nbsp;&nbsp; */}
                              {parseFloat(
                                house_info.selling_price
                              ).toLocaleString()}{" "}
                              &nbsp;
                              {house_info.house_number},&nbsp;&nbsp;
                              <Button
                                variant="contained"
                                sx={{ backgroundColor: "red", marginLeft: 3 }}
                                size="small"
                                onClick={() => handleRemove("Wards", index)}
                              >
                                Remove
                              </Button>
                            </li>
                          ))}
                        </ul>
                      ) : (
                        <div>
                          <ul>
                            {/* {house_information.map((house_info, index) => ( */}
                            <li
                              // key={index}
                              style={{
                                border: "1px solid #ccc",
                                padding: "8px",
                                marginBottom: "8px",
                              }}
                            >
                              <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span>
                              No Houses Added!
                            </li>
                            {/* ))} */}
                          </ul>
                        </div>
                      )}
                    </div>
                  </Typography>

                  <CardActions>
                    <div className="flex flex-row gap-6">
                      <Button
                        variant="contained"
                        sx={{ backgroundColor: "#242333" }}
                        size="small"
                        onClick={handleOpenModal}
                      >
                        + Add Houses
                      </Button>
                    </div>
                  </CardActions>
                </CardContent>
              </div>
            </div>

            <div className="flex flex-col gap-3 mb-3">
              <FormLabel sx={{ fontWeight: "bold", color: "#000" }}>
                House status
              </FormLabel>

              <FormControl>
                <TextField
                  id="house_status"
                  select
                  name="house_status"
                  label="Status"
                  placeholder="Hs Price"
                  fullWidth
                  value={selectedHouseStatus}
                  onChange={handleHouseStatusChange} // Handle change manually
                  onBlur={formikMain.handleBlur}
                  error={
                    formikMain.touched.house_status &&
                    Boolean(formikMain.errors.house_status)
                  }
                  helperText={
                    formikMain.touched.house_status &&
                    formikMain.errors.house_status
                  }
                >
                  {house_status.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <div className="flex flex-row gap-3 mt-">
              <div className="flex flex-row gap-3 mt-3"></div>
            </div>

            <div>
              {selectedHouseStatus && selectedHouseStatus == "SOLD" && (
                <>
                  {/* <MenuItem sx={{ fontSize: [13, 16] }}>Cancel Record</MenuItem> */}
                  <h2 className="font-bold m"> Deposit/Downpayment </h2>
                  <FormControl>
                    <FormLabel id="demo-radio-buttons-group-label">
                      Select Downpayment Mode
                    </FormLabel>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue="female"
                      name="radio-buttons-group"
                      value={paymentMode}
                      onChange={handlePaymentModeChange}
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <FormControlLabel
                        value="percentage"
                        control={<Radio />}
                        label="Percentage"
                      />
                      <FormControlLabel
                        value="cash"
                        control={<Radio />}
                        label="Cash"
                      />
                    </RadioGroup>
                  </FormControl>

                  <div className="flex flex-row gap-3 mt-">
                    <div className="flex flex-row gap-3 mt-3">
                      <TextField
                        type="text"
                        id="down_payment"
                        name="down_payment"
                        label={
                          paymentMode === "percentage" ? "Percentage" : "Cash"
                        } // Label based on paymentMode state
                        placeholder="Down Payment"
                        value={formikMain.values.down_payment}
                        onChange={(e) => {
                          const inputValue = e.target.value;
                          const numericValue = parseFloat(inputValue);

                          if (paymentMode === "percentage") {
                            // If payment mode is "Percentage", limit input to numbers between 0 and 100
                            if (
                              inputValue === "" ||
                              (!isNaN(numericValue) &&
                                /^[0-9]*$/.test(inputValue) &&
                                numericValue >= 0 &&
                                numericValue <= 100)
                            ) {
                              formikMain.setFieldValue(
                                "down_payment",
                                inputValue
                              );
                            }
                          } else {
                            // If payment mode is "Cash", allow any input
                            formikMain.setFieldValue(
                              "down_payment",
                              inputValue
                            );
                          }
                        }}
                        onBlur={formikMain.handleBlur}
                        // error={
                        //   formikMain.touched.down_payment &&
                        //   Boolean(formikMain.errors.down_payment)
                        // }
                        // helperText={
                        //   formikMain.touched.down_payment &&
                        //   formikMain.errors.down_payment
                        // }
                      />
                    </div>
                  </div>
                </>
              )}

              {selectedHouseStatus && selectedHouseStatus == "BOOKED" && (
                <>
                  <div className="border border-1 p-2 bg-yellow-500 rounded-md">
                    <Typography>
                      Booking fee of KES 100,000 will be added!
                    </Typography>
                  </div>
                </>
              )}

              {selectedHouseStatus && selectedHouseStatus == "INTERESTED" && (
                <>
                  <div className="border border-1 p-2 bg-yellow-500 rounded-md">
                    <Typography>Downpayment will be set to 0</Typography>
                  </div>
                </>
              )}
            </div>

            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginTop: 2 }}
            >
              Agent's Information
            </Typography>
            {/* <div className="flex text-center justify-center mt-4 ">
              <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
                <Search>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    //onClick={handleSearchModal}
                    //onClick={aboutToSearch}
                    placeholder="Type Agent"
                    // placeholder={searchType || "Search Name..."}
                    inputProps={{ "aria-label": "search" }}
                    value={searchValue}
                    onChange={(e) => handleChange(e.target.value)}
                  />
                </Search>
     
              </Box>
            </div> */}

            <div
            // className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]"
            // style={scroll}
            >
              {filteredResults.map((result, id) => {
                // return <SearchResult result={result.name} key={id} />;
                return (
                  <div
                    className="flex flex-row gap-2 hover:bg-blue-400 p-2 cursor-pointer"
                    key={id}
                    onClick={() =>
                      handleSearchResultItem(result.broker_information)
                    }
                  >
                    {result.broker_information.first_name}{" "}
                    {result.broker_information.last_name}
                  </div>
                );
              })}
            </div>

            <CardMedia height="540" />
            <CardContent>
              {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}
              <Typography variant="body2" color="text.secondary">
                {/* FIRST NAME:{agent_information.first_name} */}
                {agent_information && (
                  <p>First Name: {agent_information.agent_firstname}</p>
                )}
                <div>
                  <ul>
                    {/* {house_information.map((house_info, index) => ( */}
                    <li
                      // key={index}
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                        justifyContent: "space-between",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>

                      {clickedAgent ? (
                        clickedAgent !== "No Agents Added" ? (
                          <React.Fragment>
                            {clickedAgent.first_name}
                            ,&nbsp;&nbsp;
                            {clickedAgent.company_name}&nbsp;&nbsp;
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "red", marginLeft: 3 }}
                              size="small"
                              type="submit"
                              onClick={handleRemoveAgent}
                            >
                              Remove
                            </Button>
                          </React.Fragment>
                        ) : (
                          "No Agents Added"
                        )
                      ) : (
                        "No Agents Added!"
                      )}
                    </li>
                    {/* ))} */}
                  </ul>
                </div>
              </Typography>

              <CardActions>
                <div className="flex flex-row gap-6">
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleOpenAgentForm}
                  >
                    + Add Agent
                  </Button>

                  {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
                </div>
              </CardActions>
            </CardContent>

            {/* <Button type="submit">Save Agent</Button> */}
            {/* </form> */}

            <div className="flex m-4 gap-4 ">
              <Button
                variant="contained"
                size="large"
                type="submit"
                sx={{ backgroundColor: "#242333" }}
                // onClick={handle_submit}
              >
                Save Record
              </Button>
              <Button
                onClick={handleClose_booking}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="large"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      {/* <MainNavbar /> */}
      <div className="flex flex-row justify-between m-2">
        <div
          className="flex flex-row cursor-pointer hover:scale-110 duration-200 ease-linear shadow-xl px-4 py-1 rounded-lg"
          onClick={handle_back}
        >
          <ArrowBackIosIcon sx={{ fontSize: ["18px", "22px"], marginTop: 1 }} />
          {/* <Typography sx={{ fontSize: ["13px", "18px"], marginTop:1 }}>Back</Typography> */}
        </div>
        <div className="">
          <Button
            variant="contained"
            sx={{ backgroundColor: "#242333", fontSize: ["10px", "16px"] }}
            onClick={handleOpen_booking}
          >
            NEW RECORD
          </Button>
        </div>
      </div>
      <Typography
        variant="h5"
        noWrap
        component="div"
        sx={{
          // display: { xs: "none", sm: "block" },
          fontSize: ["14px", "22px"],
          fontWeight: "bold",
          cursor: "pointer",
          margin: ["10px", "18px"],
          justifyContent: "center",
        }}
        // onClick={handle_terms}
      >
        Records
      </Typography>
      {/* <div className="flex flex-col gap-6 " style={header}>
        <div className="flex flex-row gap-6 justify-center">
       

          <Card
            sx={{
              width: 445,

              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Booked Units
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <Circle
                    percent={50}
                    strokeWidth={9}
                    strokeColor="#333333"
                    trailWidth={9}
                  />
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6">
                Percentage of Booked Units:
                <Typography
                  gutterBottom
                  variant="h4"
                  component="div"
                >{`${50}%`}</Typography>
              </div>
            </CardActions>
          </Card>
    
          <Card
            sx={{
              width: 445,
              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Data Representation
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <ul>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Sold Units
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {80}/1000
                      </Button>
                    </li>
                    <li
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>
                      Booked Units
                      <Button
                        size="small"
                        // onClick={() => handleRemove("Departments", index)}
                      >
                        {200}/1000
                      </Button>
                    </li>
                  </ul>
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6 ml-6">
                <Button size="small">More Info...</Button>
              </div>
            </CardActions>
          </Card>
        </div>
      </div> */}
      {/* <div className="ml-4"> */}
      {/* <BarChartReacords /> */}
      {/* <Card
          sx={{
            width: 305,

            boxShadow: 6,
          }}
        >
          <CardMedia height="540" />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Booked Units
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <div>
                <Circle
                  percent={50}
                  strokeWidth={9}
                  strokeColor="#333333"
                  trailWidth={9}
                />
              </div>
            </Typography>
          </CardContent>
          <CardActions>
            <div className="flex flex-row gap-6">
              Percentage of Booked Units:
              <Typography
                gutterBottom
                variant="h4"
                component="div"
              >{`${50}%`}</Typography>
            </div>
          </CardActions>
        </Card>
        <Card
          sx={{
            width: 305,

            boxShadow: 6,
          }}
        >
          <CardMedia height="540" />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Booked Units
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <div>
                <Circle
                  percent={30}
                  strokeWidth={9}
                  strokeColor="#333333"
                  trailWidth={9}
                />
              </div>
            </Typography>
          </CardContent>
          <CardActions>
            <div className="flex flex-row gap-6">
              Percentage of Booked Units:
              <Typography
                gutterBottom
                variant="h4"
                component="div"
              >{`${30}%`}</Typography>
            </div>
          </CardActions>
        </Card> */}
      {/* </div> */}
    </>
  );
}
const header = {
  // border: "1px solid",
  borderColor: "#000",
  margin: "100px",
  padding: "50px",
  borderRadius: "8px",
};
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  overflowX: "auto",
  maxWidth: "80vh",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};
