import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";

const columns = [
  { id: "name", label: "Date And Time", minWidth: 170 },
  // { id: "code", label: "Action", minWidth: 100 },
  {
    id: "population",
    label: "Installments",
    minWidth: 100,
    align: "right",
    format: (value) => value.toLocaleString("en-US"),
  },
  // {
  //   id: "size",
  //   label: "Amount",
  //   minWidth: 170,
  //   align: "center",
  //   format: (value) => value.toLocaleString("en-US"),
  // },
  // {
  //   id: "commission",
  //   label: "Commission",
  //   minWidth: 170,
  //   align: "right",
  //   format: (value) => value.toFixed(2),
  // },
  // {
  //   id: "buyer",
  //   label: "Client's Name",
  //   minWidth: 170,
  //   align: "right",
  //   format: (value) => value.toFixed(2),
  // },
];

function createData(name, code, population, size, commission, buyer) {
  const density = population / size;
  return { name, code, population, size, density, commission, buyer };
}

const rows = [
  createData(
    "Sunday, 21 January 2024",
    "Sold",
    "G6, Bloock A",
    11287263,
    200000,
    "Ogola Jesse"
  ),
  createData(
    "Monday, 22 January 2024",
    "Booking",
    " M14 Block C",
    9596961,
    350000,
    "Donald Mark"
  ),
  createData(
    "Tuesday, 16 January 2024",
    "Booking",
    "M2 Blcok C",
    3013400,
    400000,
    "Jane WWF"
  ),
  createData(
    "Sunday, 21 January 2024",
    "Booking",
    "A32 Blcok B",
    9833520,
    100000,
    "Wambua Kamau"
  ),
  createData(
    "Wednesday, 10 January 2024",
    "Sold",
    "G37 Blcok C",
    9984670,
    100000,
    "Sebastian Nelly"
  ),
  createData("Thursday, 11 January 2024", "N/A", "N/A", "N/A", "N/A", "N/A"),
  createData(
    "Friday, 19 January 2024",
    "Booking",
    "G83 Block B",
    3575780,
    300000,
    "Salome Mary"
  ),
  createData(
    "Saturday, 20 January 2024",
    "Sold",
    "G48 Block A",
    7027300,
    10000,
    "Stella Ndunge"
  ),
  createData(
    "Sunday, 21 January 2024",
    "Booking",
    "E12 Block C",
    1972550,
    50000,
    "Jane wammy"
  ),
  createData(
    "Monday, 22 January 2024",
    "Booking",
    "E12 Block B",
    3779730,
    70000,
    "The Ventures"
  ),
  createData(
    "Tuesday, 16 January 2024",
    "Booking",
    "G67 Block B",
    640679,
    50000,
    "Martin"
  ),
  createData(
    "Wednesday, 10 January 2024",
    "Booking",
    "M6 Block A",
    242495,
    50000,
    "Perfilc sjnjf"
  ),
  createData(
    "Thursday, 11 January 2024",
    "Sold",
    "M14 Block A",
    17098246,
    60000,
    "Sebastian Nelly"
  ),
  createData(
    "Friday, 19 January 2024",
    "Sold",
    "M20 Block B",
    923768,
    50000,
    "Sebastian Nelly"
  ),
  createData(
    "Saturday, 20 January 2024",
    "Sold",
    "M21 Block D",
    8515767,
    60000,
    "Sebastian Nelly"
  ),
];

export default function TestTable2() {
  const role = [
    {
      key: "1",
      value: "Admin",
      label: "Admin",
    },
    {
      key: "2",
      value: "Intermediate",
      label: "Intermediate",
    },
    {
      key: "3",
      value: "Basic",
      label: "Basic",
    },
  ];
  const status = [
    {
      value: "Active",
      label: "Active",
    },
    {
      value: "Inactive",
      label: "Inactive",
    },
  ];

  const [openView, setOpenView] = React.useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleCloseViewUser = () => {
    setOpenView(false);
  };
  const handleOPenViewUser = () => {
    setOpenView(true);
  };

  return (
    <>
      <Modal
        open={openView}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              View Sales Details
            </Typography>

            <CloseIcon
              fontSize="40"
              onClick={handleCloseViewUser}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  //   value={formik.values.name}
                  //   onChange={formik.handleChange}
                  helperText="Please enter full name"
                />

                <TextField
                  id="role"
                  select
                  label="Rolez"
                  name="role"
                  //   value={formik.values.role}
                  //   onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                  helperText="Please select role"
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>

                <FormControl>
                  <FormLabel>User Status</FormLabel>

                  <TextField
                    id="status"
                    select
                    label="Status"
                    name="status"
                    // value={formik.values.status}
                    // onChange={formik.handleChange}
                    // onBlur={formik.handleBlur}
                    helperText="Please select status"
                  >
                    {/* {status.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))} */}
                  </TextField>
                </FormControl>
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button
                type="submit"
                variant="contained"
                size="small"
                sx={{ backgroundColor: "#242333" }}
              >
                Save
              </Button>
              <Button
                onClick={handleCloseViewUser}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="small"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 440, minWidth: 1000 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontWeight: "bold",
                      fontSize: "18px",
                      select: "none",
                    }}
                    className="select-none"
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                      sx={{
                        "&:hover": { backgroundColor: "rgb(212 212 212)" },
                        cursor: "pointer",
                        select: "none",
                      }}
                      className="select-none"
                      onDoubleClick={handleOPenViewUser}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
};
