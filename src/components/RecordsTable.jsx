import {
  Box,
  Button,
  CardActions,
  CardContent,
  CardMedia,
  FormControl,
  LinearProgress,
  MenuItem,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import DownloadIcon from "@mui/icons-material/Download";
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import Dropdown from "antd/es/dropdown/dropdown";
import React, { useState } from "react";
import MainNavbar from "./MainNavbar";
import LightTooltip from "./LightTooltip";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
// import { Search } from "@mui/icons-material";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import HolidayVillageOutlinedIcon from "@mui/icons-material/HolidayVillageOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import AnalyticsPage from "./NewRecord";
import NewRecord from "./NewRecord";
import * as Yup from "yup";
import RecordChangesSuccess from "../Alerts/RecordChangesSuccess";
import Spinner1 from "./Spinner1";

const validationSchema_housedetails = Yup.object({
  location: Yup.string().required("location is required"),
  residence: Yup.string().required("Residence is required"),
  house_number: Yup.string().required("House Number is required"),
  asking_price: Yup.string().required("Asking Price is required"),
  selling_price: Yup.string().required("Selling Price is required"),
  house_status: Yup.string().required("House Status is required"),
  house_type: Yup.string().required("House Type is required"),
});

export default function RecordsTable() {
  const [rows, setRows] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [loading, setLoading] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = useState(5);
  const [totalInstallments, setTotalInstallments] = useState("");
  const [balance, setBalance] = useState("");
  const [openMainEditForm, setOpenMainEditForm] = useState(false);
  const navigate = useNavigate();
  const [house_information, setHouse_Information] = React.useState([]);
  const [incoming_instalments, setInstallments] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState();
  const [filteredResults, setResults] = React.useState([]);
  const [agent_information, setAgentInformation] = React.useState(null);
  const [clickedAgent, setclickedAgent] = React.useState(null);
  const [openHouseEditModal, setopenHouseEditModal] = React.useState(false);
  const [openAddHouseInfor, setOpenAddHouseInfor] = React.useState(false);
  const [openAgentForm, setOpenAgentForm] = React.useState(false);
  const [openpopup, setOpenpopUp] = React.useState(false);
  const [openInstallmentModal, setopenInstallmentModal] = React.useState(false);
  const [refresher, setRefresher] = React.useState(false);
  const [global_index, setIndex] = React.useState();
  const [global_id, setGlobalId] = React.useState();

  const [global_date, setGlobalDate] = React.useState();

  //setGlobalId

  //setIndex

  const [openHouseDetailsModal, setOpenHouseDetailsModal] =
    React.useState(false);
  const [openRecordChangeSuccessfully, setOpenRecordChangeSuccessfully] =
    React.useState(false);
  const [openInstallment, setOpenInstallment] = React.useState(false);

  const house_status = [
    
    {
      key: "1",
      value: "BOOKED",
      label: "Booked",
    },

    {
      key: "2",
      value: "INTERESTED",
      label: "Interested",
    },
    {
      key: "3",
      value: "CANCELED",
      label: "Cancelled",
    },

    {
      key: "4",
      value: "SOLD",
      label: "Sold",
    },
  ];

  const house_type = [
    {
      key: "1",
      value: "studio",
      label: "Studio",
    },
    {
      key: "2",
      value: "1bed",
      label: "One bedroom",
    },
    {
      key: "3",
      value: "2bed",
      label: "Two bedroom",
    },
    {
      key: "4",
      value: "3bed",
      label: "Three bedroom",
    },
    {
      key: "5",
      value: "4bed",
      label: "4 bedroom",
    },
  ];

  const handleCloseHouseDetailsModal = () => {
    setOpenHouseDetailsModal(false);
  };
  const handleOpen_AddHouseInfor = () => {
    setOpenAddHouseInfor(true);
  };

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
  }));

  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const items = [
    {
      key: "1",
      label: "Edit Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => handleOpenMainEditform(),
    },
    {
      key: "2",
      label: "Add Installment",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => handle_openInstallments(),
    },

    {
      key: "3",
      label: "Delete Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => deleteRecord(),
    },
  ];

  const formikHouseDetails = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
    },

    validationSchema: validationSchema_housedetails,
    onSubmit: (values) => {
      console.log("values", values);
      const house_array = [];

      var matchFound = false;

      // Check each object in house_array

      console.log("match found", matchFound);
      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      console.log("updated object", updatedObject);
      house_array.push(updatedObject);

      const hasDuplicate = house_information.some(
        (house) => house.house_number === values.house_number
      );

      if (!hasDuplicate) {
        setHouse_Information((prevHouseInformation) => {
          return [...prevHouseInformation, ...house_array];
        });
        setOpenHouseAddedSuccessfully(true);
      } else if (hasDuplicate) {
        alert("HAS DUPLICATE CANNOT ADD!!");
      }

      // console.log("house array:", house_array);

      //const isDuplicate = house_array.some(values => house_information.some(info => /* Your comparison logic here */ ));
      console.log("house information", house_information);

      console.log("house array:", house_array);

      console.log("house information array", house_information);

      //const [house_information, setHouse_Information] = React.useState([]);

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      //}
      setOpenRecordChangeSuccessfully(true);
      setOpenHouseDetailsModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditAgentDetails = useFormik({
    // {information_to_be_Edited !== undefined && (
    //   <p>{information_to_be_Edited.asking_price}</p>
    // )}
    initialValues: {
      first_name: "",
      last_name: "",
      phone_number: "",
      // agent_email: "",
      // agent_nat_id: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("Agent Details", values);

      //setAgentInformation(values);
      setclickedAgent(values);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/

      setOpenAgentForm(false);
      setOpenpopUp(false);
      setOpenAgentAddedSuccessfully(true);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  // const handleRowDoubleClick = (er) => {
  //   navigate("/details");
  // };
  const handleRowDoubleClick = (er) => {
    navigate("/installments");
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleClose_booking = () => {
    setOpenBooking(false);
  };

  function handleRemoveAgent() {
    setclickedAgent("");
  }

  function handleOpenModal() {
    console.log("opening modal");
    setOpenHouseDetailsModal(true);
  }

  function handlecloseAgentForm() {
    setOpenAgentForm(false);
  }

  function handleOpenAgentForm(option) {
    // console.log("option selected", option);
    // if (option === "AGENT") {
    //   setOpenAgentForm(true);
    // }
    setOpenAgentForm(true);
  }

  function handleEditInstallmentDetail(installment, idx) {
    console.log("Handle edit installments");
    formikEditInstallmentsDetails.setValues({
      amount: installment.amount,

      installment_idx: idx,
    });

    //   setopenHouseEditModal(true);
    setopenInstallmentModal(true);
  }
  const handle_openInstallments = () => {
    // formikAddInstallment.setValues({
    //   client_firstname: selectedRowData.client_information.first_name,
    //   client_lastname: selectedRowData.client_information.last_name,
    //   client_phonenumber: selectedRowData.client_information.phone_number,
    //   client_email: selectedRowData.client_information.email,
    //   client_nat_id: selectedRowData.client_information.nat_id,
    //   installments: selectedRowData.down_payment,
    //   created_at: selectedRowData.created_at,
    // });

    setHouse_Information(selectedRowData.house_information);
    setclickedAgent(selectedRowData.broker_information);
    setInstallments(selectedRowData.installments);
    setOpenInstallment(true);
  };
  const handleclose_installment = () => {
    setOpenInstallment(false);
  };

  const handleOpenMainEditform = () => {
    console.log("edit form opened");

    console.log("selected row data", selectedRowData);

    setGlobalDate(selectedRowData.date);

    formikEditMain.setValues({
      client_firstname: selectedRowData.client_information.first_name,
      client_lastname: selectedRowData.client_information.last_name,
      client_phonenumber: selectedRowData.client_information.phone_number,
      client_email: selectedRowData.client_information.email,
      client_nat_id: selectedRowData.client_information.nat_id,
      installments: selectedRowData.down_payment,
      created_at: selectedRowData.created_at,
    });

    setHouse_Information(selectedRowData.house_information);
    setclickedAgent(selectedRowData.broker_information);
    setInstallments(selectedRowData.installments);
    setOpenMainEditForm(true);
  };

  const handleCloseMainEditform = () => {
    console.log("edit form opened");
    setOpenMainEditForm(false);
  };

  function closeHouseEditModal() {
    setopenHouseEditModal(false);
  }

  function closeInstallmentModal() {
    //    setopenHouseEditModal(false);
    setopenInstallmentModal(false);
  }

  function handleEditHouseDetail(house, idx) {
    console.log("edit house detail called", house);

    formikEditHouseDetails.setValues({
      location: house.location,
      residence: house.residence,
      house_number: house.house_number,
      asking_price: house.asking_price,
      selling_price: house.selling_price,
      house_status: house.house_status,
      house_type: house.house_type,
      house_idx: idx,
    });

    setopenHouseEditModal(true);
  }

  function handleDeleteHouseDetail(index) {
    console.log("delete house detail called");

    let updatedHouseArray = [];
    updatedHouseArray = [...house_information];
    updatedHouseArray.splice(index, 1);
    setHouse_Information(updatedHouseArray);
  }

  function handleDeleteInstallmentDetail(index) {
    console.log("delete installment detail called");

    let updatedHouseArray = [];
    updatedHouseArray = [...incoming_instalments];
    updatedHouseArray.splice(index, 1);
    setInstallments(updatedHouseArray);
  }

  function handleGetAllRecords() {
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    console.log("ID", storedData);
    setGlobalId(storedData._id);
    setLoading(true);
    console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        console.log("after-sorting-records:->", activeRecords);

        // activeRecords.sort(
        //   (a, b) => new Date(b.created_at) - new Date(a.created_at)
        // );

        setRows(activeRecords);

        // Assuming activeRecords is an array of objects
        // activeRecords.sort((a, b) => {
        //   // Convert created_at strings to Date objects for comparison
        //   const dateA = new Date(a.created_at);
        //   const dateB = new Date(b.created_at);

        //   // Compare the Date objects
        //   return dateA - dateB;
        // });

        //console.log("rec logs", json);

        activeRecords.forEach((element, index) => {
          // console.log("element--", element);

          const totalInstallmentsPaid = element.installments.reduce(
            (total, amount) => {
              return total + parseFloat(amount.amount);
            },
            0
          );

          const totalSellingPrice = element.house_information.reduce(
            (total, sp) => {
              return total + parseFloat(sp.selling_price);
            },
            0
          );

          setBalance(totalSellingPrice - totalInstallmentsPaid);

          setTotalInstallments(totalInstallmentsPaid);
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const updateRecord = async (updatedRecord) => {
    console.log("updated Newly updatedRecord:", updatedRecord);

    console.log("respective index", global_index, selectedRowData._id);
    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/edit-record/${global_id}/${selectedRowData._id}`,

        // `https://rems-backend.onrender.com/edit-record/${global_id}/${global_index}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedRecord),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const deleteRecord = async (updatedRecord) => {
    const delete_property = {
      status: "INACTIVE",
    };

    // let updatedObject = {
    //   ...selectedRowData, // Spread existing properties
    //   ...delete_property, // Spread updated values
    // };

    // console.log("updated Object->:", delete_property);
    console.log("selectedRowData", selectedRowData);

    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/deactivate/${global_id}/${selectedRowData._id}`,

        //`https://rems-backend.onrender.com/delete-record/${selectedRowData._id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        // setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }

 
  };

  const postInstallment = async (incoming_instalments, installment) => {
    //let installment_array = [];
    //installment_array.push(installmentRecord);

    // const updatedInstallments = {
    //   installments: incoming_instalments,
    // };

    console.log("new incoming installments", installment);

    // let updatedObject = {
    //   ...selectedRowData, // Spread existing properties
    //   ...updatedInstallments, // Spread updated values
    // };

    // console.log("new updated object", updatedObject);

    // console.log("updated nurse info:", updatedRecord);
    console.log("Document ID to be updated:-->", global_id, global_index);
    try {
      const response = await fetch(
        `https://rems-backend.onrender.com/post-installment/${global_id}/${selectedRowData._id}`,

        //`https://rems-backend.onrender.com/delete-record/${selectedRowData._id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(installment),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        // setOpenRecordChangeSuccessfully(true);
        setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const formikAddInstallment = useFormik({
    initialValues: {
      amount: "",

      // client_email: "",
      // client_nat_id: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("NEW RECORD", values);

      console.log("incoming installments", incoming_instalments);

      // console.log("new agent information", clickedAgent);
      // console.log("new house information", house_information);
      // console.log("new installment information", incoming_instalments);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      //   installments: [
      //     {
      //       amount: "",
      //       installment_type: "",
      //       created_at: "",
      //     },
      //   ],

      const corrected_installment = values.amount.replace(/,/g, "");

      const installment_record = {
        amount: corrected_installment,
        installment_type: "quarterly",
        created_at: formattedDate,
      };

      console.log("installment RECORD", installment_record);

      //setInstallments(selectedRowData.installments);

      let installment_array = [];
      installment_array = incoming_instalments;

      installment_array.push(installment_record);

      console.error("new installment array", installment_array);

      postInstallment(installment_array, installment_record);
      setOpenInstallment(false);
      setOpenMainEditForm(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditMain = useFormik({
    initialValues: {
      client_firstname: "",
      client_lastname: "",
      client_phonenumber: "",
      client_email: "",
      client_nat_id: "",
      down_payment: "",

      // client_email: "",
      // client_nat_id: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("NEW RECORD", values);

      console.log("new agent information", clickedAgent);
      console.log("new house information", house_information);
      console.log("new installment information", incoming_instalments);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      //   installments: [
      //     {
      //       amount: "",
      //       installment_type: "",
      //       created_at: "",
      //     },
      //   ],

      const record = {
        agent_name: "Nancy",
        // created_at: formattedDate,
        status: selectedRowData.status,
        created_at: selectedRowData.created_at,
        client_information: {
          first_name: values.client_firstname,
          last_name: values.client_lastname,
          phone_number: values.client_phonenumber,
          nat_id: values.client_nat_id,
          email: values.client_email,
          created_at: selectedRowData.created_at,
        },
        house_information: house_information,

        broker_information: clickedAgent,
        installments: incoming_instalments,
        date: global_date,
      };

      console.log("EDITED RECORD", record);

      updateRecord(record);

      //postData(record);

      //}

      setOpenMainEditForm(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditHouseDetails = useFormik({
    initialValues: {
      location: "",
      residence: "",
      house_number: "",
      selling_price: "",
      asking_price: "",
      house_status: "",
      house_type: "",
      house_idx: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("edit values to be submitted", values);

      const corrected_asking_price = values.asking_price.replace(/,/g, "");
      const corrected_selling_price = values.selling_price.replace(/,/g, "");

      console.log("corrected asking price:->", corrected_asking_price);

      const corrected_prices = {
        asking_price: corrected_asking_price,
        selling_price: corrected_selling_price,
      };

      let updatedObject = {
        ...values, // Spread existing properties
        ...corrected_prices, // Spread updated values
      };

      setHouse_Information((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        newArray[values.house_idx] = updatedObject; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      // VALIDATION

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      setopenHouseEditModal(false);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  const formikEditInstallmentsDetails = useFormik({
    initialValues: {
      amount: "",

      installment_idx: "",
    },

    // validationSchema: validationSchema,
    onSubmit: (values) => {
      //let house_array_to_be_edited = [];
      //const updatedHouseArray = [];
      console.log("edit installment values to be submitted", values);

      const corrected_amount = values.amount.replace(/,/g, "");
      // const corrected_selling_price = values.selling_price.replace(/,/g, "");
      const updatedObject = {
        installment_idx: values.installment_idx,
        amount: corrected_amount,
      };

      setInstallments((prevState) => {
        const newArray = [...prevState]; // Create a copy of the original array
        newArray[values.installment_idx] = updatedObject; // Replace the object at the specified index
        return newArray; // Return the new array to update the state
      });

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);
      //*******************************WILL BE NEEDED*************************************/
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllRecords();
  }, [refresher]); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <MainNavbar />
      {loading && <Spinner1 />}
      <NewRecord />
      <RecordChangesSuccess
        open={openRecordChangeSuccessfully}
        autoHideDuration={3000}
        onClose={() => setOpenRecordChangeSuccessfully(false)}
      />

      <Modal open={openInstallment}>
        <Box sx={style_box}>
          <Typography
            id="modal-modal-title"
            sx={{ fontWeight: "bold", marginBottom: 2 }}
          >
            Add New Installment
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <div>
              <form onSubmit={formikAddInstallment.handleSubmit}>
                {/* <TextField
              type="text"
              id="house_idx"
              name="house_idx"
              label="Index [read-only]"
              placeholder="House_index"
              value={formikEditHouseDetails.values.house_idx}
              InputProps={{
                readOnly: true,
              }}
              onChange={formikEditHouseDetails.handleChange}
              onBlur={formikEditHouseDetails.handleBlur}
            /> */}
                <div className="flex flex-row gap-3 mt-3">
                  <TextField
                    type="text"
                    id="amount"
                    name="amount"
                    label="Amount"
                    placeholder="Amount"
                    value={formikAddInstallment.values.amount}
                    onChange={formikAddInstallment.handleChange}
                    onBlur={formikAddInstallment.handleBlur}
                  />
                </div>
                <div className="flex flex-row gap-3 mt-3">
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleclose_installment}
                  >
                    close
                  </Button>
                </div>
              </form>
            </div>
          </Typography>
        </Box>
      </Modal>
      <Modal
        open={openMainEditForm}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Edit Record
            </Typography>
            {/* <h2 className="font-bold m-4"> Add Record </h2> */}
            <CloseIcon
              fontSize="40"
              onClick={handleCloseMainEditform}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form onSubmit={formikEditMain.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
                  Edit Client's Details
                </Typography>
                <div className="flex flex-col gap-3">
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="client_firstname"
                      name="client_firstname"
                      label="First Name"
                      placeholder="First Name"
                      value={formikEditMain.values.client_firstname}
                      // onChange={formikMain.handleChange}

                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikEditMain.handleChange(e);
                      }}
                      onBlur={formikEditMain.handleBlur}
                    />

                    <TextField
                      type="text"
                      id="client_lastname"
                      name="client_lastname"
                      label="Last Name"
                      placeholder="Last Name"
                      value={formikEditMain.values.client_lastname}
                      //onChange={formikMain.handleChange}
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikEditMain.handleChange(e);
                      }}
                      onBlur={formikEditMain.handleBlur}
                    />

                    <TextField
                      type="text"
                      id="client_phonenumber"
                      name="client_phonenumber"
                      label="Phone Number"
                      placeholder="Phone Number"
                      value={formikEditMain.values.client_phonenumber}
                      //onChange={formikMain.handleChange}
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikEditMain.handleBlur}
                    />
                  </div>
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="client_nat_id"
                      name="client_nat_id"
                      label="ID Number"
                      placeholder="ID Number"
                      value={formikEditMain.values.client_nat_id}
                      //onChange={formikMain.handleChange}
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikEditMain.handleChange(e);
                      }}
                      onBlur={formikEditMain.handleBlur}
                    />
                    <TextField
                      type="text"
                      id="client_email"
                      name="client_email"
                      label="email"
                      placeholder="Client's Email"
                      value={formikEditMain.values.client_email}
                      onChange={formikEditMain.handleChange}
                      // onChange={(e) => {
                      //   e.target.value = e.target.value.toUpperCase();
                      //   formikMain.handleChange(e);
                      // }}
                      onBlur={formikEditMain.handleBlur}
                    />
                  </div>
                </div>

                <CardMedia height="540" />
                <CardContent>
                  <Typography
                    id="modal-modal-title"
                    sx={{ fontWeight: "bold", marginBottom: 2 }}
                  >
                    Edit House Details
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    <div>
                      {house_information.length > 0 ? (
                        <ul>
                          {house_information.map((house_info, index) => (
                            <li
                              key={index}
                              style={{
                                border: "1px solid #ccc",
                                padding: "8px",
                                // marginBottom: "8px",
                              }}
                            >
                              <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span>
                              <span className="">Loc:&nbsp;</span>
                              {house_info.location}, &nbsp;{" "}
                              <span className="">Res:&nbsp;</span>
                              {house_info.residence},&nbsp;{" "}
                              <span className="">Type:&nbsp;</span>
                              {house_info.house_type},&nbsp;{" "}
                              <span className="">Sp: Kshs&nbsp;</span>
                              {house_info.selling_price},&nbsp;{" "}
                              <span className="">HsNo:&nbsp;</span>
                              {house_info.house_number},{" "}
                              <LightTooltip title="Edit house ">
                                <EditIcon
                                  sx={{
                                    color: "#000",
                                    cursor: "pointer",
                                    fontSize: "20px",
                                  }}
                                  onClick={() =>
                                    handleEditHouseDetail(house_info, index)
                                  }
                                />
                              </LightTooltip>
                              <LightTooltip title="Delete house ">
                                <DeleteOutlineOutlinedIcon
                                  sx={{
                                    color: "#000",
                                    cursor: "pointer",
                                    fontSize: "20px",
                                  }}
                                  onClick={() =>
                                    handleDeleteHouseDetail("Wards", index)
                                  }
                                />
                              </LightTooltip>
                              {/* <Button
                                variant="contained"
                                sx={{ backgroundColor: "#242333" }}
                                size="small"
                                onClick={() =>
                                  handleEditHouseDetail(house_info, index)
                                }
                              >
                                Edit
                              </Button> */}
                              {/* <Button
                                variant="contained"
                                sx={{ backgroundColor: "#242333" }}
                                size="small"
                                onClick={() => handleDeleteHouseDetail(index)}
                              >
                                Delete
                              </Button>  */}
                            </li>
                          ))}
                        </ul>
                      ) : (
                        <div>
                          <ul>
                            {/* {house_information.map((house_info, index) => ( */}
                            <li
                              // key={index}
                              style={{
                                border: "1px solid #ccc",
                                padding: "8px",
                                marginBottom: "8px",
                              }}
                            >
                              <span style={{ marginRight: "8px" }}>
                                &#8226;
                              </span>
                              No Houses Added!
                            </li>
                            {/* ))} */}
                          </ul>
                        </div>
                      )}
                    </div>
                  </Typography>

                  {/* <CardActions> */}
                  {/* <div className="flex flex-row gap-6"> */}
                  {/* <Button
                        variant="contained"
                        sx={{ backgroundColor: "#242333" }}
                        size="small"
                        onClick={handleOpenModal}
                      >
                        + Add Houses
                      </Button> */}
                  {/* </div> */}
                  {/* </CardActions> */}
                </CardContent>
              </div>
            </div>

            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginTop: 2 }}
            >
              Agent's Information
            </Typography>
            <div className="flex text-center justify-center mt-4 ">
              <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
                <Search>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    //onClick={handleSearchModal}
                    //onClick={aboutToSearch}
                    placeholder="Type Agent"
                    // placeholder={searchType || "Search Name..."}
                    inputProps={{ "aria-label": "search" }}
                    value={searchValue}
                    onChange={(e) => handleChange(e.target.value)}
                  />
                </Search>
                {/* 
            <Button variant="contained" onClick={handleSearchModal}>
              Search
            </Button> */}
              </Box>
            </div>

            <div
              className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]"
              // style={scroll}
            >
              {filteredResults.map((result, id) => {
                // return <SearchResult result={result.name} key={id} />;
                return (
                  <div
                    className="flex flex-row gap-2 hover:bg-blue-400 p-2 cursor-pointer"
                    key={id}
                    onClick={() =>
                      handleSearchResultItem(result.broker_information)
                    }
                  >
                    {result.broker_information.first_name}{" "}
                    {result.broker_information.last_name}
                  </div>
                );
              })}
            </div>

            <CardMedia height="540" />
            <CardContent>
              {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}
              <Typography variant="body2" color="text.secondary">
                {/* FIRST NAME:{agent_information.first_name} */}
                {agent_information && (
                  <p>First Name: {agent_information.agent_firstname}</p>
                )}
                <div>
                  <ul>
                    {/* {house_information.map((house_info, index) => ( */}
                    <li
                      // key={index}
                      style={{
                        border: "1px solid #ccc",
                        padding: "8px",
                        marginBottom: "8px",
                      }}
                    >
                      <span style={{ marginRight: "8px" }}>&#8226;</span>

                      {clickedAgent ? (
                        clickedAgent !== "No Agents Added" ? (
                          <React.Fragment>
                            {clickedAgent.first_name} {clickedAgent.last_name}
                            ,&nbsp;&nbsp;
                            {clickedAgent.phone_number}&nbsp;&nbsp;
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              type="submit"
                              onClick={handleRemoveAgent}
                            >
                              Remove
                            </Button>
                          </React.Fragment>
                        ) : (
                          "No Agents Added"
                        )
                      ) : (
                        "No Agents Added!"
                      )}
                    </li>
                    {/* ))} */}
                  </ul>
                </div>
              </Typography>

              <CardActions>
                <div className="flex flex-row gap-6">
                  {/* <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleOpenAgentForm}
                  >
                    + Add Agent
                  </Button> */}

                  {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
                </div>
              </CardActions>
            </CardContent>

            <h2 className="font-bold m"> Edit Installments </h2>

            <CardMedia height="540" />
            <CardContent>
              {/* <Typography gutterBottom variant="h5" component="div">
                    Wards
                  </Typography> */}
              <Typography variant="body2" color="text.secondary">
                <div>
                  {incoming_instalments.length > 0 ? (
                    <ul>
                      {incoming_instalments.map((installment, index) => (
                        <li
                          key={index}
                          style={{
                            border: "1px solid #ccc",
                            padding: "8px",
                            marginBottom: "8px",
                          }}
                          className="flex flex-row space-x-3"
                        >
                          <div>
                            <span style={{ marginRight: "8px" }}>&#8226;</span>
                            Installment {index + 1}: &nbsp;{" "}
                            {parseFloat(installment.amount).toLocaleString()}
                            &nbsp; &nbsp;
                          </div>

                          <div className="flex flex-row space-x-2">
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={() =>
                                handleEditInstallmentDetail(installment, index)
                              }
                            >
                              Edit
                            </Button>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={() =>
                                handleDeleteInstallmentDetail(index)
                              }
                            >
                              Delete
                            </Button>
                          </div>
                        </li>
                      ))}
                    </ul>
                  ) : (
                    <div>
                      <ul>
                        {/* {house_information.map((house_info, index) => ( */}
                        <li
                          // key={index}
                          style={{
                            border: "1px solid #ccc",
                            padding: "8px",
                            marginBottom: "8px",
                          }}
                        >
                          <span style={{ marginRight: "8px" }}>&#8226;</span>
                          No Installments Added!
                        </li>
                        {/* ))} */}
                      </ul>
                    </div>
                  )}
                </div>
              </Typography>

              <CardActions>
                <div className="flex flex-row gap-6">
                  {/* <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    onClick={handleOpenAgentForm}
                  >
                    + Add New Installment
                  </Button> */}

                  {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
                </div>
              </CardActions>
            </CardContent>

            {/* <form onSubmit={formikEditAgentDetails.handleSubmit}> */}

            {/* <Button type="submit">Save Agent</Button> */}
            {/* </form> */}

            <div className="flex m-4 gap-4 ">
              <Button
                variant="contained"
                size="large"
                type="submit"
                sx={{ backgroundColor: "#242333" }}
                // onClick={handle_submit}
              >
                Save Record
              </Button>
              <Button
                onClick={handleCloseMainEditform}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="large"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openHouseDetailsModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4"> Add House Information </h2>
            <CloseIcon
              fontSize="40"
              onClick={handleCloseHouseDetailsModal}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form onSubmit={formikHouseDetails.handleSubmit}>
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="location"
                name="location"
                label="Location"
                placeholder="First Name"
                value={formikHouseDetails.values.location}
                //onChange={formikHouseDetails.handleChange}

                onChange={(e) => {
                  e.target.value = e.target.value.toUpperCase();
                  formikHouseDetails.handleChange(e);
                }}
                onBlur={formikHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="residence"
                name="residence"
                label="House Residence"
                placeholder="Last Name"
                value={formikHouseDetails.values.residence}
                //onChange={formikHouseDetails.handleChange}
                onChange={(e) => {
                  e.target.value = e.target.value.toUpperCase();
                  formikHouseDetails.handleChange(e);
                }}
                onBlur={formikHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="house_number"
                name="house_number"
                label="house unit"
                placeholder="House Unit"
                value={formikHouseDetails.values.house_number}
                //onChange={formikHouseDetails.handleChange}

                onChange={(e) => {
                  e.target.value = e.target.value.toUpperCase();
                  formikHouseDetails.handleChange(e);
                }}
                onBlur={formikHouseDetails.handleBlur}
              />
            </div>
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="asking_price"
                name="asking_price"
                label="Asking Price"
                placeholder="Asking Price"
                value={formikHouseDetails.values.asking_price}
                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikHouseDetails.setFieldValue(
                    "asking_price",
                    formattedValue
                  );
                }}
                onBlur={formikHouseDetails.handleBlur}
              />
              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Selling Price"
                placeholder="Selling Price"
                value={formikHouseDetails.values.selling_price}
                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikHouseDetails.setFieldValue(
                    "selling_price",
                    formattedValue // Corrected to set "selling_price" instead of "asking_price"
                  );
                }}
                onBlur={formikHouseDetails.handleBlur}
              />
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <FormControl>
                <TextField
                  id="house_status"
                  select
                  name="house_status"
                  label="Status"
                  placeholder="Hus Price"
                  fullWidth
                  // value={formikHouseDetails.values.house_status}
                  onChange={formikHouseDetails.handleChange}
                  onBlur={formikHouseDetails.handleBlur}
                >
                  {house_status.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <FormControl>
                <TextField
                  id="house_type"
                  select
                  name="house_type"
                  label="House type"
                  placeholder="house_type"
                  fullWidth
                  // value={formikHouseDetails.values.house_status}
                  onChange={formikHouseDetails.handleChange}
                  onBlur={formikHouseDetails.handleBlur}
                >
                  {house_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  // type="submit"
                  onClick={handleOpen_AddHouseInfor}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <Modal
        open={openHouseEditModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
            <h2 className="font-bold m-4"> Edit House Information </h2>
            <CloseIcon
              fontSize="40"
              onClick={closeHouseEditModal}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form onSubmit={formikEditHouseDetails.handleSubmit}>
            {/* <TextField
              type="text"
              id="house_idx"
              name="house_idx"
              label="Index [read-only]"
              placeholder="House_index"
              value={formikEditHouseDetails.values.house_idx}
              InputProps={{
                readOnly: true,
              }}
              onChange={formikEditHouseDetails.handleChange}
              onBlur={formikEditHouseDetails.handleBlur}
            /> */}
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="location"
                name="location"
                label="Location"
                placeholder="First Name"
                value={formikEditHouseDetails.values.location}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="residence"
                name="residence"
                label="House Residence"
                placeholder="Last Name"
                value={formikEditHouseDetails.values.residence}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="house_number"
                name="house_number"
                label="house unit"
                placeholder="House Unit"
                value={formikEditHouseDetails.values.house_number}
                onChange={formikEditHouseDetails.handleChange}
                onBlur={formikEditHouseDetails.handleBlur}
              />
            </div>
            <div className="flex flex-row gap-3 mt-3">
              <TextField
                type="text"
                id="asking_price"
                name="asking_price"
                label="Asking Price"
                placeholder="Asking Price"
                value={formikEditHouseDetails.values.asking_price}
                //onChange={formikEditHouseDetails.handleChange}'

                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikEditHouseDetails.setFieldValue(
                    "asking_price",
                    formattedValue
                  );
                }}
                onBlur={formikEditHouseDetails.handleBlur}
              />

              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Selling Price"
                placeholder="Selling Price"
                value={formikEditHouseDetails.values.selling_price}
                // onChange={formikEditHouseDetails.handleChange}

                onChange={(e) => {
                  const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                  const formattedValue = rawValue
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                  formikEditHouseDetails.setFieldValue(
                    "selling_price",
                    formattedValue // Corrected to set "selling_price" instead of "asking_price"
                  );
                }}
                onBlur={formikEditHouseDetails.handleBlur}
              />
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <FormControl>
                <TextField
                  id="house_status"
                  select
                  name="house_status"
                  label="Status"
                  placeholder="House status"
                  fullWidth
                  value={formikEditHouseDetails.values.house_status}
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                >
                  {house_status.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <FormControl>
                <TextField
                  id="house_type"
                  select
                  name="house_type"
                  label="House type"
                  placeholder="house_type"
                  fullWidth
                  value={formikEditHouseDetails.values.house_type}
                  onChange={formikEditHouseDetails.handleChange}
                  onBlur={formikEditHouseDetails.handleBlur}
                >
                  {house_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <div className="flex flex-row gap-3 mt-3"></div>

            <Button
              onClick={handleClose_booking}
              variant="contained"
              sx={{ backgroundColor: "#242333" }}
              size="small"
              type="submit"
            >
              Save
            </Button>
          </form>
        </Box>
      </Modal>

      {/* ******************************************************************** */}

      <div className="absolute right-3">
        <Button>
          Download Excel
          <DownloadIcon />
        </Button>
      </div>
      <Paper sx={{ width: "100%", overflow: "hidden", padding: 3 }}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow className="select-none">
                <TableCell
                  sx={{
                    fontWeight: 800,
                    fontSize: ["14px", "18px"],
                    width: "200px",
                  }}
                >
                  Clients
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Date
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House Unit
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Location
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Status{" "}
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Selling Price{" "}
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Amount Paid{" "}
                </TableCell>
                <TableCell
                  align="right"
                  sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}
                >
                  Balance{" "}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => (
                <Dropdown
                  key={index}
                  menu={{ items }}
                  trigger={["contextMenu"]}
                  placement="bottomLeft"
                >
                  <TableRow
                    key={index}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    className="hover:bg-gray-300 select-none"
                    //onDoubleClick={handleRowDoubleClick}
                    onDoubleClick={() => handleRowDoubleClick(row)}
                    onMouseEnter={() => {
                      setSelectedRowData(row);
                      setIndex(index);
                    }}
                  >
                    {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                    <TableCell
                      className="flex flex-row w-10"
                      sx={{
                        fontWeight: 800,
                        fontSize: ["5px", "18px"],
                        width: "200px",
                      }}
                    >
                      {row.client_information.first_name} &nbsp;
                      {row.client_information.last_name}
                    </TableCell>

                    <TableCell>
                      {row.date
                        ? new Date(row.date).toLocaleString("en-GB", {
                            weekday: "short",
                            day: "numeric",
                            month: "short",
                            year: "numeric",
                            // hour: "numeric",
                            // minute: "numeric",
                            // second: "numeric",
                          })
                        : ""}
                    </TableCell>
                    {/* <TableCell >{row.house_information}</TableCell> */}

                    {/* house unit column */}
                    <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index} className="flex flex-row">
                          <HolidayVillageOutlinedIcon fontSize="20px" />
                          {house.house_number},{house.house_type}{" "}
                        </div>
                      ))}
                    </TableCell>

                    {/* Location column */}
                    <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {house.location}
                        </div>
                      ))}
                    </TableCell>

                    {/* Status column */}
                    <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index}>{house.house_status} </div>
                      ))}
                   
                    </TableCell>

                    {/* Selling Price Column */}
                    <TableCell>
                      {/* Map through each selling price and format with commas */}
                      {row.house_information.map((house, index) => (
                        <div key={index}>
                          {parseFloat(house.selling_price).toLocaleString()}
                        </div>
                      ))}
                    </TableCell>

                    {/* Amount Paid Column */}
                    <TableCell>
                      {/* Calculate the total installments */}
                      {row.installments
                        .reduce(
                          (total, installment) =>
                            total + parseFloat(installment.amount),
                          0
                        )
                        .toLocaleString()}
                    </TableCell>

                    {/* Balance Column */}
                    <TableCell align="right">
                      {/* Selling Price */}
                      {/* {row.house_information.map((house, index) => (
                        <div key={index}>{house.selling_price}</div>
                      ))} */}

                      {/* Calculate and render the balance */}
                      {/* {row.house_information.map((house, index) => (
                        <div key={index}>
                          {parseFloat(house.selling_price).toLocaleString()}
                        </div>
                      ))} */}

                      {/* Calculate and render the balance with commas */}
                      {(
                        row.house_information.reduce(
                          (total, house) =>
                            total + parseFloat(house.selling_price),
                          0
                        ) -
                        row.installments.reduce(
                          (total, installment) =>
                            total + parseFloat(installment.amount),
                          0
                        )
                      ).toLocaleString()}
                    </TableCell>
                  </TableRow>
                </Dropdown>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          className="select-none"
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}

const header = {
  //   border: "1px solid",
  borderColor: "#000",
  margin: "100px",
  padding: "50px",
  borderRadius: "8px",
};
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};
