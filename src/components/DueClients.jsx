import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import BorderLinearProgress from "./BorderLinearProgress";
import { TablePagination, Typography } from "@mui/material";

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}


function formatDate(dateString) {
  // Create a new Date object by parsing the date string
  const date = new Date(dateString);

  // Extract day, month, and year components
  const day = date.getDate();
  const month = date.getMonth() + 1; // Months are zero-based, so we add 1
  const year = date.getFullYear();

  // Format the date as "dd/mm/yyyy"
  return `${day}/${month}/${year}`;
}

export default function DueClients() {
  const [refresher, setRefresher] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(7);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  function handleGetAllRecords() {
    setLoading(true);
    console.log("handle get all records called!");
    //fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        json.records.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        // setRows(json);
       // console.log("DUE CLIENTS", json);

        const total_clients = json.records.length;
       // console.log("total clients", total_clients);

        const due_clients_array = [];

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
          //  console.log("ELEMENT", element);

            //const date = new Date(element.date);
            const lastInstallment =
              element.installments[element.installments.length - 1];

            const incoming_date = new Date(lastInstallment.created_at);

            incoming_date.setDate(incoming_date.getDate() + 90);

            //  console.log("prev date", element.date, " new date", date);

            const currentDate = new Date();
            const daysRemaining = Math.ceil(
              (incoming_date - currentDate) / (1000 * 60 * 60 * 24)
            );

           // console.log("Days remaining until due date:", daysRemaining);

            const due_client_obj = {
              first_name: element.client_information.first_name,

              last_name: element.client_information.last_name,

              prev_date: element.date,

              due_date: formatDate(new Date(incoming_date)),
              days_remaining: daysRemaining,
              house: element.house_information,
              installment: element.installments,
            };

            //  console.log("due date", due_client_obj);

            due_clients_array.push(due_client_obj);
          }
        });
        ///<******************************************END OF MAIN JSON LOOP***********************************************************>

        //  console.log("due clients", due_clients_array);

        due_clients_array.sort((a, b) => a.days_remaining - b.days_remaining);

        ///console.log("sorted array", due_clients_array);

        //console.log("sorted array", due_clients_array);
        setRows(due_clients_array);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    handleGetAllRecords();
  }, [refresher]); // The empty dependency array ensures the effect runs only once when the component mounts
  return (
    <>
      <Paper
        sx={{
          width: "50%",
          overflow: "hidden",
          display: { xs: "none", sm: "block" },
          margin: 2,
        }}
      >
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow className="bg-gray-400">
                <TableCell>NAME</TableCell>
                <TableCell align="center">Date Due</TableCell>
                <TableCell align="right"> Days Remaining</TableCell>
                <TableCell align="right">Payment Progress</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => (
                  <TableRow
                    key={row.name}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.first_name} &nbsp; {row.last_name}
                    </TableCell>
                    <TableCell align="center">
                      {row.due_date}
                      {/* <BorderLinearProgress /> */}
                    </TableCell>
                    {/* <TableCell align="right">{row.due_date}</TableCell> */}
                    <TableCell align="center">{row.days_remaining}</TableCell>
                    <TableCell>
                      {" "}
                      <BorderLinearProgress
                        house_info={row.house}
                        installment_info={row.installment}
                      />{" "}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 7]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
