import React from "react";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";
import { forEach } from "lodash";
import { useState } from "react";

export default function BorderLinearProgress({ house_info, installment_info }) {
  const [paymentProgressPercentage, setPercentage] = useState();
  //console.error("installment props", installment_info)

  let totalSellingPrice = 0;
  let totalInstallment = 0;

  for (let i = 0; i < house_info.length; i++) {
    totalSellingPrice += parseInt(house_info[i].selling_price);
  }

  for (let i = 0; i < installment_info.length; i++) {
    totalInstallment += parseInt(installment_info[i].amount);
  }

  //console.log("Total Installment Amount:", totalInstallment);

  const total_balance = totalSellingPrice - totalInstallment;

  const ratio = total_balance / totalSellingPrice;

  const percentage = ratio * 100;

 // console.error("percentage", percentage.toFixed(2));
  //setPercentage(percentage.toFixed(2));

  //const remRatio = remDays/90
  //onst remPercent = remRatio*100
  //console.log("remaining percentage", remPercent)
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));
  return (
    <div>
      <BorderLinearProgress variant="determinate" value={percentage.toFixed(2)} />
    </div>
  );
}
