import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useNavigate } from "react-router-dom";
import { Dropdown } from "antd";

import {
  Box,
  Button,
  FormControl,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

// const rows = [
//   createData("Nancy Naf", "22", 13, 3287263),
//   createData("Maggey Jane", "34", 14, 9596961),
//   createData("Catherine AFF", "12", 60, 301340),
//   createData("Kenneth Mumbi", "4", 32, 9833520),
//   createData("Peter Neppi", "4", 37, 9984670),
//   createData("John Ndirangu", "7", 25, 7692024),
//   createData("Martin", "18", 83, 357578),
//   createData("Aggy Merry", "12", 48, 70273),
//   createData("Major Jnr", "34", 12, 1972550),
//   createData("Humpry hemy", "4", 12, 377973),
//   createData("Cathy Jane", "6", 67, 640679),
//   createData("Mary Anne", "2", 67, 242495),
//   createData("MAry Jane", "5", 14, 17098246),
//   createData("Martin sila", "8", 20, 923768),
//   createData("Bama Ben", "12", 21, 8515767),
// ];

export default function TestTable() {
  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [openBooking, setOpenBooking] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [client_name, setClientName] = React.useState('john')

  const columns = [
    { id: "created_at", label: "Date and Time", minWidth: 170 },
    {
      id: client_name,
      label: "Clients Name",
      minWidth: 100,
      align: "center",
    },
    {
      id: "population",
      label: "House Unit",
      minWidth: 100,
      align: "center",
      format: (value) => value.toLocaleString("en-US"),
    },

    {
      id: "density",
      label: "Location",
      minWidth: 170,
      align: "center",
      format: (value) => value.toFixed(2),
    },
    {
      id: "size",
      label: "Status",
      minWidth: 170,
      align: "center",
      format: (value) => value.toLocaleString("en-US"),
    },
    {
      id: "size",
      label: "Selling Price",
      minWidth: 170,
      align: "center",
      format: (value) => value.toLocaleString("en-US"),
    },
    {
      id: "size",
      label: "Amount Paid",
      minWidth: 170,
      align: "center",
      format: (value) => value.toLocaleString("en-US"),
    },
    {
      id: "size",
      label: "Balance",
      minWidth: 170,
      align: "right",
      format: (value) => value.toLocaleString("en-US"),
    },
  ];

  function handleGetAllRecords() {
    setLoading(true);
    console.log("handle get all nurses called!");
    fetch("https://rems-backend.onrender.com/get-records")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec logs", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        setRows(json);

        //   console.log("incoming", incoming_nursesData);
        // console.log("filtere:->", filterNursesByWard(json, "Ward A"));
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllRecords();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleDoubleClick = (er) => {
    navigate("/details");
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleClose_booking = () => {
    setOpenBooking(false);
  };
  const handleOpen_booking = () => {
    setOpenBooking(true);
  };
  const items = [
    {
      key: "1",
      label: "Edit Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => handleOpen_booking(),
    },
  ];
  const role = [
    {
      key: "1",
      value: "Admin",
      label: "Interested",
    },
    {
      key: "2",
      value: "Intermediate",
      label: "Booked",
    },
    {
      key: "3",
      value: "Basic",
      label: "Sold",
    },
  ];
  const status = [
    {
      key: "1",
      value: "Active",
      label: "Agent",
      onclick: () => handleOpenAgent(),
    },
    {
      key: "2",
      value: "Inactive",
      label: "No Agent",
    },
  ];
  const type = [
    {
      key: "1",
      value: "studio",
      label: "Studio",
    },
    {
      key: "2",
      value: "1bed",
      label: "One bedroom",
    },
    {
      key: "3",
      value: "2bed",
      label: "Two bedroom",
    },
    {
      key: "4",
      value: "3bed",
      label: "Three bedroom",
    },
    {
      key: "5",
      value: "4bed",
      label: "4 bedroom",
    },
  ];

  return (
    <>
      <Modal
        open={openBooking}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            {/* <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add Record
            </Typography> */}
            <h2 className="font-bold m-4"> Edit Record </h2>
            <CloseIcon
              fontSize="40"
              onClick={handleClose_booking}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
                  Clients Details
                </Typography>

                <div className="flex flex-row gap-3 mt-3">
                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Name of Client"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={"M3 BLOCK A"}
                    // onChange={handleChange}
                  />

                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Client's phone"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={""}
                    //   onChange={formik.handleChange}
                  />
                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Client's Email"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={""}
                    //   onChange={formik.handleChange}
                  />
                </div>
                <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
                  Location Details
                </Typography>
                <div className="flex flex-row gap-3 mt-3">
                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Location"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={"M3 BLOCK A"}
                    // onChange={handleChange}
                  />

                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Residence"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={""}
                    //   onChange={formik.handleChange}
                  />
                  <TextField
                    type="text"
                    id="name"
                    name="name"
                    placeholder="House Unit"
                    // InputProps={{
                    //   readOnly: true,
                    // }}
                    // value={""}
                    //   onChange={formik.handleChange}
                  />
                </div>
                <Typography
                  id="modal-modal-title"
                  sx={{ fontWeight: "bold", marginTop: 2 }}
                >
                  House Type
                </Typography>
                <FormControl>
                  <TextField
                    id="type"
                    select
                    name="type"
                    // value={formik.values.status}
                    // onChange={formik.handleChange}
                    // onBlur={formik.handleBlur}
                  >
                    {type.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
                <Typography
                  id="modal-modal-title"
                  sx={{ fontWeight: "bold", marginTop: 2 }}
                >
                  Agent
                </Typography>
                <FormControl>
                  <TextField
                    id="status"
                    select
                    name="status"
                    // value={formik.values.status}
                    // onChange={formik.handleChange}
                    // onBlur={formik.handleBlur}
                  >
                    {status.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
              </div>
              <Typography
                id="modal-modal-title"
                sx={{ fontWeight: "bold", marginTop: 2 }}
              >
                Sales Details
              </Typography>
              <div className="flex flex-row gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Asking Price"
                  // InputProps={{
                  //   readOnly: true,
                  // }}
                  // value={"M3 BLOCK A"}
                  // onChange={handleChange}
                />

                <TextField
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Selling Price"
                  // InputProps={{
                  //   readOnly: true,
                  // }}
                  // value={""}
                  //   onChange={formik.handleChange}
                />
              </div>
              <div className="flex flex-col gap-3">
                <TextField
                  id="role"
                  select
                  label="Status"
                  name="role"
                  //   value={formik.values.role}
                  //   onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
            </div>
            <div className="flex m-4 gap-4 ">
              <Button
                variant="contained"
                size="small"
                sx={{ backgroundColor: "#242333" }}
                // onClick={handle_submit}
              >
                Save
              </Button>
              <Button
                onClick={handleClose_booking}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="small"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <div className="m-4">
        <Paper sx={{ width: "100%", overflow: "hidden" }}>
          <TableContainer sx={{ maxHeight: 440, minWidth: 1000 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow sx={{ backgroundColor: "rgb(212 212 212)" }}>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{
                        minWidth: column.minWidth,
                      }}
                      sx={{ fontWeight: "bold", fontSize: ["12px", "18px"] }}
                      className="select-none"
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    return (
                      <Dropdown
                        key={row._id}
                        menu={{ items }}
                        trigger={["contextMenu"]}
                        placement="bottomLeft"
                      >
                        <TableRow
                          role="checkbox"
                          tabIndex={-1}
                          key={row.code}
                          sx={{
                            "&:hover": { backgroundColor: "rgb(212 212 212)" },
                            cursor: "pointer",
                          }}
                          className="select-none"
                          onDoubleClick={handleDoubleClick}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      </Dropdown>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Paper>
      </div>
    </>
  );
}
const header = {
  //   border: "1px solid",
  borderColor: "#000",
  margin: "100px",
  padding: "50px",
  borderRadius: "8px",
};
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
};
