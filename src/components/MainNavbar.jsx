import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";
import LightTooltip from "./LightTooltip";
import { useNavigate } from "react-router-dom";
import { faIR } from "@mui/x-date-pickers";
import { useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import Loader2 from "../loader2/Loader2";
import { FaBookOpen } from "react-icons/fa6";


import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";
import avatar from "../assets/avatar5.jpg";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

function formatDate(dateString) {
  // Create a new Date object by parsing the date string
  const date = new Date(dateString);

  // Extract day, month, and year components
  const day = date.getDate();
  const month = date.getMonth() + 1; // Months are zero-based, so we add 1
  const year = date.getFullYear();

  // Format the date as "dd/mm/yyyy"
  return `${day}/${month}/${year}`;
}

export default function MainNavbar({ proprecord }) {
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [openoticafion, setOpenNotification] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [page, setPage] = React.useState(0);

  const [cricallyDue, setCriticallyDue] = useState();

  const [dueClients, setDueClient] = React.useState();

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const isNotiOpen = Boolean(openoticafion);

  const [refresher, setRefresher] = React.useState(false);

  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  //console.log("prop records", proprecord);

  //  console.error("storedData", storedData);
  const due_clients_array = [];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  function handleGetAllRecords() {
    setLoading(true);
    //console.log("handle get all records called!");
    //fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        json.records.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        // setRows(json);
        ///    console.log("DUE CLIENTS", json);

        const total_clients = json.records.length;
        //    console.log("total clients", total_clients);

        const due_clients_array = [];
        const critical_due_client = [];

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
            //       console.log("INSTALLMENTS", element);

            // console.log(
            //   "LAST INSTALLMENT",
            //   element.installments[element.installments.length - 1].created_at
            // );

            const dateString = element.date; // Example date string in "DD/MM/YYYY" format
            const parts = dateString.split("/"); // Split the string into parts
            const year = parseInt(parts[2], 10); // Parse the year part as an integer
            const month = parseInt(parts[1], 10) - 1; // Parse the month part as an integer (subtract 1 since months are zero-based)
            const day = parseInt(parts[0], 10); // Parse the day part as an integer
            //const incoming_date = new Date(year, month, day); // Create the Date object
            //console.log("incoming date", date);
            //const incoming_date = new Date(element.installments[element.installments.length - 1].created_at)

            //const date =formatDate(date_notformated)

            if (
              element.installments &&
              element.installments.length > 0 &&
              element.installments[element.installments.length - 1]
                .created_at != null
            ) {
              const lastInstallment =
                element.installments[element.installments.length - 1];
              const incoming_date = new Date(lastInstallment.created_at);

              //        console.log("Incoming Date:", incoming_date);
              //
              //       console.error("formatted date", incoming_date);

              //const date = new Date(element.date);

              // console.log(
              //   "due date",
              //   incoming_date.setDate(incoming_date.getDate() + 90)
              // );


              incoming_date.setDate(incoming_date.getDate() + 90)

         
              const currentDate = new Date();
             // console.log("current date", currentDate);

              // Assuming incoming_date is defined somewhere earlier in your code
              //const incoming_date = new Date("2024-03-01"); // Example date
              incoming_date.setDate(incoming_date.getDate()); // Adding 90 days

              const daysRemaining = Math.ceil(
                (incoming_date - currentDate) / (1000 * 60 * 60 * 24)
              );

              //console.log("Days remaining until due date:", daysRemaining);
             // console.log("Incoming date:", incoming_date);
             // console.log("Days remaining:", daysRemaining);

              const due_client_obj = {
                first_name: element.client_information.first_name,

                last_name: element.client_information.last_name,

                prev_date: element.date,

                due_date: formatDate(new Date(incoming_date)),
                days_remaining: daysRemaining,
              };

              if (daysRemaining <= 10) {
                critical_due_client.push(due_client_obj);
              }

              //  console.log("due date", due_client_obj);

              due_clients_array.push(due_client_obj);
            } else {
              //   console.log("No valid installment found");
            }
          }
        });

        //     console.error("critically due clients:---", critical_due_client.length);

        setCriticallyDue(critical_due_client.length);
        ///<******************************************END OF MAIN JSON LOOP***********************************************************>

        //  console.log("due clients", due_clients_array);

        due_clients_array.sort((a, b) => a.days_remaining - b.days_remaining);

        ///console.log("sorted array", due_clients_array);

        // console.log("sorted array", due_clients_array);
        //console.log("ARR:->", due_clients_array.slice(0, 10))
        setRows(due_clients_array.slice(0, 10));
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    handleGetAllRecords();
  }, [refresher]);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleNotificationMenuOpen = () => {
    setOpenNotification(true);
  };
  const handleNotificationMenuClose = () => {
    setOpenNotification(false);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };
  const handleNavigate_Booking = () => {
    navigate("/records");
  };
  const handleNavigate_details = () => {
    navigate("/details");
  };
  const handleNavigate_home = () => {
    navigate("/home");
  };
  const handleNavigate_myAccount = () => {
    navigate("/myaccount");
  };
  const handleNavigate_Profile = () => {
    navigate("/profile");
  };

  const handleNavigate_logout = () => {
    setTimeout(() => {
      setLoading(true);
      navigate("/");
    }, 2000); // 2000 milliseconds delay (2 seconds)
  };
  const handleNavigate_recyclebin = () => {
    navigate("/recycle");
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem sx={{ fontSize: [13, 16] }} onClick={handleNavigate_myAccount}>
        My account
      </MenuItem>
      <MenuItem sx={{ fontSize: [13, 16] }} onClick={handleNavigate_recyclebin}>
        Recycle bin
      </MenuItem>
      <MenuItem sx={{ fontSize: [13, 16] }} onClick={handleNavigate_logout}>
        Log Out
      </MenuItem>
      {/* <MenuItem sx={{ fontSize: [13, 16] }} onClick={handleNavigate_Profile}>
        Feedback
      </MenuItem> */}
    </Menu>
  );

  const notifactionId = "primary-notification-menu";

  const renderNotification = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={notifactionId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isNotiOpen}
      onClose={handleNotificationMenuClose}
    >
      {/* <MenuItem>Jane Okello Due - 12/20/20</MenuItem>
      <MenuItem>Peter Mukami</MenuItem>
      <MenuItem>Mercy Jane</MenuItem>
      <MenuItem>Carene Nandi</MenuItem> */}

      <div className="p-4 overflow-visible">
        {/* <Typography
          sx={{
            marginBottom: 1,
            fontWeight: "bold",
            fontSize: ["13px", "18px"],
            // color: "#fff",
            // bgcolor: "#242333",
            // padding:1,
            // borderRadius:8,
          }}
        >
          (Notifications) Clients Due Dates
        </Typography> */}

        {/* <ul>
          {rows.map((item, index) => (
            // <li key={index}>{item.first_name}</li>
            <li
              className="flex flex-row space-x-2 cursor-pointer hover:bg-gray-200 py-1"
              key={index}
            >
              <Typography sx={{ fontSize: ["13px", "15px"] }}>
                {item.first_name} {item.last_name}
              </Typography>

              <Typography sx={{ fontSize: ["13px", "15px"] }}>
                Last Installment: {item.prev_date.toString()}
              </Typography>

              <Typography sx={{ fontSize: ["13px", "15px"] }}>
                remaing: {item.days_remaining}
              </Typography>

              <Typography sx={{ fontSize: ["13px", "15px"] }}>
                Due Date: {item.due_date.toString()}
              </Typography>
            </li>
          ))}
        </ul> */}

        <div className="">
          <div className="flex flex-row justify-between mb-2">
            <Typography
              sx={{ fontWeight: "bold" }}
              className="text-md font-bold mb-4"
            >
              Client Due Date
            </Typography>
            <CloseIcon
              sx={{ fontSize: 14, cursor: "pointer" }}
              onClick={handleNotificationMenuClose}
            />
          </div>

          <ul className="flex flex-col gap-1 w-full bg-gray-100 rounded-lg overflow-hidden shadow-lg">
            <li className="flex flex-row  bg-gray-200 p-3 space-x-4">
              <Typography className=" text-[#000]">Name</Typography>

              <Typography className=" text-[#000]">DueDate</Typography>

              <Typography className=" text-[#000]">RemainingDays</Typography>
            </li>
            {rows.map((item) => (
              <li
                key={item.first_name}
                className=" flex flex-row space-x-4 hover:bg-gray-300 p-2"
              >
                <Typography className="text-sm">
                  <AccountCircle sx={{ fontSize: 15 }} /> {item.first_name}{" "}
                  {item.last_name}
                </Typography>
                <Typography className="">{item.due_date}</Typography>
                <Typography className="">{item.days_remaining}</Typography>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem onClick={handleNotificationMenuOpen}>
        <IconButton
          size="large"
          aria-label="show 1 new notifications"
          color="inherit"
        >
          <Badge badgeContent={1} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      {/* {loading && <Loader2/>} */}
      <div>

      {/* <Box sx={{ flexGrow: 1 }}> */}
        <AppBar
          position="static"
          sx={{ backgroundColor: "#242333", boxShadow: 3, width: "100%" }}
        >
          <Toolbar>
      
            <Typography
              // variant="h4"
              noWrap
              component="div"
              sx={{
                display: { xs: "block", sm: "block" },
                fontWeight: "bold",
                cursor: "pointer",
                fontSize: ["18px", "26px"],
              }}
              onClick={handleNavigate_home}
              className="hover:scale-110 duration-200 ease-linear select-none"
            >
              REMS &nbsp;
              {/* <Typography
              sx={{ fontSize: ["10px", "26px"] }}
              className=" bg-[#FF9900] p-1 rounded-2xl text-[#1E1E1E] justify-center"
            >
              Management system
            </Typography> */}
            </Typography>

            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: "flex", md: "flex" } }}>
              <Box className="mt-4 ">
                <Box
                  className="flex flex-row"
                  sx={{ gap: "12px", display: { xs: "flex" } }}
                >
                  <LightTooltip title="View Tables">
                    <Typography
                      sx={{
                        transition: "border-bottom-width 0.3s ease",
                        "&:hover": {
                          borderBottomWidth: "2px",
                        },
                        cursor: "pointer",
                        fontSize: ["12px", "16px"],
                       
                      }}
                      onClick={handleNavigate_Booking}
                      className="select-none"
                    >
                      Records
                    </Typography>
                  </LightTooltip>
                </Box>
              </Box>
              <LightTooltip title="notifications">
                <IconButton
                  size="large"
                  aria-label="show 17 new notifications"
                  aria-controls={notifactionId}
                  color="inherit"
                  aria-haspopup="true"
                  onClick={handleNotificationMenuOpen}
                >
                  <Badge badgeContent={cricallyDue} color="error">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              </LightTooltip>

              <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
                className="flex flex-row gap-1"
              >
                <img src={avatar} alt="" className="w-8 rounded-full" />
                <Typography
                  sx={{
                    cursor: "pointer",
                    fontSize: ["12px", "16px"],
                  }}
                >
                  {storedData.FirstName}
                </Typography>
              </IconButton>
            </Box>
            {/* <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box> */}
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
        {renderNotification}
      {/* </Box> */}


      </div>
    
    </>
  );
}
