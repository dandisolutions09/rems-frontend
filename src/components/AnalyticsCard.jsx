import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { red } from "@mui/material/colors";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import HouseIcon from "@mui/icons-material/House";
import CircularWithValueLabel from "./CircularProgress";
import hsp from "../assets/hsproperty.svg";
import bal from "../assets/bal.svg";
import rev from "../assets/rev.svg";
import client from "../assets/client.svg";
import { BsCashStack } from "react-icons/bs";
import { MdHouse } from "react-icons/md";
import { FaPeopleArrows } from "react-icons/fa";
import { GiReceiveMoney } from "react-icons/gi";
import { FaBalanceScale } from "react-icons/fa";

import { LineChart } from "@mui/x-charts/LineChart";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function AnalyticsCard(props) {
  const [expanded, setExpanded] = React.useState(false);
  const [revenuePercentage, setRevenuePercentage] = React.useState();
  const [rows, setRows] = React.useState([]);
  const [totalInstallments, setTotalInstallments] = React.useState("");
  const [totallSellingPrice, setTotalSellingPrice] = useState();
  const [totallBalance, setBalance] = useState();
  const [totalClients, setTotalClients] = useState(0);
  const [totalProperties, setTotalProperties] = useState();

  // console.warn(
  //   "incoming props",
  //   props.inc_records.client_information.house_status
  // );

  function handleGetAllRecords() {
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    //console.log("ID", storedData);
    //setGlobalId(storedData._id);
    //setLoading(true);
    //console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);
        let totalInstallmentsPaid = 0;
        let totalSellingPrice = 0; // Initialize totalSellingPrice counter
        let totalProperties = 0;

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        //console.log("after-sorting-records:->", activeRecords);

        setRows(activeRecords);

        //console.log("active records----->", activeRecords);

        setTotalClients(activeRecords.length);

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
            const totalSellingPrices = element.house_information.reduce(
              (total, house) => {
                return total + parseFloat(house.selling_price);
              },
              0
            );

           // console.log("total clients-->", activeRecords.length);

            //    setTotalClients(activeRecords.length);

            totalSellingPrice += totalSellingPrices;
            // Increment the totalSellingPrice counter

            const totalInstallments = element.installments.reduce(
              (total, installment) => {
                return total + parseFloat(installment.amount);
              },
              0
            );
            // Increment the totalInstallmentsPaid counter
            totalInstallmentsPaid += totalInstallments;

            const numHouses = element.house_information.length;
            //console.log("number of houses-->", numHouses);
            // Increment the totalProperties counter
            totalProperties += numHouses;
          }

          // setBalance(totalSellingPrice - totalInstallmentsPaid);
          // console.log("total nst", totalInstallmentsPaid);
        //  console.log("total number of houses:-", totalProperties);
          setTotalProperties(totalProperties);

          setTotalInstallments(totalInstallmentsPaid);
          setTotalSellingPrice(totalSellingPrice);
          // console.log(
          //   "balance",
          //   parseFloat(totalSellingPrice - totalInstallmentsPaid)
          // );

          const bal = parseFloat(totalSellingPrice - totalInstallmentsPaid);
          //console.log("balance", bal / 1000000);
          setBalance(parseFloat(bal));
        });
      })
      .finally(() => {
        // setLoading(false);
      });
  }

 // console.error("analytics card");

  const revenuePercentage_ =
    (props.totalInstallments / props.totalSellingPrice) * 100;
  //console.log("revenue percentage", revenuePercentage_.toFixed(2));

  // Set the revenue percentage state
  //setRevenuePercentage(revenuePercentage_.toFixed(2));

  React.useEffect(() => {
    const calculateRevenuePercentage = () => {
      const revenuePercentage_ =
        (props.totalInstallments / props.totalSellingPrice) * 100;
      // console.log("revenue percentage", revenuePercentage_.toFixed(2));
      setRevenuePercentage(revenuePercentage_.toFixed(2));
    };

    calculateRevenuePercentage();

    handleGetAllRecords();
  }, [props.totalInstallments, props.totalSellingPrice]);

  // const revenuePercentage_ =
  //   (props.totalInstallments / props.totalSellingPrice) * 100;
  // console.log("revenue percentage", revenuePercentage_.toFixed(2));

  // setRevenuePercentage(revenuePercentage_.toFixed(2));
  const navigate = useNavigate();

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleNavigate_Records = () => {
    navigate("/records");
  };

  return (
    <>
      {/* {rows} */}

      <div className="m-2">
        <div className="flex flex-col md:flex-col lg:flex-col gap-2">
          {/* <div className="grid grid-cols-4 gap-4"> */}
          {/* <div className="flex sm:flex-col md:flex-col flex-col"> */}

          <div className="border border-gray-300 p-4 shadow-lg rounded-xl bg-[#242333] flex py-8">
            {/* <p className="text-[12px]">Total Revenue:</p> */}
            <div className="flex flex-row gap-2">

              <BsCashStack style={{ color: "white" }} />
              <div className="  md:max-w-lg">
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Total Property Value Sold:
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "40px" }}
                >
                  {totallSellingPrice !== undefined ? (
                    (totallSellingPrice / 1000000).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) + "m"
                  ) : (
                    <Typography sx={{ fontSize: 10 }}>0</Typography>
                  )}
                  {/* {totallSellingPrice}m */}
                  {/* {rows.reduce((total, record) => {
                  const sellingPrice = parseFloat(record.selling_price);
                  return isNaN(sellingPrice) ? total : total + sellingPrice;
                }, 0)} */}
                </Typography>
              </div>
            </div>
          </div>
          {/* first leg */}

          <div className="flex flex-col md:flex-col sm:flex-col lg:flex-row gap-2 w-full">
            <div className="flex flex-row md:flex-row gap-2 w-full">
              <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                <MdHouse style={{ color: "white", margin: "2px" }} />
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Number of Properties Sold:
                </Typography>
                <Typography
                  variant="body2"
                  color="white"
                  style={{ fontSize: "40px" }}
                >
                  {totalProperties !== undefined ? (
                    totalProperties.toLocaleString()
                  ) : (
                    <Typography sx={{ fontSize: 10 }}>0</Typography>
                  )}
                </Typography>
              </div>
              <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-[#242333]">
                <FaPeopleArrows style={{ color: "white", margin: "2px" }} />
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Clients:
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "40px" }}
                >
                  {totalClients !== undefined ? (
                    totalClients.toLocaleString()
                  ) : (
                    <Typography sx={{ fontSize: 10 }}>0</Typography>
                  )}
                </Typography>
              </div>
            </div>
            <div className="flex flex-row md:flex-row gap-2">
              <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                <GiReceiveMoney style={{ color: "white", margin: "2px" }} />

                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Total Deposits
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "34px" }}
                >
                  {totalInstallments !== undefined ? (
                    (totalInstallments / 1000000).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) + "m"
                  ) : (
                    <Typography sx={{ fontSize: 10 }}>0</Typography>
                  )}
                </Typography>
              </div>
              <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-[#242333]">
                <FaBalanceScale style={{ color: "white", margin: "2px" }} />
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "12px" }}
                >
                  Remaining Balance
                </Typography>
                <Typography
                  variant="body2"
                  color="#fff"
                  style={{ fontSize: "40px" }}
                >
                  {totallBalance !== undefined ? (
                    (totallBalance / 1000000).toLocaleString(undefined, {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) + "m"
                  ) : (
                    <Typography sx={{ fontSize: 10 }}>0</Typography>
                  )}
                </Typography>
              </div>
            </div>
          </div>
          <div className="flex justify-center">
            <div
              className="flex border md:mt-6 md:mb-6  md:w-[600px] md:flex md:justify-center items-center border-gray-300 p-4 w-1/2 md: shadow-lg rounded-xl bg-gray-400 mt-2 justify-center  text-center cursor-pointer hover:bg-opacity-50 ease-in duration-200  select-none"
              onClick={handleNavigate_Records}
            >
              <Typography color={"#111111"}> View Records</Typography>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
