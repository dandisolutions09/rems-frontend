import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, Typography } from "@mui/material";


function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("12/3/2024 12:30 am", 159, 6.0, 24, 4.0),
  createData("2/3/2024 12:30 am", 237, 9.0, 37, 4.3),
  createData("12/3/2024 12:30 am", 262, 16.0, 24, 6.0),
  createData("12/3/2024 12:30 am", 305, 3.7, 67, 4.3),
  createData("12/3/2024 12:30 am", 356, 16.0, 49, 3.9),
];

export default function NotesTable() {
  return (
    <>
      <div className="flex m-3">
        <Button variant="contained" >
          Add notes
        </Button>
      </div>
      
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Date And Time</TableCell>
              <TableCell >Notes</TableCell>
              {/* <TableCell >Fat&nbsp;(g)</TableCell>
              <TableCell >Carbs&nbsp;(g)</TableCell>
              <TableCell >Protein&nbsp;(g)</TableCell> */}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell >{row.calories}</TableCell>
                {/* <TableCell >{row.fat}</TableCell>
                <TableCell >{row.carbs}</TableCell>
                <TableCell >{row.protein}</TableCell> */}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
   