import React from "react";
import MainNavbar from "../components/MainNavbar";
import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import hsp from "../assets/avatar5.jpg";
import hsp1 from "../assets/camera.svg";
import NotesTable from "../components/NotesTable";
import NotesDate from "../components/NotesDate";
import { useState } from "react";
import BarsDataset from "../components/AccountChartgraph";
import Loader2 from "../loader2/Loader2";
import { useNavigate } from "react-router-dom";
import CloseIcon from "@mui/icons-material/Close";
import { BsCashStack } from "react-icons/bs";
import { MdHouse } from "react-icons/md";
import { FaPeopleArrows } from "react-icons/fa";
import { GiReceiveMoney } from "react-icons/gi";
import { FaBalanceScale } from "react-icons/fa";
import Spinner1 from "../components/Spinner1";

export default function MyAccount() {
  const [openHouseDetailsModal, setOpenHouseDetailsModal] =
    React.useState(false);
  const [expanded, setExpanded] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [revenuePercentage, setRevenuePercentage] = React.useState();
  const [rows, setRows] = React.useState([]);
  const [totalInstallments, setTotalInstallments] = React.useState("");
  const [totallSellingPrice, setTotalSellingPrice] = useState();
  const [totallBalance, setBalance] = useState();
  const [totalClients, setTotalClients] = useState(0);
  const [totalProperties, setTotalProperties] = useState();
  const [fullName, setName] = useState();
  const [bonus, setBonus] = useState();
  const [bonusPayments, setBonusPayments] = useState();
  const [bonusAgent, setBonusAgent] = useState();

  const [bonus_multiplier, setBonusMultiplier] = useState(0.005);
  const [email, setEmail] = useState();

  function handleGetAllRecords() {
    setLoading(true);
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    // console.log("ID", storedData);
    //setGlobalId(storedData._id);
    //setLoading(true);
    //console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //  console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);
        let totalInstallmentsPaid = 0;
        let totalSellingPrice = 0; // Initialize totalSellingPrice counter
        let totalProperties = 0;
        let bonus_dataset = [];

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        // console.log("after-sorting-records:->", activeRecords);

        setRows(activeRecords);
        setName(json.first_name + "    " + json.last_name);

        setEmail(json.email);

        //setPhoneNumber(json.first_name + "    "+ json.last_name);

        //console.log("active records----->", activeRecords);

        setTotalClients(activeRecords.length);

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
            const totalSellingPrices = element.house_information.reduce(
              (total, house) => {
                return total + parseFloat(house.selling_price);
              },
              0
            );

            //console.error("total Bonus Recieved", totalSellingPrices * 0.03);
            const totalBonus = totalSellingPrices * 0.005;

            //console.log("total bonus inside loop:-", totalBonus);

            const bonus_obj = {
              client_first_name: element.client_information.first_name,
              client_second_name: element.client_information.second_name,
              house: element.house_information,
              date: element.date,
              broker: element.broker_information.first_name,
              //house_bonus: totalSellingPrices * bonus_multiplier,
              house_bonus:
                element.broker_information.first_name.trim() == ""
                  ? totalSellingPrices * bonus_multiplier
                  : 7000,
              bonus_paid: element.bonus_payments.reduce((total, payment) => {
                // Ensure that amount exists and is not empty
                if (payment.amount && !isNaN(parseFloat(payment.amount))) {
                  return total + parseFloat(payment.amount);
                } else {
                  return total;
                }
              }, 0),
              // month: monthName,
            };

            //console.log("bonus object-->", bonus_obj);
            bonus_dataset.push(bonus_obj);

            //setBonus(totalBonus);

            //     console.log("total clients-->", activeRecords.length);

            //    setTotalClients(activeRecords.length);

            totalSellingPrice += totalSellingPrices;
            // Increment the totalSellingPrice counter

            const totalInstallments = element.installments.reduce(
              (total, installment) => {
                return total + parseFloat(installment.amount);
              },
              0
            );
            // Increment the totalInstallmentsPaid counter
            totalInstallmentsPaid += totalInstallments;

            const numHouses = element.house_information.length;
            //console.log("number of houses-->", numHouses);
            // Increment the totalProperties counter
            totalProperties += numHouses;
          } // end og activeRecords for loop
          //console.error("TOTAL SALES", totalSellingPrice * bonus_multiplier);

          // setBalance(totalSellingPrice - totalInstallmentsPaid);
          // console.log("total nst", totalInstallmentsPaid);
          //   console.log("total number of houses:-", totalProperties);
          setTotalProperties(totalProperties);

          setTotalInstallments(totalInstallmentsPaid);
          setTotalSellingPrice(totalSellingPrice);
          // console.log(
          //   "balance",
          //   parseFloat(totalSellingPrice - totalInstallmentsPaid)
          // );

          const bal = parseFloat(totalSellingPrice - totalInstallmentsPaid);
          //console.log("balance", bal / 1000000);
          setBalance(parseFloat(bal));
        });

        //console.log("bonus dataset:-->", bonus_dataset);

        const totalBonus = bonus_dataset.reduce((total, x) => {
          return total + parseFloat(x.house_bonus);
        }, 0);

        const totalBonusAgent = bonus_dataset.reduce((total, x) => {
          if (parseFloat(x.house_bonus) === 7000) {
            return total + parseFloat(x.house_bonus);
          } else {
            return total;
          }
        }, 0);

        const totalBonusPayments = bonus_dataset.reduce((total, x) => {
          return total + parseFloat(x.bonus_paid);
        }, 0);

        console.log("total bonus:-->", totalBonus);
        console.log("totalBonusPayment-->", totalBonusPayments);
        console.log("bonus agents-->", totalBonusAgent);
        setBonus(totalBonus);
        setBonusPayments(totalBonusPayments);

        setBonusAgent(totalBonusAgent);

        //const totalBonus = totalSellingPrice * bonus_multiplier;
        // const totalBonus =
        //   element.broker_information.first_name.trim() == ""
        //     ? totalSellingPrice * bonus_multiplier
        //     : 7000;

        //console.log("total selling price", totalSellingPrice);

        // console.log("total bonus", totalBonus);

        //setBonus(totalBonus);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    handleGetAllRecords();
  }, []);

  const navigate = useNavigate();

  const handleNavigaye_bonuspage = () => {
    navigate("/bonuspage");
  };
  function handleOpenModal() {
    console.log("opening modal");
    setOpenHouseDetailsModal(true);
  }
  function handleCloseModal() {
    console.log("opening modal");
    setOpenHouseDetailsModal(false);
  }
  const [file, setFile] = useState();
  const [openfile, setOpenfile] = useState(false);
  function handleChange(e) {
    console.log(e.target.files);
    setFile(URL.createObjectURL(e.target.files[0]));
  }
  const handleOpen_file = () => setOpenfile(true);
  const handleClose_file = () => setOpenfile(false);

  return (
    <>
      <Modal
        open={openHouseDetailsModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-1">Bonus PayOut</h2>
            <CloseIcon
              fontSize="40"
              onClick={handleCloseModal}
              sx={{ cursor: "pointer" }}
            />
          </div>

          <form>
            <div className="flex flex-col gap-3 mt-3">
              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Date"
                placeholder="Date of payout"
                // error={
                //   formikHouseDetails.touched.selling_price &&
                //   Boolean(formikHouseDetails.errors.selling_price)
                // }
                // helperText={
                //   formikHouseDetails.touched.selling_price &&
                //   formikHouseDetails.errors.selling_price
                // }
                // value={formikHouseDetails.values.selling_price}
                // onChange={(e) => {
                //   const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                //   const formattedValue = rawValue
                //     .replace(/\D/g, "")
                //     .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                //   formikHouseDetails.setFieldValue(
                //     "selling_price",
                //     formattedValue // Corrected to set "selling_price" instead of "asking_price"
                //   );
                // }}
                // onBlur={formikHouseDetails.handleBlur}
              />
              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Last Amount Paid"
                placeholder="Last Amount Paid"
                value={"KES 12,000"}
                // error={
                //   formikHouseDetails.touched.selling_price &&
                //   Boolean(formikHouseDetails.errors.selling_price)
                // }
                // helperText={
                //   formikHouseDetails.touched.selling_price &&
                //   formikHouseDetails.errors.selling_price
                // }
                // value={formikHouseDetails.values.selling_price}
                // onChange={(e) => {
                //   const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                //   const formattedValue = rawValue
                //     .replace(/\D/g, "")
                //     .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                //   formikHouseDetails.setFieldValue(
                //     "selling_price",
                //     formattedValue // Corrected to set "selling_price" instead of "asking_price"
                //   );
                // }}
                // onBlur={formikHouseDetails.handleBlur}
              />
              <TextField
                type="text"
                id="selling_price"
                name="selling_price"
                label="Amount Paid"
                placeholder="Amount Paid"
                // error={
                //   formikHouseDetails.touched.selling_price &&
                //   Boolean(formikHouseDetails.errors.selling_price)
                // }
                // helperText={
                //   formikHouseDetails.touched.selling_price &&
                //   formikHouseDetails.errors.selling_price
                // }
                // value={formikHouseDetails.values.selling_price}
                // onChange={(e) => {
                //   const rawValue = e.target.value.replace(/,/g, ""); // Remove existing commas
                //   const formattedValue = rawValue
                //     .replace(/\D/g, "")
                //     .replace(/\B(?=(\d{3})+(?!\d))/g, ","); // Add commas every three digits
                //   formikHouseDetails.setFieldValue(
                //     "selling_price",
                //     formattedValue // Corrected to set "selling_price" instead of "asking_price"
                //   );
                // }}
                // onBlur={formikHouseDetails.handleBlur}
              />
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"

                  // onClick={handleOpen_AddHouseInfor}
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <MainNavbar />
      {openfile && (
        <Box sx={style_box2} className="z-50">
          <Typography>Add Image:</Typography>
          <div className="flex flex-row gap-3">
            <input type="file" onChange={handleChange} />
            <img
              src={file}
              className="w-[50px] h-[50px] rounded-full object-cover"
            />
          </div>
          <Button variant="contained" color="error" onClick={handleClose_file}>
            Close
          </Button>
        </Box>
      )}
      {/* {loading && <Loader2 />} */}
      <div className="flex flex-col top-[50%] right-[50%] justify-center items-center absolute">
      {loading && <Spinner1 className="z-50" />}</div>
      <div className="flex flex-col mt-3 m-3">
        <div className="flex justify-center text-center border border-gray-100 w-full bg-white  p-4 shadow-lg cursor-pointer rounded-lg">
          <div className="flex flex-col space-y-4 justify-center text-center">
            <Typography
              variant="h6"
              sx={{ fontWeight: "bold", fontSize: [13, 15], margin: 1 }}
            >
              My Account
            </Typography>
            <div>
              <img
                // src={file}
                src={hsp}
                alt="house property"
                className="w-[150px] h-[150px] rounded-full"
              />
              <img
                src={hsp1}
                alt=""
                onClick={handleOpen_file}
                className="absolute ml-[108px] mt-[-45px] hover:scale-110 ease-linear duration-200"
              />
            </div>
            <div className="">
              <Typography
                variant="body2"
                color="text.primary"
                style={{ fontSize: "14px", fontWeight: "bold" }}
              >
                {fullName}
              </Typography>
              <Typography sx={{ fontSize: [12, 15] }} color="text.secondary">
                {email}
              </Typography>
            </div>
          </div>
        </div>

        {/* <MyAccountCards /> */}

        <div className=" mt-3">
          <div className="flex flex-col md:flex-col lg:flex-col gap-2">
            <div className="flex flex-col md:flex-col sm:flex-col lg:flex-row gap-2 w-full">
              <div className="flex flex-row md:flex-row gap-2 w-full">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-blue-500">
                  <MdHouse style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Total Bonus Accumulated:
                  </Typography>

                  <Typography
                    variant="body2"
                    color="#fff"
                    sx={{ fontSize: [19, 30] }}
                  >
                    KES {bonus ? parseFloat(bonus).toLocaleString() : "0"}
                  </Typography>
                </div>
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-full p-4 shadow-lg rounded-xl bg-blue-500">
          
                  <FaBalanceScale style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Balance:
                  </Typography>

                  <Typography
                    variant="body2"
                    color="#fff"
                    sx={{ fontSize: [19, 30] }}
                  >
                    KES{" "}
                    {bonus - bonusPayments
                      ? parseFloat(bonus - bonusPayments).toLocaleString()
                      : "0"}
                  </Typography>
                </div>
              </div>
              <div className="flex flex-row md:flex-row gap-2">
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-blue-500">
                  <GiReceiveMoney style={{ color: "white", margin: "2px" }} />

                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Total Bonus for Agents:
                  </Typography>

                  <Typography
                    variant="body2"
                    color="#fff"
                    sx={{ fontSize: [19, 30] }}
                  >
                    KES{" "}
                    {bonusAgent ? parseFloat(bonusAgent).toLocaleString() : "0"}
                  </Typography>
                </div>
                <div className=" border border-gray-300 w-[280px]  md:w-[445px] lg:w-[350px] p-4 shadow-lg rounded-xl bg-blue-500">
                  <FaPeopleArrows style={{ color: "white", margin: "2px" }} />
                  <Typography
                    variant="body2"
                    color="#fff"
                    style={{ fontSize: "12px" }}
                  >
                    Total Bonus Paid:
                  </Typography>

                  <Typography
                    variant="body2"
                    color="#fff"
                    sx={{ fontSize: [19, 30] }}
                  >
                    KES{" "}
                    {bonusPayments
                      ? parseFloat(bonusPayments).toLocaleString()
                      : "0"}
                  </Typography>
                </div>
              </div>
            </div>
            <div className="flex justify-center">
              <div
                className="flex border md:mt-6 md:mb-6  md:w-[600px] md:flex md:justify-center items-center border-gray-300 p-4 w-1/2 md: shadow-lg rounded-xl bg-red-500 mt-2 justify-center  text-center cursor-pointer hover:bg-opacity-50 ease-in duration-200  select-none"
                onClick={handleNavigaye_bonuspage}
              >
                <Typography color={"#fff"}> View More</Typography>
              </div>
            </div>
          </div>
        </div>
        <BarsDataset />

        {/* <NotesDate />
        <NotesTable /> */}
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  overflowX: "auto",
  maxWidth: "80vh",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};
const style_box2 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "#fff",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  overflowX: "auto",
  maxWidth: "80vh",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};
