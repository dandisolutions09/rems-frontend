import React from 'react'
import TestTable from '../components/TestTable'

import MainNavbar from '../components/MainNavbar';

export default function BookingTable() {
  return (
    <>
      <MainNavbar />
      <div className="m-12 p-4 mt-[70px]">
        <TestTable />
      </div>
    </>
  );
}
