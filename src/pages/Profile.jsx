import React from "react";
import MainNavbar from "../components/MainNavbar";
// import Loader from "../loader1/Loader";
import Loader2 from "../loader2/Loader2";
import HolidayVillageIcon from "@mui/icons-material/HolidayVillage";
// import Loader3 from "../loader3/Loader3";
// import Loader4 from "../loader4/Loader4";
import Loader5 from "../loader5/Loader5";
import Loader from "../loader1/Loader";
import Details from "./Details";
import Loader4 from "../loader4/Loader4";
import { Box, Button, Typography } from "@mui/material";

export default function Profile() {
  return (
    <>
      {/* <MainNavbar /> */}

      <div
        className="flex justify-center text-center items-center"
        style={{
          backgroundImage: `url('src/assets/remsbg.jp')`, // Specify the path to your image
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          minHeight: "100vh", // Ensure the background covers the entire viewport
        }}
      >
        <div className="justify-center text-center items-center m-6 w-[300px] h-[200px] ">
          <div className="w-full text-left">
            <Typography
              sx={{ fontSize: 30, fontWeight: 700, color: "#242333" }}
            >
              Choose your payment <span className="text-[#242333]">plan</span>
            </Typography>
          </div>
          <div className="ml-[33%] mt-[24%] flex ">
            <Loader />
            <div className="flex absolute ml-[33px] mt-[-3px] text-[white]">
              <Typography
                sx={{ fontWeight: 900, fontSize: 26 }}
                className=" underline"
              >
                <span className="text-whit">R</span>EM
                <span className="">S</span>
              </Typography>
              {/* <HolidayVillageIcon /> */}
            </div>
          </div>
          <div className="mt-4 text-[#fff]">
            <Typography sx={{ fontSize: [10, 12] }}>
              Welcom To REMS...
            </Typography>
          </div>
        </div>
        {/* <Button
          variant="contained"
          size="small"
          sx={{ backgroundColor: "#242333" }}
        >
          Back Home
        </Button> */}
      </div>

      {/* <Loader2 />  */}

      {/* <Loader5/> */}
      {/* <Loader4/> */}
      {/* <Details/> */}
    </>
  );
}
