import React, { useState } from "react";
import MainNavbar from "../components/MainNavbar";
import BarChartReacords from "../components/BarChartReacords";
import BlockA from "../blocks/BlockA";
import BasicDatePicker from "../components/DatePicker";
import { BarChart } from "@mui/x-charts";
import { axisClasses } from "@mui/x-charts";
import RecipeReviewCard from "../components/AnalyticsCard";
import DownloadIcon from "@mui/icons-material/Download";
import StatsCard from "../components/AnalyticsCard";
import AnalyticsCard from "../components/AnalyticsCard";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  IconButton,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";
import InstalmentTable from "../components/DueClients";
// import { ChartGraph } from "../components/Barchart";
import LightTooltip from "../components/LightTooltip";
import { useLocation } from "react-router-dom";
import Loader2 from "../loader2/Loader2";
// import { ChartGraph } from "../components/ChartGraph";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import ChartGraph from "../components/ChartGraph";
import DueClients from "../components/DueClients";
import Spinner1 from "../components/Spinner1";

export default function Dashboard() {
  const [selectedPercentage, setSelectedPercentage] = useState("");
  const [amount, setAmount] = useState("");
  const [results, setResults] = useState("");
  const [refresher, setRefresher] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [rows, setRows] = useState([]);
  const [totalCients, setClients] = useState();
  const [properties, setTotalProperties] = useState();
  const [totallInstallments, setTotalInstallments] = useState();
  const [totallSellingPrice, setTotalSellingPrice] = useState();
  const [graphData, setgraphData] = useState([]);
  const [monthlyDataset_clients, setMonthlyDatasetForClients] = useState([]);
  const [openCalculator, setOpenCalculator] = useState();
  const [monthlyDataset_properties, setMonthlyDatasetForProperties] = useState(
    []
  );
  const [totalMonthlyInstallment, setTotalMonthlyInstallments] = useState([]);
  const [UserId, setUserId] = useState();
  const [active_recs, setAtiveRecords] = useState();

  const location = useLocation();
  const incoming_username_data = location.state;

  var storedDataJSON = localStorage.getItem("userData_storage");
  var storedData = JSON.parse(storedDataJSON);

  //  console.log("incoming user name-->", incoming_username_data);
  // setUserId(incoming_username_data.usr_id);
  let datasetForclients = [];

  const monthlyData = [];
  const option = [
    {
      key: "1",
      value: "0.5",
      label: "Bonus 0.005%",
    },
    {
      key: "2",
      value: "3",
      label: "Commission 0.03%",
    },
  ];

  var monthlyclientDataset = [];

  const handleopen_calc = () => {
    setOpenCalculator(true);
  };
  const handleclose_calc = () => {
    setOpenCalculator(false);
  };

  function handleGetAllRecords(usrId) {
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);
    //console.warn("handle get all records called!", usrId);
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //console.log("JSON:->", json.records);
        json.records.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

       // console.log("ACTIVE RECORDS ONLY", activeRecords);

        setRows(activeRecords);
        setAtiveRecords(activeRecords);
        setUserId(storedData._id);
        //console.log("rec logs", json.records);

        const total_clients = activeRecords.length;
       // console.log("total clients", total_clients);
        setClients(total_clients);

        let totalProperties = 0; // Initialize totalProperties counter

        let totalInstallmentsPaid = 0; // Initialize totalInstallmentsPaid counter
        let totalSellingPrice = 0; // Initialize totalSellingPrice counter
        let month = "none";
        let graph_array_for_clients = [];
        let graph_array_for_installments = [];

        function extractMonth(dateString) {
          return new Date(dateString).toLocaleString("en-US", {
            month: "short",
          });
        }

        const monthlyInstallments = {};
        const monthlyNumProperties = {};

        activeRecords.forEach((element, index) => {
         // console.log("JSON---->", element);
          // Count the number of houses for each record
          const numHouses = element.house_information.length;
          //console.log("number of houses-->", numHouses);
          // Increment the totalProperties counter
          totalProperties += numHouses;

          // Calculate total installments paid for each record
          const totalInstallments = element.installments.reduce(
            (total, installment) => {
              return total + parseFloat(installment.amount);
            },
            0
          );
          // Increment the totalInstallmentsPaid counter
          totalInstallmentsPaid += totalInstallments;

          const totalSellingPrices = element.house_information.reduce(
            (total, house) => {
              return total + parseFloat(house.selling_price);
            },
            0
          );
          // Increment the totalSellingPrice counter
          totalSellingPrice += totalSellingPrices;
          //console.log();

          const dateString = element.date; // Example date string in "DD/MM/YYYY" format
          const parts = dateString.split("/"); // Split the string into parts
          const year = parseInt(parts[2], 10); // Parse the year part as an integer
          const month = parseInt(parts[1], 10) - 1; // Parse the month part as an integer (subtract 1 since months are zero-based)
          const day = parseInt(parts[0], 10); // Parse the day part as an integer
          const dateObject = new Date(year, month, day); // Create the Date object
          //console.log("incoming date", dateObject);

          // const createdAtDate = new Date(element.date);
          // console.log("createdDate", createdAtDate);
          // Extract the month (0-indexed)
          const monthName = dateObject.toLocaleString("en-US", {
            month: "short",
          });

          //console.log("Month:", monthName);

          const graph_object_clients = {
            month: monthName,
          };

          const graph_object_Installments = {
            month: monthName,
          };

          //const month = extractMonth(json.date);
          let totalInstallment = 0;
          element.installments.forEach((installment) => {
            totalInstallment += parseFloat(installment.amount);
          });
          if (!monthlyInstallments[monthName]) {
            monthlyInstallments[monthName] = totalInstallment;
          } else {
            monthlyInstallments[monthName] += totalInstallment;
          }

          const numProperties = element.house_information.length;
          if (!monthlyData[monthName]) {
            monthlyData[monthName] = { totalInstallment, numProperties };
          } else {
            monthlyData[monthName].totalInstallment += totalInstallment;
            monthlyData[monthName].numProperties += numProperties;
          }
          // Update total number of properties array
          if (!monthlyNumProperties[monthName]) {
            monthlyNumProperties[monthName] = numProperties;
          } else {
            monthlyNumProperties[monthName] += numProperties;
          }
          // console.error("graph_object_clients", graph_object_clients);

          graph_array_for_clients.push(graph_object_clients);
          graph_array_for_installments.push(graph_object_Installments);
        });
        ///<******************************************END OF MAIN JSON LOOP***********************************************************>

        // console.log("ARRAY", graph_array_for_clients);

        const monthCounts = graph_array_for_clients.reduce((clients, item) => {
          const month = item.month;
          clients[month] = (clients[month] || 0) + 1;
          return clients;
        }, {});

        monthlyclientDataset = Object.entries(monthCounts).map(
          ([month, clients]) => ({ month, clients })
        );

        // Build JSON array of month and total installment
        const result = Object.keys(monthlyInstallments).map((month) => {
          return { month, totalInstallment: monthlyInstallments[month] };
        });

        const totalPropertiesArray = Object.keys(monthlyNumProperties).map(
          (month) => {
            return {
              month,
              totalProperties: monthlyNumProperties[month],
            };
          }
        );

        //    console.warn("result", result);

        setTotalMonthlyInstallments(result);

        //    console.error("total properties per month", totalPropertiesArray);

        setMonthlyDatasetForClients(monthlyclientDataset);

        setMonthlyDatasetForProperties(totalPropertiesArray);

        // if (monthlyClientData != undefined) {
        //   setMonthlyDatasetForClients(monthlyClientData);
        // }

        // console.log("total number of house==>", totalProperties);
        setTotalProperties(totalProperties);

        // console.log("total installments paid", totalInstallmentsPaid);

        setTotalInstallments(totalInstallmentsPaid);

        ///  console.log("total number of houses", properties);

        //console.log("total selling price", totalSellingPrice);
        setTotalSellingPrice(totalSellingPrice);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    handleGetAllRecords(storedData.usr_id);
  }, [refresher]); // The empty dependency array ensures the effect runs only once when the component mounts

  const valueFormatter = (value) => `${value}mm`;

  const chartSetting = {
    yAxis: [
      {
        label: "rainfall (mm)",
      },
    ],
    width: 800,
    height: 500,
    sx: {
      [`.${axisClasses.left} .${axisClasses.label}`]: {
        transform: "translate(-20px, 0)",
      },
    },
  };
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));

  const handlePercentageChange = (event) => {
    setSelectedPercentage(event.target.value);
  };
  const handleAmountChange = (event) => {
    setAmount(event.target.value);
  };
  const handleCalculation = (event) => {
    event.preventDefault(); // Prevent default form submission behavior

    // Perform the calculation
    const percentage = parseFloat(selectedPercentage);
    const amountValue = parseFloat(amount);
    const calculatedResult = (percentage / 100) * amountValue;

    // Update the state to display the result
    setResults(calculatedResult.toFixed(2)); // Rounding to 2 decimal places
  };

  const onButtonClick = () => {
    // using Java Script method to get PDF file
    fetch("SamplePDF.pdf").then((response) => {
      response.blob().then((blob) => {
        // Creating new object of PDF file
        const fileURL = window.URL.createObjectURL(blob);

        // Setting various property values
        let alink = document.createElement("a");
        alink.href = fileURL;
        alink.download = "SamplePDF.pdf";
        alink.click();
      });
    });
  };
  const [isVisible, setIsVisible] = useState(false);

  // Function to scroll to the top of the page
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  const scrollToBottom = () => {
    const documentHeight = document.documentElement.scrollHeight;
    const windowHeight = window.innerHeight;
    const scrollPosition = documentHeight - windowHeight;

    window.scrollTo({
      top: scrollPosition,
      behavior: "smooth",
    });
  };

  // Function to handle the scroll event
  const handleScroll = () => {
    if (window.scrollY > 300) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  return (
    <>
      <Modal open={openCalculator} onClose={handleclose_calc}>
        <Box sx={style_box}>
          <div className="flex flex-col gap-3 mt-3">
            <div className="flex flex-row justify-between">
              <Typography
                variant="body2"
                // backgroundColor="#FF9900"
                color="text.primary"
                style={{
                  fontSize: "16px",
                  fontWeight: "bold",
                  padding: 8,
                  borderRadius: 14,
                  boxShadow: 8,
                }}
              >
                Bonus Calculator
              </Typography>
              <CloseIcon
                fontSize="40"
                onClick={handleclose_calc}
                sx={{ cursor: "pointer" }}
              />
            </div>
            <form onSubmit={handleCalculation}>
              <div className="flex flex-col gap-3 mt-3">
                <FormControl>
                  <FormLabel sx={{ marginBottom: 2 }}>
                    Select percentage
                  </FormLabel>
                  <TextField
                    id="percentage"
                    select
                    name="percentage"
                    label="Percentage %"
                    placeholder="House status"
                    fullWidth
                    value={selectedPercentage}
                    onChange={handlePercentageChange}
                  >
                    {option.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
                <TextField
                  type="number"
                  id="amount"
                  name="amount"
                  label="Amount"
                  placeholder="Amout to be calculated"
                  fullWidth
                  value={amount}
                  onChange={handleAmountChange}
                />
                <TextField
                  id="results"
                  name="results"
                  label="Results"
                  placeholder="Results"
                  value={results}
                  disabled
                />
                <div className="flex flex-row space-x-3 justify-center">
                  <Button
                    size="small"
                    variant="contained"
                    color="error"
                    onClick={handleclose_calc}
                  >
                    Close Calc
                  </Button>
                  <Button
                    size="small"
                    variant="contained"
                    sx={{ borderRadius: 40, backgroundColor: "#242333" }}
                    type="submit"
                  >
                    Calculate
                  </Button>
                </div>
              </div>
            </form>
          </div>
        </Box>
      </Modal>
      <MainNavbar proprecord={active_recs} />

      {/* <p>{UserId}</p> */}

      {/* {loading && <LinearProgress />} */}
      <div className="flex flex-col top-[50%] right-[50%] justify-center items-center absolute">
      {loading && <Spinner1 className="z-50" />}</div>
      {/* <div> {loading && <Loader2 className="z-50" />}</div> */}
      <div className=" fixed aria-busy: right-0  flex flex-col bottom-2 bg-transparent">
        <IconButton onClick={scrollToTop}>
          <KeyboardArrowUpIcon />
        </IconButton>
        <IconButton onClick={scrollToBottom}>
          <KeyboardArrowDownIcon />
        </IconButton>
      </div>

      <AnalyticsCard
        totalClients={totalCients}
        totalProperties={properties}
        totalInstallments={totallInstallments}
        totalSellingPrice={totallSellingPrice}
        inc_records={active_recs}
      />

      <div className="flex flex-row justify-between w-auto">
        <ChartGraph />
        <DueClients />
      </div>
    </>
  );
}

const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
};
