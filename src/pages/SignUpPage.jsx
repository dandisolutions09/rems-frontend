import { AccountCircle } from "@mui/icons-material";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import HolidayVillageIcon from "@mui/icons-material/HolidayVillage";
import LOGO from "../assets/Kffice.jpg";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useNavigate } from "react-router-dom";
import Deactivate from "../Alerts/Deactivate";
import React from "react";
import EmailIcon from "@mui/icons-material/Email";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import PersonIcon from "@mui/icons-material/Person";
import { Box, LinearProgress, Typography } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button } from "@mui/material";
import axios from "axios";
// import ErrorEmailTaken from "../Alerts/ErrorUsernameTaken";
import ErrorUsernameTaken from "../Alerts/ErrorUsernameTaken";
import ErrorEmailTaken from "../Alerts/ErrorEmailAlreadyTaken";
import ErrorPhoneNumber from "../Alerts/ErrorPhoneNumber";
import Loader2 from "../loader2/Loader2";
import Spinner1 from "../components/Spinner1";

//ErrorPhoneNumber

export default function SignUpPage() {
  const navigate = useNavigate();
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [openEmailTakenAlert, setOpenEmailTakenAlert] = React.useState(false);
  const [openUsernameTakenAlert, setOpenUsernameTakenAlert] =
    React.useState(false);

  const [openPhoneNumberTakenAlert, setOpenPhoneTakenAlert] =
    React.useState(false);

  //openPhoneNumberTakenAlert

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const [showPassword, setShowPassword] = React.useState(false);
  const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleNavigate_LoginPageAfterSignup = () => {
    setLoading(true);
    setTimeout(() => {
      navigate("/");
    }, 2000);

    setOpenSuccessAlert(true);
  };

  const handleNavigate_LoginPage = () => {
    setLoading(true);
    setTimeout(() => {
      navigate("/");
    }, 2000);

    //setOpenSuccessAlert(true);
  };

  const validationSchema = Yup.object({
    username: Yup.string().required("username is required"),
    password: Yup.string().required("password is required"),
    first_name: Yup.string().required("First name is required"),
    last_name: Yup.string().required("Last Name is required"),
    phone_number: Yup.string().required("Phone number is required"),
    email: Yup.string().email().required("Email is required"),
  });

  const postSignupInformation = async (userdata) => {
    console.log("incoming userdata:->", userdata);

    try {
      setLoading(true);
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        // "https://nurse-backend.onrender.com/add-nurse",
        "https://rems-backend.onrender.com/register-user",
        // "http://localhost:8080/register-user",
        userdata
      );

      setOpenSuccessAlert(true);

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);

      setOpenSuccessAlert(true);
      // setTimeout(handleNavigate_LoginPage, 3000);
      handleNavigate_LoginPageAfterSignup();
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error:", error.response.data);
      const errmsg = error.response.data;
      const test = "Username-already-taken";
      console.log("errmsg", errmsg.split("-"));
      const err_storage = errmsg.split("-")[0];
      console.log("splitted", err_storage);
      if (err_storage === "Username") {
        console.log("username already taken");
        setOpenUsernameTakenAlert(true);
        //setLoading(false);
      } else if (err_storage == "Email") {
        setOpenEmailTakenAlert(true);
        //setLoading(false);
        console.log("email taken");
      } else if (err_storage == "Phone") {
        setOpenPhoneTakenAlert(true);
        //setLoading(false);
      } else {
        console.log("nothing came up");
        //setLoading(false);
      }
    } finally {
      setLoading(false);
    }

    // alert("nurse saved successfully...");
  };

  const formik = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      phone_number: "",
      email: "",
      username: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      //postLoginCreds(values.username, values.password);

      const currentDate = new Date();
      const options = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        //timeZoneName: "short",
      };

      const formattedDate = currentDate.toLocaleDateString("en-US", options);

      const signup_data = {
        //   "_id": "614689abf7842f6b4d8c1567",
        first_name: values.first_name.trim(),
        last_name: values.last_name.trim(),
        email: values.email.trim(),
        username: values.username.trim(),
        password: values.password.trim(),
        status: "active",
        records: [],
        created_at: formattedDate,
      };

      postSignupInformation(signup_data);
    },
  });

  return (
    <>
      {/* {loading && <LinearProgress /> && (
        <Deactivate
          open={openSuccessAlert}
          autoHideDuration={3000}
          onClose={() => setOpenSuccessAlert(false)}
        />
      )} */}
      {loading && <Spinner1 /> && (
        <Deactivate
          open={openSuccessAlert}
          autoHideDuration={3000}
          onClose={() => setOpenSuccessAlert(false)}
        />
      )}

      <Deactivate
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />

      <ErrorEmailTaken
        open={openEmailTakenAlert}
        autoHideDuration={5000}
        onClose={() => setOpenEmailTakenAlert(false)}
      />

      <ErrorUsernameTaken
        open={openUsernameTakenAlert}
        autoHideDuration={5000}
        onClose={() => setOpenUsernameTakenAlert(false)}
      />

      <ErrorPhoneNumber
        open={openPhoneNumberTakenAlert}
        autoHideDuration={3000}
        onClose={() => setOpenUsernameTakenAlert(false)}
      />

      <Box
        sx={{ width: [400, 600] }}
        style={myComponentStyle}
        className="shadow-xl rounded-lg border"
      >
        {loading && <Spinner1 />}
        {/* <HolidayVillageIcon sx={company_logo}/> */}
        <div className="header" style={Header}>
          <img src={LOGO} alt="componay_logo" style={company_logo} />
          <Typography sx={{ fontSize: [30, 48], fontWeight: "bold" }}>
            Real Estate
          </Typography>
          {/* <div style={text}> Real Estate</div> */}
          <div className="underline" style={underline}></div>
          <Typography className="text-sm">Management System</Typography>

          <Box sx={{ fontSize: [30, 48], fontWeight: "bold" }}>
            Create Account
          </Box>
        </div>
        <form onSubmit={formik.handleSubmit}>
          {/* <div className="inputs" style={inputs}> */}
          <div className="inputs" style={inputs}>
            <div className="flex flex-row space-x-4">
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
                // type="text"
                // // style={inputfield}
                sx={{ width: [177, 250] }}
                placeholder="First Name"
                name="first_name"
                value={formik.values.first_name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.first_name && Boolean(formik.errors.first_name)
                }
                helperText={
                  formik.touched.first_name && formik.errors.first_name
                }
              />
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
                type="text"
                // style={inputfield}
                sx={{ width: [177, 250] }}
                placeholder="Last Name"
                name="last_name"
                value={formik.values.last_name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.last_name && Boolean(formik.errors.last_name)
                }
                helperText={formik.touched.last_name && formik.errors.last_name}
              />
            </div>

            <div className="flex flex-row space-x-4" style={input}>
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <PhoneAndroidIcon />
                    </InputAdornment>
                  ),
                }}
                type="text"
                // style={inputfield}
                sx={{ width: [177, 250] }}
                placeholder="Phone Number"
                name="phone_number"
                value={formik.values.phone_number}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.phone_number &&
                  Boolean(formik.errors.phone_number)
                }
                helperText={
                  formik.touched.phone_number && formik.errors.phone_number
                }
              />
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon />
                    </InputAdornment>
                  ),
                }}
                type="text"
                // style={inputfield}
                sx={{ width: [177, 250] }}
                placeholder="Email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </div>
            <div className="input" style={input}>
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <PersonIcon />
                    </InputAdornment>
                  ),
                }}
                type="text"
                // style={inputfield}
                sx={{ width: [340, 400] }}
                placeholder="Username"
                name="username"
                value={formik.values.username}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.username && Boolean(formik.errors.username)
                }
                helperText={formik.touched.username && formik.errors.username}
              />
            </div>
            <div className="input" style={input}>
              <TextField
                type={showPassword ? "text" : "password"}
                name="password"
                // style={inputfield}
                sx={{ width: [340, 400] }}
                placeholder="password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete="none"
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </div>
          </div>

          <div className="flex flex-col gap-4">
            <Box sx={{ marginLeft: [13, 24], marginTop: 2 }}>
              <Typography
                className="hover:underline cursor-pointer "
                onClick={handleNavigate_LoginPage}
                sx={{ fontSize: [12, 16] }}
              >
                Log in If You Have An Account
              </Typography>
            </Box>

            <div className="flex justify-center items-center mb-4">
              <button
                //onClick={handleNavigate_BasicPage}
                type="submit"
                style={submit}
                className=" text-white"
              >
                <Typography sx={{ fontSize: [12, 15] }}>Signup</Typography>
              </button>
            </div>
          </div>
        </form>
      </Box>
    </>
  );
}

const myComponentStyle = {
  background: "",
  // height: "100%",
  alignItems: "center",
  // display: "flex",
  // padding: "10px",
  margin: "auto",
  // marginTop: "20px",
  // width: "600px",
  flexDirection: "column",
  // padingBottom: "20px",
};
const Header = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  gap: "9px",
  width: "100%",
  // marginTop: "2px",
};

const text = {
  color: "#000",

  fontSize: "48px",
  fontWeight: "700",
};
const underline = {
  width: "71px",
  height: "6px",
  background: "#000",
  borderRadius: "9px",
};
const inputs = {
  marginTop: "30px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  gap: "25px",
};
const input = {
  display: "flex",
  // alignItems: "center",
  // justifyContent: "center",
  // width: "480px",
  // height: "80px",
};
const icons = {
  marginRight: "30px",
};
const inputfield = {
  height: "50px",
  width: "400px",
  background: "transparent",
  border: "none",
  outline: "none",
  color: "#797979",
  fontSize: "19px",
};
const forgot_password = {
  // paddingLeft: "70px",
  // marginTop: "27px",
  color: "#797979",
  // justifyContent:"center",
  // alignItems: "center",
};
const spn = {
  color: "#4c00b4",
};
const submit_container = {
  // display: "flex",
  // gap: "30px",
  // margin: "20px 120px",
};
const submit = {
  display: "flex",
  background: "#000",
  justifyContent: "center",
  alignItems: "center",
  width: "220px",
  height: "59px",
  color: "#fff",
  borderRadius: "50px",
  fontSize: "19px",
  fontWeight: "700",
  cursor: "pointer",
};
const company_logo = {
  width: "100px",
  height: "100px",
  marginTop: "20px",
  borderRadius: 50,
  // padingBottom: "30px",
  // boxShadow: "0px 0px 10px 0px rgba(0, 0, 0, 0.1)",
};
