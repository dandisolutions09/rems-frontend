import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useLocation, useNavigate } from "react-router-dom";
import { Dropdown } from "antd";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";

import {
  Box,
  Button,
  FormControl,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import MainNavbar from "../components/MainNavbar";

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

export default function ClientInstallments() {
  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [openBooking, setOpenBooking] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [rows, setRows] = React.useState([]);
  const [client_name, setClientName] = React.useState("john");

  const location = useLocation();
  const incoming_userData = location.state;

  console.log("installments", incoming_userData);
  //setRows(incoming_userData.installments);

  const columns = [
    { id: "created_at", label: "Date and Time" },
    {
      id: "amount",
      label: "Installments",
      minWidth: 100,
      align: "center",
    },
    // {
    //   id: "population",
    //   label: "House Unit",
    //   minWidth: 100,
    //   align: "center",
    //   format: (value) => value.toLocaleString("en-US"),
    // },
  ];

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();
    // handleGetAllRecords();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleDoubleClick = (er) => {
    navigate("/details");
  };
  const handle_back = () => {
    navigate("/records");
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleClose_booking = () => {
    setOpenBooking(false);
  };
  const handleOpen_booking = () => {
    setOpenBooking(true);
  };
  const items = [
    {
      key: "1",
      label: "Edit Record",
      // icon: <AccountCircle sx={{ fontSize: 32 }} />,
      onClick: () => handleOpen_booking(),
    },
  ];
  const role = [
    {
      key: "1",
      value: "Admin",
      label: "Interested",
    },
    {
      key: "2",
      value: "Intermediate",
      label: "Booked",
    },
    {
      key: "3",
      value: "Basic",
      label: "Sold",
    },
  ];
  const status = [
    {
      key: "1",
      value: "Active",
      label: "Agent",
      onclick: () => handleOpenAgent(),
    },
    {
      key: "2",
      value: "Inactive",
      label: "No Agent",
    },
  ];
  const type = [
    {
      key: "1",
      value: "studio",
      label: "Studio",
    },
    {
      key: "2",
      value: "1bed",
      label: "One bedroom",
    },
    {
      key: "3",
      value: "2bed",
      label: "Two bedroom",
    },
    {
      key: "4",
      value: "3bed",
      label: "Three bedroom",
    },
    {
      key: "5",
      value: "4bed",
      label: "4 bedroom",
    },
  ];

  return (
    <>
      <MainNavbar />
      <div className="flex flex-row justify-between m-4">
        <div
          className="flex flex-row cursor-pointer hover:scale-110 duration-200 ease-linear"
          onClick={handle_back}
        >
          <ArrowBackIosIcon sx={{ fontSize: ["15px", "22px"] }} />
          <Typography sx={{ fontSize: ["13px", "18px"] }}>Back</Typography>
        </div>
        <div className="">
          <Button
            variant="contained"
            sx={{ backgroundColor: "#242333", fontSize: ["10px", "16px"] }}
            // onClick={handleOpen_booking}
          >
            Download pdf
          </Button>
        </div>
      </div>

      <div className=" flex flex-col ml-4">
        <div className="flex flex-row gap-2 mt-12">
          <Typography  sx={{ fontSize: [13,18] }}>Name:</Typography>
          <Typography sx={{ fontSize: 20 }}>
            {incoming_userData.client_information.first_name}&nbsp;&nbsp;
            {incoming_userData.client_information.second_name} &nbsp;&nbsp;
            {incoming_userData.client_information.last_name} &nbsp;&nbsp;
          </Typography>
        </div>

        <div className="flex flex-row gap-2">
          <Typography  sx={{ fontSize: [13,18] }}>Email:</Typography>
          <Typography sx={{ fontSize: 15, fontWeight:"bold" }}>
            {incoming_userData.client_information.email}&nbsp;&nbsp;
          </Typography>
        </div>

        <div className="flex flex-row gap-2">
          <Typography  sx={{ fontSize: [13,18] }}>Booking Date:</Typography>
          <Typography sx={{ fontSize: 15, fontWeight:"bold" }}>
            {incoming_userData.date}&nbsp;&nbsp;
          </Typography>
        </div>

        <div className="flex flex-row gap-2 mt-4">
          <Typography  sx={{ fontSize: [13,18] }}>Total House Value (KES):</Typography>
          <Typography sx={{ fontSize: 15, fontWeight:"bold" }}>
            {parseInt(
              incoming_userData.house_information.reduce(
                (total, house) => total + parseInt(house.selling_price),
                0
              )
            ).toLocaleString()}
            &nbsp;&nbsp;
          </Typography>
        </div>



        <div className="flex flex-row gap-2">
          <Typography  sx={{ fontSize: [13,18] }}>Total Installments Paid (KES):</Typography>
          <Typography sx={{ fontSize: 15, fontWeight:"bold" }}>
            {parseInt(
              incoming_userData.installments.reduce(
                (total, house) => total + parseInt(house.amount),
                0
              )
            ).toLocaleString()}
            &nbsp;&nbsp;
          </Typography>
        </div>

        <div className="flex flex-row gap-2">
          <Typography  sx={{ fontSize: [13,18] }}>Remaining Balance (KES):</Typography>
          <Typography sx={{ fontSize: 15, fontWeight:"bold" }}>
            {(
              parseInt(
                incoming_userData.house_information.reduce(
                  (total, house) => total + parseInt(house.selling_price),
                  0
                )
              ) -
              parseInt(
                incoming_userData.installments.reduce(
                  (total, installment) => total + parseInt(installment.amount),
                  0
                )
              )
            ).toLocaleString()}
            &nbsp;&nbsp;
          </Typography>
        </div>
      </div>

     
        <TableContainer component={Paper} sx={{marginTop:8}}>
          <Table stickyHeader aria-label="sticky table">
          <TableHead sx={{ bgcolor: "#242333" }}>
              <TableRow className="select-none">
                <TableCell
                  sx={{
                    fontWeight: 800,
                    fontSize: ["14px", "18px"],
                    width: "200px",
                  }}
                
                >
                  installment Date
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }} align="right">
                  Amount
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {incoming_userData.installments.map((row, index) => (
                <Dropdown
                  key={index}
                  menu={{ items }}
                  trigger={["contextMenu"]}
                  placement="bottomLeft"
                >
                  <TableRow
                    key={index}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    className="hover:bg-gray-300 select-none"
                    //onDoubleClick={handleRowDoubleClick}
                    // onDoubleClick={() => handleRowDoubleClick(row)}
                    onMouseEnter={() => {
                      setSelectedRowData(row);
                      setIndex(index);
                    }}
                  >
                    {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                    <TableCell
                      className="flex flex-row w-10"
                      sx={{
                        // fontWeight: 800,
                        fontSize: ["15px", "18px"],
                        width: "200px",
                      }}
                    >
                      {row.created_at} &nbsp;
                    </TableCell>

                    <TableCell align="right">
                      {parseFloat(row.amount).toLocaleString()}
                    </TableCell>
                    {/* <TableCell >{row.house_information}</TableCell> */}

                    {/* house unit column */}
                    {/* <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index} className="flex flex-row">
                          <HolidayVillageOutlinedIcon fontSize="20px" />
                          {house.house_number},{house.house_type}{" "}
                        </div>
                      ))}
                    </TableCell> */}

                    {/* Location column */}
                    {/* <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {house.location}
                        </div>
                      ))}
                    </TableCell> */}

                    {/* Status column */}
                    {/* <TableCell>
                      {row.house_information.map((house, index) => (
                        <div key={index}>{house.house_status} </div>
                      ))}
                    </TableCell> */}

                    {/* Selling Price Column */}

                    {/* Balance Column */}
                  </TableRow>
                </Dropdown>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          className="select-none"
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
 
    </>
  );
}
const header = {
  //   border: "1px solid",
  borderColor: "#000",
  margin: "100px",
  padding: "50px",
  borderRadius: "8px",
};
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
};
