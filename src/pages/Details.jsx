import { Button } from "@mui/material";
import React, { Component } from "react";

class Details extends Component {
  componentDidMount() {
    // Load the Visme forms embedding script
    const script = document.createElement("script");
    script.src = "https://static-bundles.visme.co/forms/vismeforms-embed.js";
    script.async = true;
    document.body.appendChild(script);
  }

  render() {
    return (
      <>
        <div
          class="visme_d"
          data-title="Email Contact Form"
          data-url="6x39y3w3-email-contact-form"
          data-domain="forms"
          data-full-page="false"
          data-min-height="500px"
          data-form-id="28136"
        ></div>
      

        {/* <div className=" justify-center text-center mb-10">
          <Button
            variant="contained"
            sx={{ backgroundColor: "#242333", fontSize: ["10px", "16px"] }}
          >
            open
          </Button>
        </div> */}
      </>
    );
  }
}

export default Details;
