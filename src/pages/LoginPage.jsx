import { AccountCircle } from "@mui/icons-material";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import HolidayVillageIcon from "@mui/icons-material/HolidayVillage";
import LOGO from "../assets/Kffice.jpg";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useNavigate } from "react-router-dom";
import Deactivate from "../Alerts/Deactivate";
import React from "react";
import { Box, Button, LinearProgress, Typography } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import AddSucces from "../Alerts/AddSucces";
import LoginUnsuccefull from "../Alerts/LoginUnsuccefull";
import LoginSuccessfull from "../Alerts/LoginSuccessfull";
import Loader2 from "../loader2/Loader2";
import Spinner1 from "../components/Spinner1";

export default function LoginPage() {
  const navigate = useNavigate();
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [openLoginSuccess, setOpenLoginSuccess] = React.useState(false);
  const [openLoginFailed, setOpenLoginFailed] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword, setShowPassword] = React.useState(false);
  const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const validationSchema = Yup.object({
    username: Yup.string().required("username is required"),
    password: Yup.string().required("password is required"),
  });

  const handleNavigate_BasicPage = () => {
    navigate("/home");
    setOpenSuccessAlert(true);
  };

  const handleNavigate_signup = () => {
    navigate("/signup");
    setOpenSuccessAlert(true);
  };

  const handleNavigateMain = (passed_username, id, passed_firstname) => {
    setOpenLoginSuccess(true);
    console.log("Connecting...");
    console.log("username", passed_username, id);

    const userData = {
      username: passed_username,
      _id: id,
      FirstName: passed_firstname,
      // Add other properties as needed
    };
    //setUserData(userData);

    const userDataJSON = JSON.stringify(userData);

    localStorage.setItem("userData_storage", userDataJSON);

    // setText("mycontext username");

    const usr_data = { usr_name: passed_username, usr_id: id };
    // login({ username: 'exampleUser' });

   

    // Introduce a delay using setTimeout (e.g., 2000 milliseconds or 2 seconds)
    setTimeout(() => {
      navigate("/home", { state: usr_data });

      // Navigate to the "/home" route after the delay
    }, 2000);
    formik.resetForm();
  };

  const postLoginCreds = async (usrname, pwd) => {
    console.log("data to be sent", usrname, pwd);

    const LoginData = {
      username: usrname,
      password: pwd,
    };

    try {
      setLoading(true); // Set loading to true when making the request
      const response = await axios.post(
        "https://rems-backend.onrender.com/login",
        LoginData
      );

      console.log("Response from the backend:", response.data);

      //var arrayResult = response.data.split(",");

      if (response.data.message == "Authentication successful") {
        console.error("SUCCESSFULL", response.data.user._id);

        handleNavigateMain(
          LoginData.username,
          response.data.user._id,
          response.data.user.first_name
        );
      } else if (response.data.message == "Authentication Not Successful") {
        console.error("NOT SUCCESSFULL");
        setOpenLoginFailed(true);
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      postLoginCreds(values.username.trim(), values.password.trim());
    },
  });

  return (
    <>
      {/* {loading && <Loader2 />} */}
      {loading && <Spinner1 />}
      <LoginSuccessfull
        open={openLoginSuccess}
        autoHideDuration={6000}
        onClose={() => setOpenLoginSuccess(false)}
      />
      <LoginUnsuccefull
        open={openLoginFailed}
        autoHideDuration={3000}
        onClose={() => setOpenLoginFailed(false)}
      />
      <Box
        sx={{ width: [390, 600] }}
        style={myComponentStyle}
        className="shadow-xl rounded-lg border"
      >
        {/* {loading && (
          <Typography sx={{ color: "#000" }} className="text-sm">
            Loading...
          </Typography>
        )} */}
        <img src={LOGO} alt="componay_logo" style={company_logo} />
        {/* <HolidayVillageIcon sx={company_logo}/> */}
        {/* Sign Up if you don't Have an Account */}
        <div className="header" style={Header}>
          <Typography sx={{ fontSize: [30, 48], fontWeight: "bold" }}>
            Real Estate
          </Typography>

          <div className="underline" style={underline}></div>
          <Typography className="text-sm" sx={{ fontSize: [12, 20] }}>
            Management System
          </Typography>
        </div>
        <form onSubmit={formik.handleSubmit}>
          <div className="inputs" style={inputs}>
            <div className="input" style={input}>
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
                type="text"
                // style={inputfield}
                sx={{ width: [320, 400] }}
                placeholder="username"
                name="username"
                value={formik.values.username}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.username && Boolean(formik.errors.username)
                }
                helperText={formik.touched.username && formik.errors.username}
              />
            </div>
            <div className="input" style={input}>
              <TextField
                type={showPassword ? "text" : "password"}
                name="password"
                // style={inputfield}
                sx={{ width: [320, 400] }}
                placeholder="password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete="none"
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </div>
          </div>
          {/* <Box sx={{ marginLeft: [15, 10], marginTop: 4 }}>
            <Typography
              className="hover:underline cursor-pointer "
              onClick={handleNavigate_signup}
              sx={{ fontSize: [12, 18] }}
            >
              Sign Up if you don't Have an Account
            </Typography>
          </Box> */}

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div style={{ marginLeft: [15, 10], marginTop: 4 }}>
              <Typography
                className="hover:underline cursor-pointer"
                onClick={handleNavigate_signup}
                sx={{ fontSize: [12, 18] }}
              >
                Sign Up if you don't Have an Account
              </Typography>
            </div>
            <div style={submit_container}>
              <Button
                //onClick={handleNavigate_BasicPage}
                type="submit"
                style={submit}
                className=" text-white"
              >
                <Typography sx={{ fontSize: [12, 15] }}>Login</Typography>
              </Button>
            </div>
          </div>
        </form>
      </Box>
    </>
  );
}

const myComponentStyle = {
  background: "",
  // height: "760px",
  alignItems: "center",
  display: "flex",
  padding: "10px",
  margin: "auto",
  marginTop: "50px",
  // width: "600px",
  flexDirection: "column",
  padingBottom: "20px",
};
const Header = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  gap: "9px",
  width: "100%",
  marginTop: "30px",
};

const text = {
  color: "#000",

  fontSize: "48px",
  fontWeight: "700",
};
const underline = {
  width: "71px",
  height: "6px",
  background: "#000",
  borderRadius: "9px",
};
const inputs = {
  marginTop: "55px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  gap: "25px",
};
const input = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "480px",
  height: "80px",
};
const icons = {
  marginRight: "30px",
};
const inputfield = {
  height: "50px",
  width: "400px",
  background: "transparent",
  border: "none",
  outline: "none",
  color: "#797979",
  fontSize: "19px",
};
const forgot_password = {
  // paddingLeft: "70px",
  // marginTop: "27px",
  // color: "#797979",
  // justifyContent:"center",
  // alignItems: "center",
};
const spn = {
  color: "#4c00b4",
};
const submit_container = {
  // display: "flex",
  // gap: "30px",
  margin: "30px 110px",
  display: "flex",
  justifyContent: "center",
};
const submit = {
  display: "flex",
  background: "#000",
  justifyContent: "center",
  alignItems: "center",
  width: "220px",
  height: "59px",
  color: "#fff",
  borderRadius: "50px",
  fontSize: "19px",
  fontWeight: "700",
  cursor: "pointer",
};
const company_logo = {
  width: "100px",
  height: "100px",
  marginTop: "20px",
  borderRadius: 50,
  // padingBottom: "30px",
  // boxShadow: "0px 0px 10px 0px rgba(0, 0, 0, 0.1)",
};
