import React from "react";
import TestTable from "../components/TestTable";
import MainNavbar from "../components/MainNavbar";
import { useNavigate } from "react-router-dom";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import {
  IconButton,
  InputAdornment,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import Dropdown from "antd/es/dropdown/dropdown";
import HolidayVillageOutlinedIcon from "@mui/icons-material/HolidayVillageOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import { styled, alpha } from "@mui/material/styles";
import SearchIcon from "@mui/icons-material/Search";
import Loader2 from "../loader2/Loader2";
import Spinner1 from "../components/Spinner1";

export default function BonusPage() {
  const [expanded, setExpanded] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [revenuePercentage, setRevenuePercentage] = React.useState();
  const [rows, setRows] = React.useState([]);
  const [selectedRowData, setSelectedRowData] = useState();
  const [totalInstallments, setTotalInstallments] = React.useState("");
  const [totallSellingPrice, setTotalSellingPrice] = useState();
  const [totallBalance, setBalance] = useState();
  const [totalClients, setTotalClients] = useState(0);
  const [totalProperties, setTotalProperties] = useState();
  const [fullName, setName] = useState();
  const [bonus, setBonus] = useState();
  const [bonus_multiplier, setBonusMultiplier] = useState(0.005);
  const [email, setEmail] = useState();
  const [bonus_dataset_state, setBonusDataset] = useState();
  const [sorted_year, setSortedYear] = useState("2024");
  const [has2023, setContains2023] = useState();
  const [has2024, setContains2024] = useState();
  const [bonusInfo, setBonusInfo] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const navigate = useNavigate();
  const handle_back = () => {
    navigate("/myaccount");
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  function handleGetAllRecords(searchVal) {
    setLoading(true);
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    // console.log("ID", storedData);
    //setGlobalId(storedData._id);
    //setLoading(true);
    //console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://nurse-backend.onrender.com/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        //  console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);

        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //  // setRows(activeRecords);
        //   // setActiveRecords(activeRecords);
        // } else {
        //   const results = activeRecords.filter((user) => {
        //     const firstName =
        //       user.client_information && user.client_information.first_name
        //         ? user.client_information.first_name.toLowerCase()
        //         : "";
        //     const lastName =
        //       user.client_information && user.client_information.last_name
        //         ? user.client_information.last_name.toLowerCase()
        //         : "";
        //     const searchTerm = searchVal.toLowerCase();
        //     return (
        //       firstName.includes(searchTerm) || lastName.includes(searchTerm)
        //     );
        //   });

        //   console.log("filtered results:-->", results);

        // //  setRows(results);
        //   //setActiveRecords(results);
        // }

        let totalInstallmentsPaid = 0;
        let totalSellingPrice = 0; // Initialize totalSellingPrice counter
        let totalProperties = 0;
        let bonus_dataset = [];

        const monthNames = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ];

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        // console.log("after-sorting-records:->", activeRecords);

        //  setRows(activeRecords);
        setName(json.first_name + "    " + json.last_name);
        setEmail(json.email);

        //setPhoneNumber(json.first_name + "    "+ json.last_name);

        //console.log("active records----->", activeRecords);

        setTotalClients(activeRecords.length);

        activeRecords.forEach((element, index) => {
          if (
            element.client_information.house_status == "SOLD" ||
            element.client_information.house_status == "BOOKED" ||
            element.client_information.house_status == "FULLY-SOLD"
            // ||
            // element.client_information.house_status == "INTERESTED"
          ) {
            const totalSellingPrices = element.house_information.reduce(
              (total, house) => {
                return total + parseFloat(house.selling_price);
              },
              0
            );

            //console.error("total Bonus Recieved", totalSellingPrices * 0.03);

            console.log("house", element);

            // console.log("month", new Date(element.date))

            const dateString = element.date; // Example date string in "DD/MM/YYYY" format
            const parts = dateString.split("/"); // Split the string into parts
            const year = parseInt(parts[2], 10); // Parse the year part as an integer
            const month = parseInt(parts[1], 10) - 1; // Parse the month part as an integer (subtract 1 since months are zero-based)
            const day = parseInt(parts[0], 10); // Parse the day part as an integer
            const dateObject = new Date(year, month, day); // Create the Date object
            //console.log("incoming date", dateObject.getMonth());

            const monthIndex = dateObject.getMonth();

            const monthName = monthNames[monthIndex];

            // const bonus_obj = {
            //   client_first_name: element.client_information.first_name,
            //   client_second_name: element.client_information.second_name,
            //   house: element.house_information,
            //   date: element.date,
            //   broker: element.broker_information.first_name,
            //   //house_bonus: totalSellingPrices * bonus_multiplier,
            //   house_bonus:
            //     element.broker_information.first_name.trim() == ""
            //       ? totalSellingPrices * bonus_multiplier
            //       : 7000,

            //   bonus_paid: element.bonus_payments,

            //   month: monthName,
            // };

            console.log("Bonus payments", element);

            const bonus_obj = {
              client_first_name: element.client_information.first_name,
              client_second_name: element.client_information.second_name,
              house: element.house_information,
              date: element.date,
              broker: element.broker_information.first_name,
              //house_bonus: totalSellingPrices * bonus_multiplier,
              house_bonus:
                element.broker_information.first_name.trim() == ""
                  ? totalSellingPrices * bonus_multiplier
                  : 7000,
              bonus_paid: element.bonus_payments.reduce((total, payment) => {
                // Ensure that amount exists and is not empty
                if (payment.amount && !isNaN(parseFloat(payment.amount))) {
                  return total + parseFloat(payment.amount);
                } else {
                  return total;
                }
              }, 0),
              month: monthName,
            };

            // console.warn("house object", bonus_obj);
            bonus_dataset.push(bonus_obj);

            //setBonus(totalBonus);

            //     console.log("total clients-->", activeRecords.length);

            //    setTotalClients(activeRecords.length);

            totalSellingPrice += totalSellingPrices;
            // Increment the totalSellingPrice counter

            const totalInstallments = element.installments.reduce(
              (total, installment) => {
                return total + parseFloat(installment.amount);
              },
              0
            );
            // Increment the totalInstallmentsPaid counter
            totalInstallmentsPaid += totalInstallments;

            const numHouses = element.house_information.length;
            //console.log("number of houses-->", numHouses);
            // Increment the totalProperties counter
            totalProperties += numHouses;
          } // end og activeRecords for loop
          //console.error("TOTAL SALES", totalSellingPrice * bonus_multiplier);

          // setBalance(totalSellingPrice - totalInstallmentsPaid);
          // console.log("total nst", totalInstallmentsPaid);
          //   console.log("total number of houses:-", totalProperties);
          setTotalProperties(totalProperties);

          setTotalInstallments(totalInstallmentsPaid);
          setTotalSellingPrice(totalSellingPrice);
          // console.log(
          //   "balance",
          //   parseFloat(totalSellingPrice - totalInstallmentsPaid)
          // );

          const bal = parseFloat(totalSellingPrice - totalInstallmentsPaid);
          //console.log("balance", bal / 1000000);
          setBalance(parseFloat(bal));
        }); //end of activRecords LOOP

        console.log("full array", bonus_dataset);
        //setRows(bonus_dataset);

        if (searchVal === undefined || searchVal.trim().length === 0) {
          //setRows(activeRecords);
          setRows(bonus_dataset);
          // setActiveRecords(activeRecords);
        } else {
          const results = bonus_dataset.filter((user) => {
            const firstName =
              user && user.client_first_name
                ? user.client_first_name.toLowerCase()
                : "";
            const lastName =
              user && user.client_second_name
                ? user.client_second_name.toLowerCase()
                : "";
            const searchTerm = searchVal.toLowerCase();
            return (
              firstName.includes(searchTerm) || lastName.includes(searchTerm)
            );
          });

          console.log("filtered results:-->", results);

          setRows(results);
          //setRows(bonus_dataset);
          //setActiveRecords(results);
        }

        function containsYear2024(dataArray) {
          return dataArray.some((obj) => {
            const year = parseInt(obj.date.split("/")[2]);
            return year === 2024;
          });
        }

        function containsYear2023(dataArray) {
          return dataArray.some((obj) => {
            const year = parseInt(obj.date.split("/")[2]);
            return year === 2023;
          });
        }

        const contains2024 = containsYear2024(bonus_dataset);
        const contains2023 = containsYear2023(bonus_dataset);
        setContains2023(containsYear2023(bonus_dataset));
        setContains2024(containsYear2024(bonus_dataset));

        console.error("has 2024", containsYear2024(bonus_dataset));

        if (contains2023) {
          setContains2023(true);
        }

        console.log("contains2024", contains2024);

        // Dictionary to store total house selling price for each month
        const monthlyTotals = {};
        console.log("sorted year", sorted_year);

        if (sorted_year != undefined) {
        }

        const filteredData = bonus_dataset.filter((entry) =>
          entry.date.includes(sorted_year)
        );
        console.error("filteredData", filteredData);

        // Step 1: Parse dates into Date objects
        const parsedData = filteredData.map((item) => {
          const [day, month, year] = item.date.split("/");
          return {
            ...item,
            date: new Date(year, month - 1, day), // month is 0-indexed in Date constructor
          };
        });

        // Handle the case where filteredData is undefined

        // Step 2: Group by month
        const groupedByMonth = parsedData.reduce((acc, item) => {
          const month = item.date.getMonth();
          acc[month] = acc[month] || [];
          acc[month].push(item);
          return acc;
        }, {});

        // Step 3: Calculate total house_bonus for each month
        const totalsByMonth = Object.entries(groupedByMonth).map(
          ([month, items]) => ({
            month: new Date(2024, month).toLocaleString("default", {
              month: "short",
            }), // Get full month name
            total_house_bonus: items.reduce(
              (total, item) => total + item.house_bonus,
              0
            ),
          })
        );

        // Step 4: Sort by month
        totalsByMonth.sort((a, b) => {
          const monthA = new Date(`${a.month} 1, 2024`);
          const monthB = new Date(`${b.month} 1, 2024`);
          return monthA - monthB;
        });

        console.log("TOTALS BY MONTH:->", totalsByMonth);

        console.log("TOTALS BY MONTH", totalsByMonth);
        //console.log("dataet", dataset);

        const totalBonus = totalSellingPrice * bonus_multiplier;
        console.log("total selling price", totalSellingPrice);

        console.log("total bonus", totalBonus);

        // setBonus(totalsByMonth);

        setBonusDataset(totalsByMonth);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    console.log("search term:->", event.target.value);
    handleGetAllRecords(event.target.value);
    // You can perform additional logic here, such as filtering/searching based on the entered search term
  };

  React.useEffect(() => {
    handleGetAllRecords();
  }, []);

  return (
    <>
      <MainNavbar />
      {/* <div> {loading && <Loader2 className="z-50" />}</div> */}
      <div className="flex flex-col top-[50%] right-[50%] justify-center items-center absolute">
      {loading && <Spinner1 className="z-50" />}</div>
      <div
        className="m-4 w-10 cursor-pointer hover:scale-110 duration-200 ease-linear shadow-xl px-4 py-1 rounded-lg"
        onClick={handle_back}
      >
        <ArrowBackIosIcon sx={{ fontSize: ["18px", "22px"], marginTop: 1 }} />
      </div>
      <div className="m-4 flex">
        <Typography
          sx={{ fontSize: ["13px", "18px"], marginTop: 1, fontWeight: "bold" }}
        >
          Bonus on Each House Unit
        </Typography>
      </div>
      {/* <TestTable /> */}

      <div className="flex  justify-center mt-[20px] m-3">
        <TextField
          size="small"
          sx={{ width: [220, 400] }}
          variant="outlined"
          label="Search client's name"
          value={searchTerm}
          onChange={handleSearchChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton>
                  <SearchIcon sx={{ fontSize: [18, 22] }} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </div>

      <Paper sx={{ width: "100%", overflow: "hidden", padding: 3 }}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow className="select-none">
                <TableCell
                  sx={{
                    fontWeight: 800,
                    fontSize: ["14px", "18px"],
                    width: "200px",
                  }}
                >
                  Clients
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Date
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Bonus Paid
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Bonus
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Balance
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House selling Price
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House Number{" "}
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Project{" "}
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Location{" "}
                </TableCell>
                {/* <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Selling Price{" "}
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Amount Paid{" "}
                </TableCell>
                <TableCell
                  align="right"
                  sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}
                >
                  Balance{" "}
                </TableCell> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => (
                  <Dropdown
                    key={index}
                    // menu={{ items }}
                    trigger={["contextMenu"]}
                    placement="bottomLeft"
                  >
                    <TableRow
                      key={index}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                      className="hover:bg-gray-400 select-none"
                      //onDoubleClick={handleRowDoubleClick}
                      // onDoubleClick={() => handleRowDoubleClick(row)}
                      // onMouseEnter={() => {
                      //   setSelectedRowData(row);
                      //   setIndex(index);
                      // }}
                    >
                      {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                      <TableCell
                        className="flex flex-row w-10"
                        sx={{
                          fontWeight: 800,
                          fontSize: ["12px", "15px"],
                          width: "200px",
                        }}
                      >
                        {row.client_first_name} &nbsp;
                        {row.client_second_name}
                      </TableCell>

                      <TableCell>{row.date}</TableCell>
                      {/* <TableCell >{row.house_information}</TableCell> */}

                      <TableCell>
                        {parseFloat(row.bonus_paid).toLocaleString()}
                      </TableCell>

                      <TableCell>
                        {parseFloat(row.house_bonus).toLocaleString()}
                      </TableCell>


                      <TableCell>
                        {parseFloat(row.house_bonus-row.bonus_paid).toLocaleString()}
                      </TableCell>


                      {/* Selling Price Column */}
                      <TableCell>
                        {/* Map through each selling price and format with commas */}
                        {row.house.map((x, index) => (
                          <div key={index}>
                            {parseFloat(x.selling_price).toLocaleString()}
                          </div>
                        ))}
                      </TableCell>

                      {/* house unit column */}
                      <TableCell>
                        {row.house.map((x, index) => (
                          <div key={index} className="flex flex-row">
                            {x.house_number},{x.house_type}{" "}
                          </div>
                        ))}
                      </TableCell>

                      {/* Location column */}

                      <TableCell>
                        {/* {row.house_information.map((house, index) => (
                        <div key={index}>{house.house_status} </div>
                      ))}
                    */}

                        {row.house.map((x, index) => (
                          <div key={index} className="flex">
                            <LocationOnOutlinedIcon fontSize="20px" />{" "}
                            {x.residence}
                          </div>
                        ))}
                      </TableCell>

                      {/* Status column */}
                      <TableCell>
                        {row.house.map((x, index) => (
                          <div key={index} className="flex">
                            <LocationOnOutlinedIcon fontSize="20px" />{" "}
                            {x.location}
                          </div>
                        ))}
                      </TableCell>

                      {/* Amount Paid Column */}

                      {/* Balance Column */}
                    </TableRow>
                  </Dropdown>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          className="select-none"
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
