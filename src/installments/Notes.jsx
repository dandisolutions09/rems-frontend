import { Typography } from "@mui/material";
import React from "react";

React

export default function Notes() {
//   const { notes } = useContext(State);

  return (
    <>
      <section className="mt-10 mb-5">
        <Typography>Additional notes</Typography>
        <Typography className="lg:w-1/2 text-justify">{"notes"}</Typography>
      </section>
    </>
  );
}
