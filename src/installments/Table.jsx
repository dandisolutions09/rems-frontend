import { TableCell, Typography } from "@mui/material";
import React from "react";

export default function Table({ tableData }) {
  console.log("tabledata", tableData);

  return (
    <>
      <table width="100%" className="mb-10">
        <thead>
          <tr className="bg-gray-100 p-1">
            <TableCell
              style={{
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              Date and Time
            </TableCell>
            <TableCell
              style={{
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              House Unit
            </TableCell>

            <TableCell
              style={{
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              House Type
            </TableCell>
            <TableCell
              style={{
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              House Location
            </TableCell>

            <TableCell
              style={{
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              Amount Paid
            </TableCell>
          </tr>
        </thead>

        {/* <tbody>
          {tableData.map((row, index) => (
            <tr key={index} className="h-10">
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.location}</TableCell>
              <TableCell>{row.type}</TableCell>
              <TableCell>{row.unit}</TableCell>
              <TableCell>{row.amount}</TableCell>
            </tr>
          ))}
        </tbody> */}
      </table>

      <div className="flex items-end justify-between">
        <span className=" text-gray-800 ">
          <Typography className="">Total: </Typography>
          <Typography
            variant="h4"
            noWrap
            component="div"
            sx={{
              display: { xs: "none", sm: "block" },
              fontWeight: "bold",
              cursor: "pointer",
            }}
          >
            {/* Kshs. {total.toLocaleString()} */} Kshs 100,000
          </Typography>
        </span>
        <span className=" text-gray-800 ">
          <Typography className="">Balance: </Typography>
          <Typography
            variant="h4"
            noWrap
            component="div"
            sx={{
              display: { xs: "none", sm: "block" },
              fontWeight: "bold",
              cursor: "pointer",
            }}
          >
            {/* Kshs. {total.toLocaleString()} */} Kshs 1000,000
          </Typography>
        </span>
      </div>
    </>
  );
}
