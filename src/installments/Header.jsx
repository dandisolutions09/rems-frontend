import { Typography } from "@mui/material";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { useNavigate } from "react-router-dom";



export default function Header() {
  const navigate = useNavigate();

  function handlenavigate_back(){
    navigate("/records")
  }
  return (
    <>
      <header className="flex flex-col items-center justify-center mb-5 xl:flex-row xl:justify-between">
        <div className="flex flex-col ">
          <div
            className="m-2 flex flex-row cursor-pointer "
            onClick={handlenavigate_back}
          >
            <ArrowBackIosNewIcon className="hover:scale-110 duration-200 ease-linear select-none" />
            <Typography className="hover:scale-110 duration-200 ease-linear select-none">
              Back
            </Typography>
          </div>
          <Typography
            variant="body2"
            color="black"
            style={{ fontSize: "30px", margin:4 }}
          >
            Installments
          </Typography>
        </div>

        <div>
          <ul className="flex items-center justify-between flex-wrap">
            <li>
              <button
                // onClick={handlePrint}
                className="bg-gray-500 text-white font-bold py-2 px-8 rounded shadow border-2 border-gray-500 hover:bg-transparent hover:text-gray-500 transition-all duration-300"
              >
                <Typography sx={{ fontWeight: "bold" }}>Print</Typography>
              </button>
            </li>
            <li className="mx-2">
              <button className="bg-blue-500 text-white font-bold py-2 px-8 rounded shadow border-2 border-blue-500 hover:bg-transparent hover:text-blue-500 transition-all duration-300">
                <Typography sx={{ fontWeight: "bold" }}>Download</Typography>
              </button>
            </li>
            <li>
              <button className="bg-green-500 text-white font-bold py-2 px-8 rounded shadow border-2 border-green-500 hover:bg-transparent hover:text-green-500 transition-all duration-300">
                <Typography sx={{ fontWeight: "bold" }}>Send</Typography>
              </button>
            </li>
          </ul>
        </div>
      </header>
    </>
  );
}
