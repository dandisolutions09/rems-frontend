import React, { useState } from "react";
import Header from "./Header";
import MainDetails from "./MainDetails";
import ClientDetails from "./ClientDetails";
import Dates from "./Dates";
import Table from "./Table";
import Notes from "./Notes";

import MainNavbar from "../components/MainNavbar";

export default function InstallmentsPage() {
  const [refresher, setRefresher] = React.useState(false);
  const [rows, setRows] = useState([]);
  const [global_id, setGlobalId] = React.useState();
  const [loading, setLoading] = React.useState(false);

  function handleGetAllRecords() {
    var storedDataJSON = localStorage.getItem("userData_storage");
    var storedData = JSON.parse(storedDataJSON);
    console.log("ID", storedData);
    setGlobalId(storedData._id);
    setLoading(true);
    console.log("handle get all records called!");
    // fetch("https://rems-backend.onrender.com/get-records")
    fetch(`https://rems-backend.onrender.com/get-userbyid/${storedData._id}`)
      // fetch(`http://localhost:8080/get-userbyid/${storedData._id}`)
      //`http://localhost:8080/get-userbyid/${storedData._id}
      // fetch("http://localhost:8080/get-records")
      .then((response) => response.json())
      .then((json) => {
        console.log("before-sorting-records:->", json);

        // if(json.date){
        //   console.log("date", json)

        // }

        // console.error("json", json.records);

        const activeRecords = json.records.filter(
          (record) => record.status === "ACTIVE"
        );

        activeRecords.sort(
          (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );

        console.log("after-sorting-records:->", activeRecords.house_information);

        // activeRecords.sort(
        //   (a, b) => new Date(b.created_at) - new Date(a.created_at)
        // );

        setRows(activeRecords.house_information);

        // Assuming activeRecords is an array of objects
        // activeRecords.sort((a, b) => {
        //   // Convert created_at strings to Date objects for comparison
        //   const dateA = new Date(a.created_at);
        //   const dateB = new Date(b.created_at);

        //   // Compare the Date objects
        //   return dateA - dateB;
        // });

        //console.log("rec logs", json);

        activeRecords.forEach((element, index) => {
          // console.log("element--", element);

          const totalInstallmentsPaid = element.installments.reduce(
            (total, amount) => {
              return total + parseFloat(amount.amount);
            },
            0
          );

          const totalSellingPrice = element.house_information.reduce(
            (total, sp) => {
              return total + parseFloat(sp.selling_price);
            },
            0
          );

          // setBalance(totalSellingPrice - totalInstallmentsPaid);

          //setTotalInstallments(totalInstallmentsPaid);
        });
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllRecords();
  }, [refresher]); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <MainNavbar />
      {/* Invoice Preview */}
      <div className="invoice__preview bg-white p-5  ">
        <div>
          <Header />

          <MainDetails />

          <ClientDetails />

          <Dates />

          <Table tableData={rows} />

          <Notes />
          {/* 
          <Footer /> */}
        </div>
      </div>
    </>
  );
}
