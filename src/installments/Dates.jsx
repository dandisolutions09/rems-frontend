// import { useContext } from "react";

import { Typography } from "@mui/material";
import React from "react";

// import { State } from "../context/stateContext";


export default function Dates() {
//   const { invoiceNumber, invoiceDate, dueDate } = useContext(State);

  return (
    <>
      <article className="mb-14 flex items-end ml-[400px]">
        <ul className="flex flex-row space-x-6">
          <Typography className="p-1 ">
            <span className="font-bold">Record number:</span> {"30002"}
          </Typography>
          <Typography className="p-1 bg-gray-100">
            <span className="font-bold">Record date:</span> {"12/3/2024"}
          </Typography>
          <Typography className="p-1 ">
            <span className="font-bold">Due date:</span> {"15/02/2024"}
          </Typography>
        </ul>
      </article>
    </>
  );
}
