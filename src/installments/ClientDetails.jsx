// import { useContext } from "react";

import { Typography } from "@mui/material";
import React from "react";

// import { State } from "../context/stateContext";


export default function ClientDetails() {
//   const { clientName, clientAddress } = useContext(State);

  return (
    <>
      <section className="flex flex-row space-x-6">
        <Typography
         
          noWrap
          component="div"
          sx={{
            display: { xs: "none", sm: "block" },
            fontWeight: "bold",
            cursor: "pointer",
          }}
        >
          {"clients Name"}:
        </Typography>
        <Typography
          variant="h5"
          noWrap
          component="div"
          sx={{
            display: { xs: "none", sm: "block" },
            fontWeight: "bold",
            cursor: "pointer",
          }}
          className="text-2xl uppercase font-bold mb-1"
        >
          {"Ogola Jesse"}
        </Typography>
      </section>
    </>
  );
}
