// import { useContext } from "react";

import { Typography } from "@mui/material";
import React from "react";

// import { State } from "../context/stateContext";


export default function MainDetails() {
//   const { name, address } = useContext(State);

  return (
    <>
      <section className="flex flex-col items-end justify-end ">
        <Typography className="font-bold text-3xl uppercase mb-1">{"name"}</Typography>
       <Typography
            variant="h5"
            noWrap
            component="div"
            sx={{
              display: { xs: "none", sm: "block" },
              fontWeight: "bold",
              cursor: "pointer",
            }}>{"Don Collins"}</Typography>
      </section>
    </>
  );
}
